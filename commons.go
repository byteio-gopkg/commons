package commons

func Equals(a, b any) bool {
	return a == b
}

func Zero[T any]() T {
	var z T
	return z
}

func If[T any](flag bool, v1, v2 T) T {
	if flag {
		return v1
	} else {
		return v2
	}
}

func NilIf[T comparable](v T, zero T) any {
	if v == zero {
		return nil
	}
	return v
}

type Number interface {
	int | uint | int8 | int16 | int32 | int64 | uint8 | uint16 | uint32 | uint64 | float32 | float64
}

func NilIf0[T Number](v T) any {
	if v == 0 {
		return nil
	}
	return v
}

func IsType[V any](v any) bool {
	_, ok := v.(V)
	return ok
}

func Cast[V any](v any) V {
	var ret V
	if v == nil {
		return ret
	}
	return v.(V)
}

func TryCast[V any](v any) (V, bool) {
	if nv, ok := v.(V); ok {
		return nv, true
	} else {
		var z V
		return z, false
	}
}

func ValueOf[V any](vp *V) V {
	if vp == nil {
		var z V
		return z
	}
	return *vp
}

func ValuePtr[V any](v V) *V {
	return &v
}

func SendToChan[M any](c chan M, m M) bool {
	select {
	case c <- m:
		return true
	default:
		return false
	}
}

type Iterator[E any] interface {
	HasNext() bool
	Next() E
}
