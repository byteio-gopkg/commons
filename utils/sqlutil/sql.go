package sqlutil

import (
	. "byteio.org/commons"
	. "byteio.org/commons/goi/sql"
	"context"
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"
)

func ExecI(db ExecAble, sql string, args ...any) int64 {
	res, err := db.Exec(sql, args...)
	if err != nil {
		return 0
	}
	id, _ := res.LastInsertId()
	return id
}

func ExecIE(db ExecAble, sql string, args ...any) (int64, error) {
	res, err := db.Exec(sql, args...)
	if err != nil {
		return 0, err
	}
	return res.LastInsertId()
}

func Insert(db ExecAble, table string, row Entity) int64 {
	columns := make([]string, len(row))
	args := make([]any, len(row))
	vars := make([]string, len(row))
	for i, column := range row {
		columns[i] = column.Name
		args[i] = column.Value
		vars[i] = "?"
	}
	return ExecI(db, `INSERT INTO `+table+` (`+strings.Join(columns, `, `)+`) VALUES (`+strings.Join(vars, `, `)+`)`,
		args...,
	)
}

func Exec(db ExecAble, sql string, args ...any) int64 {
	res, err := db.Exec(sql, args...)
	if err != nil {
		return 0
	}
	effected, _ := res.RowsAffected()
	return effected
}

func ExecE(db ExecAble, sql string, args ...any) (int64, error) {
	res, err := db.Exec(sql, args...)
	if err != nil {
		return 0, err
	}
	return res.RowsAffected()
}

func Update(db ExecAble, table string, set Entity, where Entity) int64 {
	setArgsNum := len(set)
	if setArgsNum == 0 {
		return 0
	}
	whereArgsNum := len(where)
	setSql := make([]string, setArgsNum)
	args := make([]any, 0, setArgsNum+whereArgsNum)
	for i, column := range set {
		setSql[i] = column.Name + ` = ?`
		args = append(args, column.Value)
	}
	_sql := `UPDATE ` + table + ` SET ` + strings.Join(setSql, `, `)
	if whereArgsNum > 0 {
		whereSql := make([]string, 0, whereArgsNum)
		for _, w := range where {
			if w.Value != nil {
				whereSql = append(whereSql, w.Name+` = ?`)
				args = append(args, w.Value)
			}
		}
		if len(whereSql) > 0 {
			_sql += ` WHERE ` + strings.Join(whereSql, ` AND `)
		}
	}
	return Exec(db, _sql, args...)
}

func ExecConn(conn *sql.Conn, sql string, args ...any) int64 {
	res, err := conn.ExecContext(context.Background(), sql, args...)
	if err != nil {
		return 0
	}
	effected, _ := res.RowsAffected()
	return effected
}

func ValueSQL(v any) string {
	switch v1 := v.(type) {
	case string:
		return `'` + v1 + `'`
	case int:
		return strconv.FormatInt(int64(v1), 10)
	case int8:
		return strconv.FormatInt(int64(v1), 10)
	case int32:
		return strconv.FormatInt(int64(v1), 10)
	case int64:
		return strconv.FormatInt(v1, 10)
	default:
		return fmt.Sprint(v)
	}
}

func ValuesSQL[E string | int | int8 | int32 | int64](values []E) string {
	list := make([]string, len(values))
	for i, v := range values {
		list[i] = ValueSQL(v)
	}
	return `(` + strings.Join(list, `, `) + `)`
}

func ValuesArgsSQL(n int) string {
	if n < 1 {
		return "()"
	}
	_sql := strings.Repeat(", ?", n)
	return `(` + _sql[2:] + `)`
}

// --------------------------------------------------------

func QueryExists(db QueryAble, sql string, args ...any) bool {
	rows, _ := db.Query(sql, args...)
	if rows == nil {
		return false
	}
	defer rows.Close()
	return rows.Next()
}

type ColumnValue interface {
	int | *int | uint | *uint | int32 | *int32 | uint32 | *uint32 | int64 | *int64 | uint64 | *uint64 |
		float32 | *float32 | float64 | *float64 |
		bool | *bool | string | *string | []byte |
		time.Time | *time.Time
}

func Query[T ColumnValue](db QueryAble, sql string, args ...any) T {
	rows, _ := db.Query(sql, args...)
	var value T
	if rows == nil {
		return value
	}
	defer rows.Close()
	if rows.Next() {
		_ = rows.Scan(&value)
	}
	return value
}

func QueryE[T ColumnValue](db QueryAble, sql string, args ...any) (T, error) {
	rows, err := db.Query(sql, args...)
	var value T
	if err != nil {
		return value, err
	}
	defer rows.Close()
	if rows.Next() {
		err = rows.Scan(&value)
	}
	return value, err
}

func QueryV[T ColumnValue](db QueryAble, sql string, args ...any) (T, bool) {
	rows, _ := db.Query(sql, args...)
	var value T
	if rows == nil {
		return value, false
	}
	defer rows.Close()
	if rows.Next() {
		err := rows.Scan(&value)
		if err == nil {
			return value, true
		}
	}
	return value, false
}

func QueryList[T ColumnValue](db QueryAble, sql string, args ...any) []T {
	rows, _ := db.Query(sql, args...)
	if rows == nil {
		return nil
	}
	defer rows.Close()
	list := make([]T, 0)
	for rows.Next() {
		var val T
		if rows.Scan(&val) == nil {
			list = append(list, val)
		}
	}
	return list
}

func QueryRow[T any](db QueryAble, scanFunc func(*sql.Rows, *T) error, sql string, args ...any) *T {
	rows, _ := db.Query(sql, args...)
	if rows == nil {
		return nil
	}
	defer rows.Close()
	if !rows.Next() {
		return nil
	}
	row := new(T)
	err := scanFunc(rows, row)
	if err != nil {
		return nil
	}
	return row
}

func QueryRows[T any](db QueryAble, scanFunc func(*sql.Rows, *T) error, sql string, args ...any) []*T {
	rows, _ := db.Query(sql, args...)
	if rows == nil {
		return nil
	}
	defer rows.Close()
	list := make([]*T, 0)
	var err error
	for rows.Next() {
		row := new(T)
		err = scanFunc(rows, row)
		if err == nil {
			list = append(list, row)
		}
	}
	return list
}

func QueryConn[T ColumnValue](conn *sql.Conn, sql string, args ...any) T {
	rows, _ := conn.QueryContext(context.Background(), sql, args...)
	var value T
	if rows == nil {
		return value
	}
	defer rows.Close()
	if rows.Next() {
		_ = rows.Scan(&value)
	}
	return value
}

// --------------------------------------------------------

type Querying struct {
	listSql  string
	orderSql string
	limit    int64
	where    []string
	params   []any
}

//goland:noinspection GoExportedFuncWithUnexportedType
func NewQuery(listSql string) *Querying {
	return &Querying{listSql: listSql}
}

func (q *Querying) AddWhere(sql string) *Querying {
	q.where = append(q.where, sql)
	return q
}

func (q *Querying) AddValue(value any) *Querying {
	q.params = append(q.params, value)
	return q
}

func (q *Querying) WhereAnd(sql string, value any) *Querying {
	q.where = append(q.where, sql)
	q.params = append(q.params, value)
	return q
}

func (q *Querying) WhereAndValues(sql string, values ...any) *Querying {
	q.where = append(q.where, sql)
	for _, v := range values {
		q.params = append(q.params, v)
	}
	return q
}

func (q *Querying) WhereBy(sql string, value any, condition bool) *Querying {
	if condition {
		q.WhereAnd(sql, value)
	}
	return q
}

func (q *Querying) WhereAndOr(columns []string, orSql string, value any) *Querying {
	var where []string
	for _, column := range columns {
		where = append(where, column+" "+orSql)
		q.params = append(q.params, value)
	}
	if len(where) > 0 {
		q.where = append(q.where, "("+strings.Join(where, " OR ")+")")
	}
	return q
}

func (q *Querying) WhereEqual(column string, value any, condition bool) *Querying {
	if condition {
		q.WhereAnd(column+" = ?", value)
	}
	return q
}

func (q *Querying) WhereEqualAny(columns []string, value any, condition bool) *Querying {
	if condition {
		q.WhereAndOr(columns, "= ?", value)
	}
	return q
}

func (q *Querying) WhereNotEqual(column string, value any, condition bool) *Querying {
	if condition {
		q.WhereAnd(column+" != ?", value)
	}
	return q
}

func (q *Querying) WhereNotEqualAny(columns []string, value any, condition bool) *Querying {
	if condition {
		for _, column := range columns {
			q.WhereAnd(column+" != ?", value)
		}
	}
	return q
}

func (q *Querying) WhereEqualInt32(column string, value int32) *Querying {
	if value != 0 {
		q.WhereAnd(column+" = ?", value)
	}
	return q
}

func (q *Querying) WhereEqualString(column string, value string) *Querying {
	if value != "" {
		q.WhereAnd(column+" = ?", value)
	}
	return q
}

func (q *Querying) WhereLike(column string, value string) *Querying {
	if value != "" {
		q.WhereAnd(column+" LIKE ?", "%"+value+"%")
	}
	return q
}

func (q *Querying) WhereLikeAny(columns []string, value string) *Querying {
	if value != "" {
		q.WhereAndOr(columns, "LIKE ?", "%"+value+"%")
	}
	return q
}

func (q *Querying) WhereIn(column string, values []any) *Querying {
	if values != nil {
		q.where = append(q.where, column+" IN "+ValuesArgsSQL(len(values)))
		q.params = append(q.params, values...)
	}
	return q
}

func (q *Querying) WhereNotIn(column string, values []any) *Querying {
	if values != nil {
		q.where = append(q.where, column+" NOT IN "+ValuesArgsSQL(len(values)))
		q.params = append(q.params, values...)
	}
	return q
}

func (q *Querying) OrderBy(sql string) *Querying {
	q.orderSql = sql
	return q
}

func (q *Querying) Limit(limit int64) *Querying {
	q.limit = limit
	return q
}

// Executable output executable
// return SQL, SQL arguments
func (q *Querying) Executable() (string, []any) {
	fullSql := q.listSql
	if len(q.where) > 0 {
		whereSql := ` WHERE ` + strings.Join(q.where, ` AND `)
		fullSql += whereSql
	}
	if q.orderSql != "" {
		fullSql += ` ORDER BY ` + q.orderSql
	}
	if q.limit > 0 {
		fullSql += ` LIMIT ` + fmt.Sprint(q.limit)
	}
	return fullSql, q.params
}

func (q *Querying) QueryMapRows(db QueryAble, handler MapRowHandler) []map[string]any {
	_sql, args := q.Executable()
	return QueryMapRows(db, handler, _sql, args...)
}
