package sqlutil

import (
	. "byteio.org/commons/goi/sql"
	"database/sql/driver"
	"io"
)

type MapRowHandler func(name string, value driver.Value) (string, any)

type IMapRows interface {
	MapRows() driver.Rows
}

func QueryMapRow(db QueryAble, handler MapRowHandler, sql string, args ...any) map[string]any {
	rows, _ := db.Query(sql, args...)
	if rows == nil {
		return nil
	}
	defer rows.Close()
	rawRows := unsafeRawRows(rows)
	if rawRows == nil {
		return nil
	}

	var columns []string
	var row []driver.Value
	if im, ok := rawRows.(IMapRows); ok {
		columns = rawRows.Columns()
		if columns == nil {
			return nil
		}
		mapRows := im.MapRows()
		row = make([]driver.Value, len(columns))
		err := mapRows.Next(row)
		if err != nil {
			return nil
		}
	} else {
		if rows.Next() {
			columns = rawRows.Columns()
			if columns == nil {
				return nil
			}
			row = unsafeRawRow(rows)
		}
		if row == nil {
			return nil
		}
	}
	ret := make(map[string]any)
	for i, columnValue := range row {
		columnName := columns[i]
		if handler != nil {
			columnName, columnValue = handler(columnName, columnValue)
		}
		ret[columnName] = columnValue
	}
	return ret
}

func QueryMapRows(db QueryAble, handler MapRowHandler, sql string, args ...any) []map[string]any {
	rows, _ := db.Query(sql, args...)
	if rows == nil {
		return nil
	}
	defer rows.Close()
	rawRows := unsafeRawRows(rows)
	if rawRows == nil {
		return nil
	}

	var columns []string
	var columnName string
	var columnIndex int
	var columnValue any
	var row []driver.Value
	list := make([]map[string]any, 0)
	if im, ok := rawRows.(IMapRows); ok {
		columns = rawRows.Columns()
		if columns == nil {
			return nil
		}
		mapRows := im.MapRows()
		row = make([]driver.Value, len(columns))
		var err error
		for {
			err = mapRows.Next(row)
			if err != nil {
				if err != io.EOF {
					return nil
				}
				break
			}
			mapRow := make(map[string]any)
			for columnIndex, columnValue = range row {
				columnName = columns[columnIndex]
				if handler != nil {
					columnName, columnValue = handler(columnName, columnValue)
				}
				mapRow[columnName] = columnValue
			}
			list = append(list, mapRow)
		}
	} else {
		for rows.Next() {
			if columns == nil {
				columns = rawRows.Columns()
				if columns == nil {
					return nil
				}
			}
			row = unsafeRawRow(rows)
			if row == nil {
				return nil
			}
			mapRow := make(map[string]any)
			for columnIndex, columnValue = range row {
				columnName = columns[columnIndex]
				if handler != nil {
					columnName, columnValue = handler(columnName, columnValue)
				}
				mapRow[columnName] = columnValue
			}
			list = append(list, mapRow)
		}
	}

	return list
}
