package sqlutil

import (
	"database/sql"
	"database/sql/driver"
	"reflect"
	"time"
	"unsafe"
)

//goland:noinspection SpellCheckingInspection
var (
	rowsRowsiOffset    uintptr = 0
	rowsLastcolsOffset uintptr = 0
)

//goland:noinspection SpellCheckingInspection
func init() {
	rowsType := reflect.TypeOf(sql.Rows{})
	getOffset := func(name string) uintptr {
		f, found := rowsType.FieldByName(name)
		if !found {
			panic("package incompatible: rows field '" + name + "' not found")
		}
		return f.Offset
	}
	rowsRowsiOffset = getOffset("rowsi")
	rowsLastcolsOffset = getOffset("lastcols")
}

//goland:noinspection GoVetUnsafePointer,SpellCheckingInspection
func unsafeRawRows(rows *sql.Rows) driver.Rows {
	rowsPointer := uintptr(unsafe.Pointer(rows))
	return *(*driver.Rows)(unsafe.Pointer(rowsPointer + rowsRowsiOffset))
}

//goland:noinspection GoVetUnsafePointer,SpellCheckingInspection
func unsafeRawRow(rows *sql.Rows) []driver.Value {
	rowsPointer := uintptr(unsafe.Pointer(rows))
	return *(*[]driver.Value)(unsafe.Pointer(rowsPointer + rowsLastcolsOffset))
}

//go:linkname convertAssign database/sql.convertAssign
func convertAssign(dest, src any) error

// --------------------------------------------------------

// WithNull scan value with null
func WithNull[T int | uint | int8 | uint8 | int32 | uint32 | int64 | uint64 | float32 | float64 | bool | string | time.Time](ptr *T) any {
	return nullValue{ptr}
}

type nullValue struct{ ptr any }

func (v nullValue) Scan(value any) error {
	if value == nil {
		return nil
	}
	return convertAssign(v.ptr, value)
}
