package sqlutil

import (
	. "byteio.org/commons/goi/sql"
	"database/sql/driver"
	"encoding/json"
	"errors"
	"reflect"
)

func QueryJson[T any](db QueryAble, sql string, args ...any) T {
	var value T
	b := Query[[]byte](db, sql, args...)
	if b == nil {
		return value
	}
	if json.Unmarshal(b, &value) != nil {
		var zero T
		return zero
	}
	return value
}

func QueryJsonE[T any](db QueryAble, sql string, args ...any) (T, error) {
	var value T
	b, err := QueryE[[]byte](db, sql, args...)
	if err != nil {
		return value, err
	}
	err = json.Unmarshal(b, &value)
	if err != nil {
		var zero T
		return zero, err
	}
	return value, nil
}

// --------------------------------------------------------

func JsonOf(v any) any {
	return jsonValue{v}
}

type jsonValue struct{ v any }

func (j jsonValue) Value() (driver.Value, error) {
	rv := reflect.ValueOf(j.v)
	if vk := rv.Kind(); (vk == reflect.Pointer || vk == reflect.Slice || vk == reflect.Map) && rv.IsNil() {
		return nil, nil
	}
	b, err := json.Marshal(j.v)
	if err != nil {
		return nil, err
	}
	return string(b), nil
}

func (j jsonValue) Scan(value any) error {
	if value == nil {
		return nil
	}
	if b, ok := value.([]byte); ok {
		return json.Unmarshal(b, j.v)
	}
	if b, ok := value.(string); ok {
		return json.Unmarshal([]byte(b), j.v)
	}
	return errors.New("converting to json only supported for []byte or string")
}

func ScanJsonAny(value any) any {
	if value == nil {
		return nil
	}
	var v any
	if b, ok := value.([]byte); ok {
		if json.Unmarshal(b, &v) == nil {
			return v
		}
	}
	if b, ok := value.(string); ok {
		if json.Unmarshal([]byte(b), &v) == nil {
			return v
		}
	}
	return nil
}
