package ziputil

import (
	"archive/zip"
	"io"
	"os"
	"path/filepath"
)

func ZipFiles(srcBase string, srcList []string, dest string) error {
	zipFile, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer zipFile.Close()
	zipWriter := zip.NewWriter(zipFile)
	defer zipWriter.Close()

	srcBaseLen := 0
	if srcBase != "" {
		srcBase, err = filepath.Abs(srcBase)
		if err != nil {
			return err
		}
		srcBaseLen = len(srcBase)
		if srcBase != filepath.Dir(srcBase) {
			srcBaseLen += 1
		}
	}
	for _, srcPath := range srcList {
		baseLen := srcBaseLen
		if srcBase != "" {
			srcPath = filepath.Join(srcBase, srcPath)
		} else {
			srcPath, err = filepath.Abs(srcPath)
			if err != nil {
				return err
			}
			baseLen = len(filepath.Dir(srcPath))
			if baseLen < len(srcPath) {
				baseLen += 1
			}
		}
		if _, err = os.Stat(srcPath); err != nil && os.IsNotExist(err) {
			continue
		}
		err = filepath.Walk(srcPath, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if len(path) <= baseLen {
				return nil
			}
			var fileHeader *zip.FileHeader
			fileHeader, err = zip.FileInfoHeader(info)
			if err != nil {
				return err
			}
			fileHeader.Name = filepath.ToSlash(path[baseLen:])
			if info.IsDir() {
				fileHeader.Name += "/"
			} else {
				fileHeader.Method = zip.Deflate
			}
			var writer io.Writer
			writer, err = zipWriter.CreateHeader(fileHeader)
			if err != nil {
				return err
			}
			if !info.IsDir() {
				var srcFile *os.File
				srcFile, err = os.Open(path)
				if err != nil {
					return err
				}
				defer srcFile.Close()
				_, err = io.Copy(writer, srcFile)
			}
			return err
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func ZipFile(src string, dest string) error {
	return ZipFiles("", []string{src}, dest)
}

// --------------------------------------------------------

func UnzipFile(src string, dest string) error {
	zipReader, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer zipReader.Close()

	for _, file := range zipReader.Reader.File {
		path := filepath.Join(dest, file.Name)
		if file.FileInfo().IsDir() {
			err = os.MkdirAll(path, file.Mode())
		} else {
			err = copyToFile(file, path)
		}
		if err != nil {
			return err
		}
	}
	for _, file := range zipReader.Reader.File {
		_ = os.Chtimes(filepath.Join(dest, file.Name), file.Modified, file.Modified)
	}
	return nil
}

func copyToFile(file *zip.File, dest string) error {
	reader, err := file.Open()
	if err != nil {
		return err
	}
	defer reader.Close()

	var destFile *os.File
	destFile, err = os.OpenFile(dest, os.O_CREATE|os.O_TRUNC|os.O_RDWR, file.Mode())
	if err != nil {
		return err
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, reader)
	return err
}
