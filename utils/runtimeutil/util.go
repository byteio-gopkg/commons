package runtimeutil

import (
	"byteio.org/commons/utils/timeutil"
	"bytes"
	"fmt"
	"runtime"
	"runtime/debug"
	"strconv"
)

func PanicIfError(err error) {
	if err != nil {
		panic(err)
	}
}

func RecoverWithErrorPrint() {
	if rv := recover(); rv != nil {
		PrintError("routine panic:", rv)
	}
}

func PrintError(message string, err any) {
	println(timeutil.NowDateTimeMS(), message, fmt.Sprint(err)+"\n"+string(debug.Stack()))
}

func GoID() uint64 {
	b := make([]byte, 64)
	b = b[:runtime.Stack(b, false)]
	b = bytes.TrimPrefix(b, []byte("goroutine "))
	b = b[:bytes.IndexByte(b, ' ')]
	n, _ := strconv.ParseUint(string(b), 10, 64)
	return n
}

func DumpAllStacks() []byte {
	bufSize := runtime.NumGoroutine() * 1024
	if bufSize > 16*1024*1024 {
		bufSize = 16 * 1024 * 1024
	}
	buf := make([]byte, bufSize)
	size := runtime.Stack(buf, true)
	if size < 1 {
		return make([]byte, 0)
	}
	return buf[:size]
}
