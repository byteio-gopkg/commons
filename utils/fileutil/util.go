package fileutil

import (
	"bufio"
	"io"
	"os"
	"path/filepath"
	"time"
)

// ExecPath Get execute file directory path
func ExecPath() string {
	path, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return ""
	}
	return path
}

// ----------------------------------------------

func Exist(p string) bool {
	_, err := os.Stat(p)
	return err == nil || !os.IsNotExist(err)
}

func Append(name string) (*os.File, error) {
	return os.OpenFile(name, os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
}

func Write(path string, data []byte) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	_, err = f.Write(data)
	_ = f.Close()
	return err
}

func WriteTemp(path string, data []byte) error {
	return Write(filepath.Join(os.TempDir(), path), data)
}

func Read(path string) ([]byte, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	bytes, err := io.ReadAll(f)
	_ = f.Close()
	return bytes, err
}

func ReadTemp(path string) ([]byte, error) {
	return Read(filepath.Join(os.TempDir(), path))
}

func ReadAsString(path string) (string, error) {
	bytes, err := Read(path)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func ReadLines(path string) ([]string, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	reader := bufio.NewReader(f)
	lines := make([]string, 0)
	for {
		line, _, read := reader.ReadLine()
		if read == io.EOF {
			break
		}
		lines = append(lines, string(line))
	}
	return lines, nil
}

func Tail(f *os.File, size int64) ([]byte, error) {
	stat, err := f.Stat()
	if err != nil {
		return nil, err
	}
	end := stat.Size()
	offset := end - size
	if offset < 0 {
		offset = 0
	}
	buf := make([]byte, end-offset)
	_, err = f.ReadAt(buf, offset)
	if err != nil {
		return nil, err
	}
	return buf, nil
}

//goland:noinspection GoUnhandledErrorResult
func TailWithPath(path string, size int64) ([]byte, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return Tail(f, size)
}

func Walk(path string) []string {
	list := make([]string, 0)
	dir, err := os.ReadDir(path)
	for _, f := range dir {
		fullPath := filepath.Join(path, f.Name())
		if f.IsDir() {
			files := Walk(fullPath)
			if err == nil {
				list = append(list, files...)
			}
		} else {
			list = append(list, fullPath)
		}
	}
	return list
}

func Size(file string) (int64, error) {
	fi, err := os.Stat(file)
	if err != nil {
		return 0, err
	}
	return fi.Size(), nil
}

func ModTime(file string) (time.Time, error) {
	fi, err := os.Stat(file)
	if err != nil {
		return time.Time{}, err
	}
	return fi.ModTime(), nil
}

func copyFile(src string, srcInfo os.FileInfo, dst string) error {
	if srcInfo.IsDir() {
		err := os.MkdirAll(dst, srcInfo.Mode())
		if err != nil {
			return err
		}
		var files []os.DirEntry
		files, err = os.ReadDir(src)
		if err != nil {
			return err
		}
		var fi os.FileInfo
		for _, f := range files {
			fi, err = f.Info()
			if err != nil {
				return err
			}
			fn := f.Name()
			err = copyFile(filepath.Join(src, fn), fi, filepath.Join(dst, fn))
			if err != nil {
				return err
			}
		}
	} else {
		srcFile, err := os.Open(src)
		if err != nil {
			return err
		}
		defer srcFile.Close()
		dstFile, err := os.OpenFile(dst, os.O_RDWR|os.O_CREATE|os.O_TRUNC, srcInfo.Mode())
		if err != nil {
			return err
		}
		defer dstFile.Close()
		_, err = io.Copy(dstFile, srcFile)
		if err != nil {
			return err
		}
	}
	modTime := srcInfo.ModTime()
	_ = os.Chtimes(dst, modTime, modTime)
	return nil
}

func Copy(src string, dst string) error {
	srcInfo, err := os.Stat(src)
	if err != nil {
		return err
	}
	return copyFile(src, srcInfo, dst)
}
