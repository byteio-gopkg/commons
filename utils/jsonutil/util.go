package jsonutil

import (
	"encoding/base64"
	"encoding/json"
)

func UnmarshalAny(bytes []byte) (any, error) {
	var out any
	err := json.Unmarshal(bytes, &out)
	return out, err
}

func Marshal(v any) []byte {
	b, _ := json.Marshal(v)
	return b
}

func MarshalAsString(v any) string {
	b, _ := json.Marshal(v)
	if b != nil {
		return string(b)
	} else {
		return ""
	}
}

func MarshalAsBase64(v any) string {
	b, _ := json.Marshal(v)
	if b != nil {
		return base64.StdEncoding.EncodeToString(b)
	} else {
		return ""
	}
}

func UnmarshalBase64(data string, out any) error {
	bytes, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return err
	}
	return json.Unmarshal(bytes, out)
}
