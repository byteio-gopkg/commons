package mathutil

import (
	"strconv"
	"strings"
)

//goland:noinspection SpellCheckingInspection
func Decimal(v float64, prec int) float64 {
	if float64(int64(v)) == v {
		return v
	}
	v2, err := strconv.ParseFloat(strconv.FormatFloat(v, 'f', prec, 64), 64)
	if err != nil {
		return v
	}
	return v2
}

//goland:noinspection SpellCheckingInspection
func FloatPrec(v float64) int {
	if v == 0 {
		return 0
	}
	s := strconv.FormatFloat(v, 'f', -1, 64)
	i := strings.LastIndexByte(s, '.')
	if i < 0 {
		return 0
	}
	return len(s) - i - 1
}

func FormatFloat(v float64, precision int) string {
	s := strconv.FormatFloat(v, 'f', precision, 64)
	if precision < 1 {
		return s
	}
	sl := len(s)
	end := sl
	var ch uint8
	for i := sl - 1; i > 0; i-- {
		if ch = s[i]; ch != '0' {
			if ch == '.' {
				end = i
			} else {
				end = i + 1
			}
			break
		}
	}
	if end < sl {
		return s[:end]
	}
	return s
}
