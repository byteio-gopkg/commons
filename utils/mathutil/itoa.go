package mathutil

func Ito36A(v int64) string {
	as := make([]byte, 0)
	for {
		m := byte(v % 36)
		v = v / 36
		if m <= 9 {
			as = append(as, '0'+m)
		} else {
			as = append(as, 'A'+(m-10))
		}
		if v == 0 {
			break
		}
	}
	end := len(as)
	if end > 1 {
		mid := end / 2
		for i := 0; i < mid; i++ {
			as[i], as[end-i-1] = as[end-i-1], as[i]
		}
	}
	return string(as)
}
