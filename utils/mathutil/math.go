package mathutil

import "time"

func Abs[N int | int16 | int32 | int64 | time.Duration](n N) N {
	if n < 0 {
		return -n
	}
	return n
}
