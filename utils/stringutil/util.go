package stringutil

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

func NilIfEmpty(v string) any {
	if v == "" {
		return nil
	} else {
		return v
	}
}

func IfEmpty(v string, emptyValue string) string {
	if v == "" {
		return emptyValue
	}
	return v
}

func Sub(s string, start int, length int) string {
	end := start + length
	if end <= len(s) {
		return s[start:end]
	}
	return ""
}

func SubLeft(s string, i int) string {
	if i <= len(s) {
		return s[:i]
	}
	return ""
}

func SubRight(s string, i int) string {
	if i < len(s) {
		return s[i:]
	}
	return ""
}

func ContainsAny(text string, targets ...string) bool {
	for _, t := range targets {
		if strings.Contains(text, t) {
			return true
		}
	}
	return false
}

func EqualAny(text string, targets ...string) bool {
	for _, t := range targets {
		if text == t {
			return true
		}
	}
	return false
}

func HasAnyPrefix(target string, prefixes []string) bool {
	for _, prefix := range prefixes {
		if strings.HasPrefix(target, prefix) {
			return true
		}
	}
	return false
}

func HasAnySuffix(target string, suffixes []string) bool {
	for _, suffix := range suffixes {
		if strings.HasSuffix(target, suffix) {
			return true
		}
	}
	return false
}

func Left(v string, sep string) string {
	return strings.SplitN(v, sep, 2)[0]
}

func Right(v string, sep string) string {
	list := strings.SplitN(v, sep, 2)
	if len(list) == 2 {
		return list[1]
	}
	return ""
}

func TrimSpaceAll(array []string) {
	for i, v := range array {
		array[i] = strings.TrimSpace(v)
	}
}

func TrimSpaceEach(array ...*string) {
	for _, p := range array {
		*p = strings.TrimSpace(*p)
	}
}

func TrimSpaceTokens(v string, sep string) string {
	array := strings.Split(v, sep)
	TrimSpaceAll(array)
	return strings.Join(array, sep)
}

func Abbreviate(v string, maxLen int) string {
	if maxLen <= 0 {
		return v
	}
	if len(v) > maxLen {
		return v[:maxLen] + "..."
	}
	return v
}

func AbbreviateRunes(v string, maxLen int) string {
	if maxLen <= 0 {
		return v
	}
	runes := []rune(v)
	if len(runes) > maxLen {
		return string(runes[:maxLen]) + "..."
	}
	return v
}

func UnderscoreToLowerCamel(v string) string {
	bytes := []byte(v)
	l := len(bytes)
	updated := false
	for i := 0; i < l; {
		b := bytes[i]
		if b == '_' {
			for j := i + 1; j < l; j++ {
				bytes[j-1] = bytes[j]
			}
			l--
			if i < l {
				b = bytes[i]
				if b >= 'a' && b <= 'z' {
					bytes[i] = b - 32
					i++
				}
			}
			updated = true
		} else {
			i++
		}
	}
	if updated {
		return string(bytes[:l])
	} else {
		return v
	}
}

func ToString(v any) string {
	switch v1 := v.(type) {
	case string:
		return v1

	case int:
		return strconv.FormatInt(int64(v1), 10)
	case int8:
		return strconv.FormatInt(int64(v1), 10)
	case int16:
		return strconv.FormatInt(int64(v1), 10)
	case int32:
		return strconv.FormatInt(int64(v1), 10)
	case int64:
		return strconv.FormatInt(v1, 10)

	case uint8:
		return strconv.FormatUint(uint64(v1), 10)
	case uint16:
		return strconv.FormatUint(uint64(v1), 10)
	case uint32:
		return strconv.FormatUint(uint64(v1), 10)
	case uint64:
		return strconv.FormatUint(v1, 10)

	case float32:
		return strconv.FormatFloat(float64(v1), 'f', -1, 32)
	case float64:
		return strconv.FormatFloat(v1, 'f', -1, 64)

	case bool:
		if v1 {
			return "true"
		} else {
			return "false"
		}
	case time.Time:
		return v1.String()

	default:
		return fmt.Sprint(v1)
	}
}

func ToStrings[T any](s []T) []string {
	if s == nil {
		return nil
	}
	out := make([]string, len(s))
	for i, v := range s {
		out[i] = ToString(v)
	}
	return out
}

func HashCode(v string) uint32 {
	var h uint32 = 0
	for i := 0; i < len(v); i++ {
		h = (h << 5) - h + uint32(v[i]) // h = 31*h + v[i]
	}
	return h
}

func Hash(v string) string {
	return strconv.FormatUint(uint64(HashCode(v)), 10)
}
