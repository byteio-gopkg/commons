package stringutil

import (
	"strings"
)

func Extract(v string, left string, right string) string {
	begin := strings.Index(v, left)
	if begin < 0 {
		return ""
	}
	begin += len(left)
	end := strings.Index(v[begin:], right)
	if end < 0 {
		return ""
	}
	return v[begin : begin+end]
}

func ExtractFully(v string, left string, right string, withBoundary bool) string {
	begin := strings.Index(v, left)
	if begin < 0 {
		return ""
	}
	if !withBoundary {
		begin += len(left)
	}
	end := strings.LastIndex(v, right)
	if end <= begin {
		return ""
	}
	if withBoundary {
		end += len(right)
	}
	return v[begin:end]
}
