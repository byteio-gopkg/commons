package sliceutil

//goland:noinspection ALL
var (
	EMPTY_ANYS    = make([]any, 0)
	EMPTY_STRINGS = make([]string, 0)
	EMPTY_INTS    = make([]int, 0)
)

func DeepClone[T any](s []*T) []*T {
	if s == nil {
		return nil
	}
	d := make([]*T, len(s))
	for i, e := range s {
		if e == nil {
			d[i] = nil
		} else {
			ne := *e
			d[i] = &ne
		}
	}
	return d
}

func AppendIfAbsent[T comparable](list []T, element T) []T {
	for _, v := range list {
		if v == element {
			return list
		}
	}
	return append(list, element)
}

func DeleteZero[T comparable](list ...T) []T {
	size := len(list)
	if size == 0 {
		return list
	}
	var zero T
	out := make([]T, 0, size)
	for _, v := range list {
		if v != zero {
			out = append(out, v)
		}
	}
	return out
}

func Deduplicate[T comparable](list []T) []T {
	if list == nil {
		return list
	}
	out := make([]T, 0, len(list))
	set := make(map[T]bool)
	for _, v := range list {
		if set[v] {
			continue
		}
		set[v] = true
		out = append(out, v)
	}
	return out
}

func Integers[T int32 | uint32 | int64 | uint64](s []int) []T {
	if s == nil {
		return nil
	}
	out := make([]T, len(s))
	for i, v := range s {
		out[i] = T(v)
	}
	return out
}

func Anys[T any](list []T) []any {
	if list == nil {
		return EMPTY_ANYS
	}
	ret := make([]any, len(list))
	for i, v := range list {
		ret[i] = v
	}
	return ret
}
