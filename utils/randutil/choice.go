package randutil

import "math/rand/v2"

func Choice[T any](all []T) (v T) {
	if total := len(all); total > 0 {
		v = all[rand.IntN(total)]
	}
	return
}
