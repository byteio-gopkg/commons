package randutil

import (
	"math/rand/v2"
	"time"
)

func Intn(n int) int {
	return rand.IntN(n)
}

func Int63n(n int64) int64 {
	return rand.Int64N(n)
}

func Duration(n time.Duration) time.Duration {
	return time.Duration(rand.Int64N(int64(n)))
}
