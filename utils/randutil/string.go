package randutil

import (
	"math/rand/v2"
)

//goland:noinspection ALL
var (
	alphanumericSeeds      = []byte("0123456789ABCDEFGHIGKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz")
	alphanumericUpperSeeds = []byte("0123456789ABCDEFGHIGKLMNOPQRSTUVWXYZ")
	alphanumericLowerSeeds = []byte("0123456789abcdefghigklmnopqrstuvwxyz")
	numericSeeds           = []byte("0123456789")
	alphabeticSeeds        = []byte("ABCDEFGHIGKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz")
)

func String(length int, seeds []byte) string {
	if length <= 0 {
		return ""
	}
	seedLength := len(seeds)
	dst := make([]byte, length)
	for i := 0; i < length; i++ {
		dst[i] = seeds[rand.IntN(seedLength)]
	}
	return string(dst)
}

func PutString(dst []byte, seeds []byte) {
	length := len(dst)
	seedLength := len(seeds)
	for i := 0; i < length; i++ {
		dst[i] = seeds[rand.IntN(seedLength)]
	}
}

func AlphanumericString(length int) string {
	return String(length, alphanumericSeeds)
}

func PutAlphanumericString(dst []byte) {
	PutString(dst, alphanumericSeeds)
}

func AlphanumericUpperString(length int) string {
	return String(length, alphanumericUpperSeeds)
}

func AlphanumericLowerString(length int) string {
	return String(length, alphanumericLowerSeeds)
}

func NumericString(length int) string {
	return String(length, numericSeeds)
}

func PutNumericString(dst []byte) {
	PutString(dst, numericSeeds)
}

func AlphabeticString(length int) string {
	return String(length, alphabeticSeeds)
}

func PutAlphabeticString(dst []byte) {
	PutString(dst, alphabeticSeeds)
}
