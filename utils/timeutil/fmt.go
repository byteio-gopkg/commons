package timeutil

import (
	"fmt"
	"time"
)

func Reformat(value string, fromFmt string, toFmt string) (string, error) {
	t, err := time.Parse(fromFmt, value)
	if err != nil {
		return "", err
	}
	return t.Format(toFmt), nil
}

func ParseDate(value string) (time.Time, error) {
	return time.ParseInLocation("2006-01-02", value, time.Local)
}

func ParseDateTime(value string) (time.Time, error) {
	return time.ParseInLocation("2006-01-02 15:04:05", value, time.Local)
}

// --------------------------------------------------------

func formatFill(b []byte, x int) {
	u := uint(x)
	i := len(b) - 1
	for ; i >= 0 && u >= 10; i-- {
		q := u / 10
		b[i] = byte('0' + u - q*10)
		u = q
	}
	if i < 0 {
		return
	}
	b[i] = byte('0' + u)
	for j := 0; j < i; j++ {
		b[j] = '0'
	}
}

func formatDateTime(t *time.Time, buf []byte) {
	year, month, day := t.Date()
	formatFill(buf[0:4], year)
	buf[4] = '-'
	formatFill(buf[5:7], int(month))
	buf[7] = '-'
	formatFill(buf[8:10], day)
	buf[10] = ' '
	hour, minute, sec := t.Clock()
	formatFill(buf[11:13], hour)
	buf[13] = ':'
	formatFill(buf[14:16], minute)
	buf[16] = ':'
	formatFill(buf[17:19], sec)
}

// FormatDateTime format to date time, is faster than time.Format
//
//	"FormatDateTime" cost 39.81 ns/op
//	"time.Format" cost 128.9 ns/op
func FormatDateTime(t time.Time) string {
	buf := make([]byte, 19)
	formatDateTime(&t, buf)
	return string(buf)
}

func FormatDateTimeBytes(t time.Time) []byte {
	buf := make([]byte, 19)
	formatDateTime(&t, buf)
	return buf
}

func FormatDateTimeMS(t time.Time) string {
	buf := make([]byte, 23)
	formatDateTime(&t, buf)
	buf[19] = '.'
	formatFill(buf[20:23], t.Nanosecond()/1e6)
	return string(buf)
}

// FormatDate format to date, is faster than time.Format
//
//	"FormatDate" cost 32 ns/op
//	"time.Format" cost 76 ns/op
func FormatDate(t time.Time) string {
	buf := make([]byte, 10)
	year, month, day := t.Date()
	formatFill(buf[0:4], year)
	buf[4] = '-'
	formatFill(buf[5:7], int(month))
	buf[7] = '-'
	formatFill(buf[8:10], day)
	return string(buf)
}

func FormatMiniDate(t time.Time) string {
	_, month, day := t.Date()
	return fmt.Sprintf("%d.%d", month, day)
}

func MinifyDate(date string) string {
	if len(date) >= 10 {
		month := date[5:7]
		if month[0] == '0' {
			month = month[1:]
		}
		day := date[8:10]
		if day[0] == '0' {
			day = day[1:]
		}
		return month + "." + day
	}
	return ""
}

// FormatDateNo format to date no, eg: 20250307
func FormatDateNo(t time.Time) string {
	buf := make([]byte, 8)
	year, month, day := t.Date()
	formatFill(buf[0:4], year)
	formatFill(buf[4:6], int(month))
	formatFill(buf[6:8], day)
	return string(buf)
}

// FormatHourNo format to date and hour no, eg: 2025030711
func FormatHourNo(t time.Time) string {
	buf := make([]byte, 10)
	year, month, day := t.Date()
	formatFill(buf[0:4], year)
	formatFill(buf[4:6], int(month))
	formatFill(buf[6:8], day)
	formatFill(buf[8:10], t.Hour())
	return string(buf)
}
