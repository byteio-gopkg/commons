package timeutil

import (
	"time"
	_ "unsafe"
)

//go:linkname RuntimeNano runtime.nanotime
func RuntimeNano() int64

// --------------------------------------------------------

func NilIf00(t time.Time) any {
	if t.IsZero() {
		return nil
	}
	return t
}

func ToAM00(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func CostSeconds(begin, end time.Time) float64 {
	return float64(end.UnixMilli()-begin.UnixMilli()) / 1000
}

func MillsToSeconds(mills int64) float64 {
	return float64(mills) / 1000
}

func DurationOfSeconds(seconds float64) time.Duration {
	return time.Duration(seconds * float64(time.Second))
}

func NowDate() string {
	return time.Now().Format("2006-01-02")
}

func NowDateTime() string {
	return FormatDateTime(time.Now())
}

func NowDateTimeMS() string {
	return FormatDateTimeMS(time.Now())
}

func AddDate(date string, deltaMonths, deltaDays int) string {
	t, err := time.ParseInLocation("2006-01-02", date, time.Local)
	if err != nil {
		return ""
	}
	return t.AddDate(0, deltaMonths, deltaDays).Format("2006-01-02")
}

// --------------------------------------------------------

var DOOMSDAY = time.Date(9999, 12, 31, 12, 0, 0, 0, time.UTC)

var isoWeekdays = map[time.Weekday]int{
	time.Monday:    1,
	time.Tuesday:   2,
	time.Wednesday: 3,
	time.Thursday:  4,
	time.Friday:    5,
	time.Saturday:  6,
	time.Sunday:    7,
}

func ISOWeekday(d time.Weekday) int {
	return isoWeekdays[d]
}

func parseISOWeekday(n any) time.Weekday {
	switch n {
	case 1, "1":
		return time.Monday
	case 2, "2":
		return time.Tuesday
	case 3, "3":
		return time.Wednesday
	case 4, "4":
		return time.Thursday
	case 5, "5":
		return time.Friday
	case 6, "6":
		return time.Saturday
	case 7, "7":
		return time.Sunday
	}
	return -1
}

func ParseISOWeekday[N string | int](n N) time.Weekday {
	return parseISOWeekday(n)
}
