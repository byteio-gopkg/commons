package smtputil

import (
	"crypto/tls"
	"errors"
	"net"
	"net/smtp"
	"strings"
)

// SendMail Send mail with tls options, reference net/smtp/SendMail
//
//goland:noinspection GoUnhandledErrorResult
func SendMail(addr string, a smtp.Auth, from string, to []string, msg []byte, insecureSkipVerify bool) error {
	if err := validateLine(from); err != nil {
		return err
	}
	for _, rcv := range to {
		if err := validateLine(rcv); err != nil {
			return err
		}
	}
	host, _, _ := net.SplitHostPort(addr)
	c, err := smtp.Dial(addr)
	if err != nil {
		return err
	}

	defer c.Close()
	if err = c.Hello("localhost"); err != nil {
		return err
	}
	if ok, _ := c.Extension("STARTTLS"); ok {
		config := &tls.Config{ServerName: host, InsecureSkipVerify: insecureSkipVerify}
		if err = c.StartTLS(config); err != nil {
			return err
		}
	}
	if a != nil {
		if ok, _ := c.Extension("AUTH"); !ok {
			return errors.New("smtp: server doesn't support AUTH")
		}
		if err = c.Auth(a); err != nil {
			return err
		}
	}
	if err = c.Mail(from); err != nil {
		return err
	}
	for _, _addr := range to {
		if err = c.Rcpt(_addr); err != nil {
			return err
		}
	}
	w, err := c.Data()
	if err != nil {
		return err
	}
	_, err = w.Write(msg)
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	return c.Quit()
}

func validateLine(line string) error {
	if strings.ContainsAny(line, "\n\r") {
		return errors.New("smtp: A line must not contain CR or LF")
	}
	return nil
}
