package httputil

import (
	"compress/gzip"
	"io"
	"net/http"
	"strings"
)

func CloseBody(resp *http.Response) {
	if resp != nil && resp.Body != nil {
		_ = resp.Body.Close()
	}
}

//goland:noinspection GoUnhandledErrorResult
func ConsumeAndCloseBody(resp *http.Response) error {
	if resp == nil {
		return nil
	}
	body := resp.Body
	if body == nil || body == http.NoBody {
		return nil
	}
	defer body.Close()
	buf := make([]byte, 4096)
	for {
		_, err := body.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				return err
			}
		}
	}
	return nil
}

func readAll(r io.Reader, contentLength int) ([]byte, error) {
	if contentLength <= 0 {
		contentLength = 4096
	}
	// FIXME: 此处性能需要优化, 可以加个buffer池, append扩容方式也并非x2, 而是缓慢扩容
	b := make([]byte, 0, contentLength)
	for {
		n, err := r.Read(b[len(b):cap(b)])
		b = b[:len(b)+n]
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			return b, err
		}
		if len(b) == cap(b) {
			b = append(b, 0)[:len(b)]
		}
	}
}

func ReadBody(resp *http.Response) ([]byte, error) {
	if resp == nil {
		return nil, nil
	}
	body := resp.Body
	if body == nil || body == http.NoBody {
		return nil, nil
	}
	encoding := resp.Header.Get("Content-Encoding")
	contentLength := int(resp.ContentLength)
	if encoding == "gzip" {
		var err error = nil
		// FIXME: 此处每次都创建giz, 存在一定的性能问题
		body, err = gzip.NewReader(body)
		if err != nil {
			return nil, err
		}
		if contentLength > 0 {
			contentLength *= 5
		}
	}
	return readAll(body, contentLength)
}

func ReadBodyAndClose(resp *http.Response) ([]byte, error) {
	b, err := ReadBody(resp)
	CloseBody(resp)
	return b, err
}

func ReadBodyAsString(resp *http.Response) string {
	body, _ := ReadBody(resp)
	if body != nil {
		return string(body)
	} else {
		return ""
	}
}

func NoRedirect(*http.Request, []*http.Request) error {
	return http.ErrUseLastResponse
}

func MatchPaths(target string, paths []string) bool {
	for _, path := range paths {
		if strings.HasPrefix(target, path) {
			pathLen := len(path)
			if len(target) == pathLen || target[pathLen] == '/' {
				return true
			}
		}
	}
	return false
}
