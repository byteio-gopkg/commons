package cookieutil

import (
	"net/http"
	"net/url"
	"strings"
)

func FindValue(cookies []*http.Cookie, name string) string {
	for _, cookie := range cookies {
		if cookie.Name == name {
			return cookie.Value
		}
	}
	return ""
}

func CollectAsMap(cookies []*http.Cookie, names ...string) map[string]string {
	nameIndex := make(map[string]bool)
	for _, name := range names {
		nameIndex[name] = true
	}
	out := make(map[string]string)
	for _, cookie := range cookies {
		if nameIndex[cookie.Name] {
			out[cookie.Name] = cookie.Value
		}
	}
	return out
}

func RequestHeader(cookies []*http.Cookie, names ...string) string {
	nameIndex := make(map[string]bool)
	for _, name := range names {
		nameIndex[name] = true
	}
	out := make([]string, 0)
	for _, cookie := range cookies {
		if nameIndex[cookie.Name] {
			out = append(out, cookie.Name+"="+cookie.Value)
		}
	}
	return strings.Join(out, "; ")
}

func RequestHeaderWithMap(cookies map[string]string) string {
	out := make([]string, 0)
	for name, value := range cookies {
		out = append(out, name+"="+value)
	}
	return strings.Join(out, "; ")
}

func RequestHeaderWithJar(jar http.CookieJar, requestUrl string) string {
	u, _ := url.Parse(requestUrl)
	if u == nil {
		return ""
	}
	out := make([]string, 0)
	for _, cookie := range jar.Cookies(u) {
		out = append(out, cookie.Name+"="+cookie.Value)
	}
	return strings.Join(out, "; ")
}

func ParseRequestHeader(value string) []*http.Cookie {
	cookies := make([]*http.Cookie, 0)
	for _, ckv := range strings.Split(value, ";") {
		_ckv := strings.SplitN(ckv, "=", 2)
		if len(_ckv) == 2 {
			cookies = append(cookies, &http.Cookie{
				Name:  strings.TrimSpace(_ckv[0]),
				Value: strings.TrimSpace(_ckv[1]),
			})
		}
	}
	return cookies
}
