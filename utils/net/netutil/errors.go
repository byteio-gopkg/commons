package netutil

import (
	"net"
	"net/url"
)

//goland:noinspection GoTypeAssertionOnErrors
func IsNetError(err error) bool {
	if _, ok := err.(net.Error); ok {
		return true
	}
	return false
}

//goland:noinspection GoTypeAssertionOnErrors
func IsNetTimeout(err error) bool {
	if netErr, ok := err.(net.Error); ok {
		return netErr.Timeout()
	}
	return false
}

//goland:noinspection GoTypeAssertionOnErrors
func UnwrapIfUrlError(err error) error {
	if urlErr, ok := err.(*url.Error); ok {
		return urlErr.Err
	}
	return err
}
