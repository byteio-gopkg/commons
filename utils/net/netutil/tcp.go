package netutil

import (
	"net"
	"time"
)

func TestTCPReachable(address string, timeout time.Duration) bool {
	con, err := net.DialTimeout("tcp", address, timeout)
	if err != nil {
		return false
	}
	_ = con.Close()
	return true
}
