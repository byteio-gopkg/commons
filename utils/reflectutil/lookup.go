package reflectutil

import (
	"reflect"
	"unsafe"
)

// _Interface go interface internal layout
type _Interface struct {
	Type  unsafe.Pointer
	Value unsafe.Pointer
}

//go:linkname typelinks reflect.typelinks
func typelinks() (sections []unsafe.Pointer, offset [][]int32)

//go:linkname resolveTypeOff reflect.resolveTypeOff
func resolveTypeOff(rtype unsafe.Pointer, off int32) unsafe.Pointer

// LookupType returns the type whose name property equals to the given name, or nil if not found.
// see reflect.typesByString
func LookupType(name string) reflect.Type {
	key := name
	if len(name) == 0 || name[0] != '*' {
		key = "*" + key
	}

	// type interface.value -> rtype
	typ := reflect.TypeOf(0)
	typI := (*_Interface)(unsafe.Pointer(&typ))

	// find target through binary search algorithm
	sections, offset := typelinks()
	for offsI, offs := range offset {
		section := sections[offsI]
		i, j := 0, len(offs)
		for i < j {
			h := i + (j-i)/2
			typI.Value = resolveTypeOff(section, offs[h])
			if !(typ.String() >= key) {
				i = h + 1
			} else {
				j = h
			}
		}

		// i == j, f(i-1) == false, and f(j) (= f(i)) == true  =>  answer is i.
		// Having found the first, linear scan forward to find the last.
		// We could do a second binary search, but the caller is going
		// to do a linear scan anyway.
		if i < len(offs) {
			typI.Value = resolveTypeOff(section, offs[i])
			if typ.Kind() == reflect.Ptr {
				if typ.String() == name {
					return typ
				}
				elem := typ.Elem()
				if elem.String() == name {
					return elem
				}
			}
		}
	}
	return nil
}
