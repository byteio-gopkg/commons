package cryptoutil

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
	"os"
)

func FileSha256Hex(path string) string {
	file, _ := os.Open(path)
	if file == nil {
		return ""
	}
	defer file.Close()
	hash := sha256.New()
	if _, err := io.Copy(hash, file); err != nil {
		return ""
	}
	return hex.EncodeToString(hash.Sum(nil))
}
