package cryptoutil

import (
	"crypto/aes"
)

func AesCBCEncrypt(raw, key, iv []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	return CBCEncrypt(raw, block, iv), err
}

func AesCBCDecrypt(raw, key, iv []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	return CBCDecrypt(raw, block, iv), nil
}

func NewAesCBCCipher(key []byte) (*CBCCipher, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	return &CBCCipher{block}, nil
}

func AesECBEncrypt(raw, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	return ECBEncrypt(raw, block), nil
}

func AesECBDecrypt(raw, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	return ECBDecrypt(raw, block), nil
}

func NewAesGCMCipher(key []byte) (*GCMCipher, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	return NewGCMCipher(block)
}
