package cryptoutil

import (
	"crypto/md5"
	"encoding/hex"
)

func Md5Hex[Bytes []byte | string](data Bytes) string {
	md := md5.New()
	md.Write([]byte(data))
	return hex.EncodeToString(md.Sum(nil))
}

func Hash16[Bytes []byte | string](data Bytes) string {
	b := md5.Sum([]byte(data))
	return hex.EncodeToString(b[:])[8:24]
}
