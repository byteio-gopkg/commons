package cryptoutil

import (
	impl "byteio.org/commons/internal/crypto/rsa"
	"crypto/rsa"
	"hash"
	"io"
	_ "unsafe"
)

// EncryptOAEP encrypts the given message with RSA-OAEP and msg hash.
func EncryptOAEP(hash hash.Hash, mgfHash hash.Hash, random io.Reader, pub *rsa.PublicKey, msg []byte, label []byte) ([]byte, error) {
	return impl.EncryptOAEP(hash, mgfHash, random, pub, msg, label)
}
