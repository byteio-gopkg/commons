package cryptoutil

import (
	"bytes"
	"crypto/cipher"
)

func PKCS7Padding(cipherText []byte, blockSize int) []byte {
	padding := blockSize - len(cipherText)%blockSize
	paddingText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(cipherText, paddingText...)
}

func PKCS7UnPadding(cipherText []byte) []byte {
	length := len(cipherText)
	if length < 1 {
		return cipherText
	}
	padLen := int(cipherText[length-1])
	if padLen >= 0 && padLen <= length {
		return cipherText[:(length - padLen)]
	}
	return cipherText
}

// --------------------------------------------------------

func CBCEncrypt(raw []byte, block cipher.Block, iv []byte) []byte {
	mode := cipher.NewCBCEncrypter(block, iv)
	raw = PKCS7Padding(raw, block.BlockSize())
	encrypted := make([]byte, len(raw))
	mode.CryptBlocks(encrypted, raw)
	return encrypted
}

func CBCDecrypt(raw []byte, block cipher.Block, iv []byte) []byte {
	mode := cipher.NewCBCDecrypter(block, iv)
	decrypted := make([]byte, len(raw))
	mode.CryptBlocks(decrypted, raw)
	return PKCS7UnPadding(decrypted)
}

type CBCCipher struct {
	Block cipher.Block
}

func (c *CBCCipher) Encrypt(raw, iv []byte) []byte {
	return CBCEncrypt(raw, c.Block, iv)
}

func (c *CBCCipher) Decrypt(raw, iv []byte) []byte {
	return CBCDecrypt(raw, c.Block, iv)
}

func ECBEncrypt(raw []byte, block cipher.Block) []byte {
	blockSize := block.BlockSize()
	raw = PKCS7Padding(raw, blockSize)
	encrypted := make([]byte, len(raw))
	for bs, be := 0, blockSize; bs < len(raw); bs, be = bs+blockSize, be+blockSize {
		block.Encrypt(encrypted[bs:be], raw[bs:be])
	}
	return encrypted
}

func ECBDecrypt(raw []byte, block cipher.Block) []byte {
	blockSize := block.BlockSize()
	decrypted := make([]byte, len(raw))
	for bs, be := 0, blockSize; bs < len(raw); bs, be = bs+blockSize, be+blockSize {
		block.Decrypt(decrypted[bs:be], raw[bs:be])
	}
	return PKCS7UnPadding(decrypted)
}

type GCMCipher struct {
	block     cipher.Block
	nonceSize int
}

func NewGCMCipher(block cipher.Block) (*GCMCipher, error) {
	c, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	return &GCMCipher{block, c.NonceSize()}, nil
}

func (c *GCMCipher) NonceSize() int {
	return c.nonceSize
}

func (c *GCMCipher) Encrypt(data, nonce, dst []byte) []byte {
	aesGcm, _ := cipher.NewGCM(c.block)
	return aesGcm.Seal(dst, nonce, data, nil)
}

func (c *GCMCipher) Decrypt(data, nonce, dst []byte) []byte {
	aesGcm, _ := cipher.NewGCM(c.block)
	out, _ := aesGcm.Open(dst, nonce, data, nil)
	return out
}
