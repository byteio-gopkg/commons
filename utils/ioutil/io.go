package ioutil

import (
	"io"
)

func Close(closer io.Closer) {
	if closer != nil {
		_ = closer.Close()
	}
}

func CloseAll(closers ...io.Closer) {
	for _, closer := range closers {
		Close(closer)
	}
}

func ReadFully(r io.Reader, buf []byte) error {
	offset := 0
	bufLen := len(buf)
	for {
		read, err := r.Read(buf[offset:])
		if err != nil {
			return err
		}
		offset += read
		if offset >= bufLen {
			return nil
		}
	}
}

func WriteFully(r io.Writer, buf []byte) error {
	offset := 0
	bufLen := len(buf)
	for {
		written, err := r.Write(buf[offset:])
		if err != nil {
			return err
		}
		offset += written
		if offset >= bufLen {
			return nil
		}
	}
}
