package maputil

func Keys[K comparable, V any](m map[K]V) []K {
	keys := make([]K, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	return keys
}

func Values[K comparable, V any](m map[K]V) []V {
	values := make([]V, 0, len(m))
	for _, v := range m {
		values = append(values, v)
	}
	return values
}

func Value[V any](m map[string]any, key string) V {
	var v V
	v0, ok := m[key]
	if ok {
		v, _ = v0.(V)
	}
	return v
}

func Copy[K comparable, V any](m map[K]V) map[K]V {
	if m == nil {
		return nil
	}
	nm := make(map[K]V, len(m))
	for k, v := range m {
		nm[k] = v
	}
	return nm
}

func PutAll[K comparable, V any](target, items map[K]V) map[K]V {
	if target == nil {
		return nil
	}
	for k, v := range items {
		target[k] = v
	}
	return target
}

func Extend[K comparable](dest map[K]any, source map[K]any) map[K]any {
	if dest == nil && source != nil {
		dest = make(map[K]any)
	}
	for k, v := range source {
		if _, ok := dest[k]; !ok {
			dest[k] = v
		}
	}
	return dest
}

func PutIfNot[V comparable](m map[string]any, k string, v V, not V) {
	if m != nil && v != not {
		m[k] = v
	}
}

func Extracts[K comparable, V any](s map[K]V, keys ...K) map[K]V {
	if s == nil {
		return nil
	}
	d := map[K]V{}
	for _, k := range keys {
		if v, ok := s[k]; ok {
			d[k] = v
		}
	}
	return d
}
