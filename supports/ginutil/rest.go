package ginutil

import (
	. "byteio.org/commons"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"reflect"
	"strings"
)

func IdParam(c *gin.Context) string {
	id0 := c.Param("id")
	if id0 == "" {
		c.String(http.StatusBadRequest, "ID required")
		return ""
	}
	if len(id0) > 500 {
		c.String(http.StatusBadRequest, "invalid ID '"+id0+"'")
		return ""
	}
	return id0
}

func IdsParam(c *gin.Context, limit int) []string {
	idV := c.Param("id")
	if idV == "" {
		c.String(http.StatusBadRequest, "ID required")
		return nil
	}
	idList0 := strings.Split(idV, ",")
	idLen := len(idList0)
	if idLen == 0 {
		c.String(http.StatusBadRequest, "ID required")
		return nil
	}
	if idLen > limit {
		c.String(http.StatusBadRequest, "maximum of "+fmt.Sprint(limit)+" IDs allowed")
		return nil
	}
	idList := make([]string, 0, idLen)
	for _, id0 := range idList0 {
		if len(id0) == 0 {
			continue
		}
		if len(id0) > 500 {
			c.String(http.StatusBadRequest, "invalid ID '"+id0+"'")
			return nil
		}
		idList = append(idList, id0)
	}
	if len(idList) == 0 {
		c.String(http.StatusBadRequest, "ID required")
		return nil
	}
	return idList
}

func IntIdParam0[T Int](c *gin.Context, key string) T {
	id0 := c.Param(key)
	if id0 == "" {
		c.String(http.StatusBadRequest, key+" required")
		return 0
	}
	id, _ := Atoi[T](id0)
	if id == 0 {
		c.String(http.StatusBadRequest, "invalid "+key+" '"+id0+"'")
		return 0
	}
	return id
}

func IntIdParam[T Int](c *gin.Context) T {
	id0 := c.Param("id")
	if id0 == "" {
		c.String(http.StatusBadRequest, "ID required")
		return 0
	}
	id, _ := Atoi[T](id0)
	if id == 0 {
		c.String(http.StatusBadRequest, "invalid ID '"+id0+"'")
		return 0
	}
	return id
}

func IntIdsParam0[T Int](c *gin.Context, key string, limit int) []T {
	idV := c.Param(key)
	if idV == "" {
		c.String(http.StatusBadRequest, key+" required")
		return nil
	}
	idList0 := strings.Split(idV, ",")
	idLen := len(idList0)
	if idLen == 0 {
		c.String(http.StatusBadRequest, key+" required")
		return nil
	}
	if idLen > limit {
		c.String(http.StatusBadRequest, "maximum of "+fmt.Sprint(limit)+" "+key+" allowed")
		return nil
	}
	idList := make([]T, idLen)
	for i, id0 := range idList0 {
		id, _ := Atoi[T](id0)
		if id == 0 {
			c.String(http.StatusBadRequest, "invalid "+key+" '"+id0+"'")
			return nil
		}
		idList[i] = id
	}
	return idList
}

func IntIdsParam[T Int](c *gin.Context, limit int) []T {
	return IntIdsParam0[T](c, "id", limit)
}

func CompositeIntIdParam[T Int](c *gin.Context, n int) []T {
	id0 := IdParam(c)
	if id0 == "" {
		return nil
	}
	parts := strings.Split(id0, "-")
	if len(parts) != n {
		c.String(http.StatusBadRequest, "invalid ID '"+id0+"'")
		return nil
	}
	out := make([]T, n)
	for i, v0 := range parts {
		if v0 == "" {
			c.String(http.StatusBadRequest, "ID required")
			return nil
		}
		id, _ := Atoi[T](v0)
		if id == 0 {
			c.String(http.StatusBadRequest, "invalid ID '"+id0+"'")
			return nil
		}
		out[i] = id
	}
	return out
}

// --------------------------------------------------------

type Id interface {
	string | Int
}

func restIdParam(c *gin.Context, zero any) any {
	switch zero.(type) {
	case int:
		id := IntIdParam[int](c)
		if id == 0 {
			return nil
		}
		return id
	case int32:
		id := IntIdParam[int32](c)
		if id == 0 {
			return nil
		}
		return id
	case int64:
		id := IntIdParam[int64](c)
		if id == 0 {
			return nil
		}
		return id
	case string:
		id := IdParam(c)
		if id == "" {
			return nil
		}
		return id
	}
	return nil
}

func restGetNotFound(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.Map, reflect.Slice, reflect.Pointer:
		return v.IsNil()
	case reflect.String:
		return v.Len() == 0
	}
	return false
}

func RestGetResult(c *gin.Context, ret any) {
	if ret == nil || restGetNotFound(reflect.ValueOf(ret)) {
		c.Status(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, ret)
	}
}

func RestGet[ID Id, RET any](c *gin.Context, impl func(id ID) RET) {
	id0 := restIdParam(c, Zero[ID]())
	if id0 == nil {
		return
	}
	id := id0.(ID)
	ret := impl(id)
	RestGetResult(c, ret)
}

func RestGetH[ID Id, RET any](impl func(id ID) RET) gin.HandlerFunc {
	return func(c *gin.Context) {
		RestGet(c, impl)
	}
}

func RestGetO[ID Id, RET any](c *gin.Context, impl func(id ID, ownerId int32) RET, ownerIdFunc func(c *gin.Context) int32) {
	id0 := restIdParam(c, Zero[ID]())
	if id0 == nil {
		return
	}
	id := id0.(ID)
	ret := impl(id, ownerIdFunc(c))
	RestGetResult(c, ret)
}

func RestGetOH[ID Id, RET any](impl func(id ID, ownerId int32) RET, ownerIdFunc func(c *gin.Context) int32) gin.HandlerFunc {
	return func(c *gin.Context) {
		RestGetO(c, impl, ownerIdFunc)
	}
}

func RestIH[ID Int](impl func(id ID, c *gin.Context)) gin.HandlerFunc {
	return func(c *gin.Context) {
		id := IntIdParam[ID](c)
		if id != 0 {
			impl(id, c)
		}
	}
}

func RestSET[ID Id](c *gin.Context, impl func(c *gin.Context, id ID)) {
	var id ID
	if c.Request.Method == http.MethodPut {
		id0 := restIdParam(c, id)
		if id0 == nil {
			return
		}
		id = id0.(ID)
	}
	impl(c, id)
}

func HandleRestSET[ID Id](r gin.IRoutes, path string, impl func(c *gin.Context, id ID)) {
	h := func(c *gin.Context) {
		RestSET(c, impl)
	}
	r.POST(path, h)
	r.PUT(path+"/:id", h)
}
