package ginutil

import (
	"github.com/gin-gonic/gin"
	"net"
	"strconv"
)

func ClientIpOf(c *gin.Context) string {
	ip := c.GetHeader("X-Real-IP")
	if ip == "" {
		ip, _, _ = net.SplitHostPort(c.Request.RemoteAddr)
	}
	return ip
}

func SetCacheControl(c *gin.Context, maxAge string) {
	c.Header("Cache-Control", "max-age="+maxAge)
}

// --------------------------------------------------------

type Int interface {
	int | int32 | int64
}

//goland:noinspection SpellCheckingInspection
func Atoi[T Int](s string) (T, bool) {
	if strconv.IntSize == 64 {
		v, err := strconv.Atoi(s)
		if err != nil {
			return 0, false
		} else {
			return T(v), true
		}
	} else {
		var zero T
		switch any(zero).(type) {
		case int, int32:
			v, err := strconv.Atoi(s)
			if err != nil {
				return 0, false
			} else {
				return T(v), true
			}
		}
		v, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			return 0, false
		} else {
			return T(v), true
		}
	}
}

func intValue[T Int](v0 string) T {
	v, ok := Atoi[T](v0)
	if !ok {
		return 0
	}
	return v
}

func intValueC[T Int](v0 string, min T, max T) (T, bool) {
	v, ok := Atoi[T](v0)
	if !ok {
		return 0, false
	}
	if v < min || v > max {
		return 0, false
	}
	return v, true
}

// --------------------------------------------------------

func IntQuery[T Int](c *gin.Context, name string) T {
	return intValue[T](c.Query(name))
}

func IntQueryC[T Int](c *gin.Context, name string, defaultValue T, min T, max T) (T, bool) {
	v0 := c.Query(name)
	if v0 == "" {
		return defaultValue, true
	}
	return intValueC(v0, min, max)
}

func BoolQuery(c *gin.Context, name string) bool {
	return c.Query(name) == "true"
}

func PostFormIntValue[T Int](c *gin.Context, name string) T {
	return intValue[T](c.PostForm(name))
}

func PostFormIntValueC[T Int](c *gin.Context, name string, defaultValue T, min T, max T) (T, bool) {
	v0 := c.PostForm(name)
	if v0 == "" {
		return defaultValue, true
	}
	return intValueC(v0, min, max)
}

func PostFormBoolValue(c *gin.Context, name string) bool {
	return c.PostForm(name) == "true"
}

func PostFormBoolValueC(c *gin.Context, name string, out *bool) bool {
	v0 := c.PostForm(name)
	if v0 == "" {
		return false
	}
	if v0 == "true" {
		*out = true
	} else if v0 == "false" {
		*out = false
	} else {
		return false
	}
	return true
}
