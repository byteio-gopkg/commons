package ginutil

import (
	"byteio.org/commons/x/data"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"math"
	"net/http"
	"strings"
)

func PaginationFormQuery(c *gin.Context) *data.PaginationForm {
	form := &data.PaginationForm{}
	var ok bool
	if form.Page, ok = IntQueryC(c, "page", 1, 1, math.MaxInt); !ok {
		return nil
	}
	if form.Size, ok = IntQueryC(c, "size", 50, 1, 5000); !ok {
		return nil
	}
	fv := c.Query("filter")
	if fv != "" && fv != "{}" {
		filter := make(map[string]any)
		if json.Unmarshal([]byte(fv), &filter) != nil {
			return nil
		}
		for k, v := range filter {
			if v == nil {
				delete(filter, k)
				continue
			}
			if v1, ok1 := v.(string); ok1 {
				v1 = strings.TrimSpace(v1)
				if v1 == "" {
					delete(filter, k)
				} else {
					filter[k] = v
				}
			}
		}
		form.Filter = filter
	}
	fv = c.Query("sort")
	if fv != "" && fv != "{}" {
		sort := make(map[string]int)
		if json.Unmarshal([]byte(fv), &sort) != nil {
			return nil
		}
		for k, v := range sort {
			if v != -1 && v != 1 {
				delete(sort, k)
			}
		}
		form.Sort = sort
	}
	form.Offset = (form.Page - 1) * form.Size
	form.ListOnly = c.Query("listOnly") == "true"
	form.Projection = c.Query("projection")
	return form
}

const contextKeyPaginationForm = "PaginationForm"

func PaginationFormOf(c *gin.Context) *data.PaginationForm {
	if resolved, ok := c.Get(contextKeyPaginationForm); ok {
		return resolved.(*data.PaginationForm)
	}
	form := PaginationFormQuery(c)
	if form == nil {
		c.String(http.StatusBadRequest, "invalid page parameters")
		return nil
	}
	return form
}

type PagePrepareFunc = func(form *data.PaginationForm, c *gin.Context)

func PagePrepare(h PagePrepareFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		form := PaginationFormOf(c)
		if form == nil {
			c.Abort()
			return
		}
		h(form, c)
		if c.Writer.Status() != http.StatusOK {
			c.Abort()
			return
		}
		c.Set(contextKeyPaginationForm, form)
	}
}

func Page(c *gin.Context, impl func(form *data.PaginationForm) *data.Pagination, prepares ...PagePrepareFunc) {
	form := PaginationFormOf(c)
	if form == nil {
		return
	}
	if len(prepares) > 0 {
		for _, pre := range prepares {
			pre(form, c)
			if c.Writer.Status() != http.StatusOK {
				return
			}
		}
	}
	ret := impl(form)
	if ret != nil {
		c.JSON(http.StatusOK, ret)
	} else {
		c.Status(http.StatusInternalServerError)
	}
}

func PageH(impl func(form *data.PaginationForm) *data.Pagination, prepares ...PagePrepareFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		Page(c, impl, prepares...)
	}
}

func PagePFI[ID Int](filterKey string) gin.HandlerFunc {
	return func(c *gin.Context) {
		id := IntIdParam[ID](c)
		if id == 0 {
			c.Abort()
			return
		}
		form := PaginationFormOf(c)
		if form == nil {
			c.Abort()
			return
		}
		form.PutFilter(filterKey, id)
		c.Set(contextKeyPaginationForm, form)
	}
}
