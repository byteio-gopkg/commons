package ginutil

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func ServeStatic(router gin.IRouter, urlPrefix, root string, withSAP bool) {
	handleFunc := func(c *gin.Context) {
		serveStatic(c, urlPrefix, root, withSAP)
	}
	router.GET(urlPrefix+"/*filepath", handleFunc)
	router.HEAD(urlPrefix+"/*filepath", handleFunc)
}

func serveStatic(c *gin.Context, urlPrefix, root string, withSAP bool) {
	upath := c.Param("filepath")
	if upath == "/index.html" {
		c.Request.URL.Path = urlPrefix // prevent redirect to / in http.ServeFile
	} else if upath == "/" {
		upath = "/index.html"
	}
	filename := filepath.Join(root, upath)
	fstat, err := os.Stat(filename)
	if err != nil || fstat.IsDir() {
		if withSAP && os.IsNotExist(err) && isSAPRoute(c, upath) {
			filename = filepath.Join(root, "index.html")
		} else {
			serveNotFound(c)
			return
		}
	}
	http.ServeFile(c.Writer, c.Request, filename)
}

var sapSuffixSet = map[string]bool{
	".html":  true,
	".htm":   true,
	".shtml": true,
	".xhtml": true,
}

func isSAPRoute(c *gin.Context, path string) bool {
	suffix := filepath.Ext(path)
	return (suffix == "" || sapSuffixSet[suffix]) &&
		strings.Contains(c.GetHeader("Accept"), "text/html") &&
		c.GetHeader("X-Request-With") == ""
}

func serveNotFound(c *gin.Context) {
	w := c.Writer
	w.WriteHeader(http.StatusNotFound)
	h := w.Header()
	h.Set("Content-Type", "text/plain; charset=utf-8")
	_, _ = w.WriteString("404 page not found")
}
