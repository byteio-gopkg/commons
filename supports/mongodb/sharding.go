package mongodb

import (
	"byteio.org/commons/utils/timeutil"
	"encoding/binary"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"runtime"
	"sync"
	"time"
	"weak"
)

type collectionSharding struct {
	database *mongo.Database
	options  []*options.CollectionOptions
	cache    map[string]weak.Pointer[mongo.Collection]
	cacheMu  sync.Mutex
}

func (s *collectionSharding) init(database *mongo.Database, options []*options.CollectionOptions) {
	s.database = database
	s.options = options
	s.cache = make(map[string]weak.Pointer[mongo.Collection])
}

func (s *collectionSharding) Database() *mongo.Database {
	return s.database
}

func (s *collectionSharding) WithName(name string) *mongo.Collection {
	s.cacheMu.Lock()
	c := s.cache[name].Value()
	if c == nil {
		c = s.database.Collection(name, s.options...)
		s.cache[name] = weak.Make(c)
		runtime.AddCleanup(c, s.removeCache, name)
	}
	s.cacheMu.Unlock()
	return c
}

func (s *collectionSharding) removeCache(name string) {
	s.cacheMu.Lock()
	delete(s.cache, name)
	s.cacheMu.Unlock()
}

type DateCollection struct {
	collectionSharding
	prefix string
}

func NewDateCollection(database *mongo.Database, prefix string, options ...*options.CollectionOptions) *DateCollection {
	d := &DateCollection{
		prefix: prefix,
	}
	d.init(database, options)
	return d
}

func (s *DateCollection) WithId(id primitive.ObjectID) *mongo.Collection {
	date := timeutil.FormatDateNo(time.Unix(int64(binary.BigEndian.Uint32(id[0:4])), 0))
	return s.WithName(s.prefix + date)
}

func (s *DateCollection) WithTime(time time.Time) *mongo.Collection {
	date := timeutil.FormatDateNo(time)
	return s.WithName(s.prefix + date)
}
