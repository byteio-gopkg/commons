package wsa

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
)

func MarshalJSON(v any) []byte {
	buf := bytes.NewBuffer([]byte{})
	encoder := json.NewEncoder(buf)
	encoder.SetEscapeHTML(false)
	err := encoder.Encode(v)
	if err != nil {
		return nil
	}
	out := buf.Bytes()
	outSize := len(out)
	if outSize > 0 && out[outSize-1] == '\n' {
		return out[:outSize-1]
	}
	return out
}

var UnmarshalJSONFunc = json.Unmarshal

func UnmarshalJSON(data []byte, v any) error {
	err := UnmarshalJSONFunc(data, v)
	if err != nil {
		body := string(data)
		if len(body) > 200 {
			body = body[:200] + "..."
		}
		return errors.New(fmt.Sprintf("malformed json: %s, body: %s", err, body))
	}
	return nil
}

func ReturnJSON[T any](b []byte, err error) (T, error) {
	var ret T
	if err != nil {
		return ret, err
	}
	if err = UnmarshalJSON(b, &ret); err != nil {
		var zero T
		return zero, err
	}
	return ret, nil
}
