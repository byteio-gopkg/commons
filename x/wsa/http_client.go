package wsa

import (
	"byteio.org/commons/utils/net/httputil"
	"bytes"
	"context"
	"crypto/tls"
	"io"
	"net"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"time"
)

type HeaderMap map[string]string

var httpClientHeader = http.Header{
	"Accept":          []string{"*/*"},
	"Accept-Encoding": []string{"gzip"},
	"Accept-Language": []string{"en-US,en"},
	"Connection":      []string{"keep-alive"},
	"User-Agent":      []string{"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:130.0) Gecko/20100101 Firefox/130.0"},
}

// --------------------------------------------------------

var (
	noProxyDialer = net.Dialer{
		Timeout: 15 * time.Second,
	}
	proxyDialer = net.Dialer{
		Timeout: 3 * time.Second,
	}
)

func NewHttpClient(maxConnectionsPerHost int) *HttpClient {
	c := &HttpClient{
		Client: &http.Client{
			Transport: &http.Transport{
				MaxIdleConns:        0,
				MaxConnsPerHost:     maxConnectionsPerHost,
				MaxIdleConnsPerHost: maxConnectionsPerHost,
				IdleConnTimeout:     90 * time.Second,
				Proxy:               getProxyFromContext,
				TLSNextProto:        map[string]func(authority string, c *tls.Conn) http.RoundTripper{},
				DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
					if mc, ok := ctx.(*requestContext); ok && mc.proxy != nil {
						return proxyDialer.DialContext(ctx, network, addr)
					}
					return noProxyDialer.DialContext(ctx, network, addr)
				},
			},
		},
		header: httpClientHeader,
	}
	return c
}

type HttpClient struct {
	*http.Client
	header http.Header
	Proxy  *url.URL
}

func (c *HttpClient) NewUse(configurable bool) *HttpClient {
	hc := c.Client
	if configurable {
		hc0 := *hc
		hc = &hc0
	}
	nc := *c
	nc.Client = hc
	return &nc
}

func (c *HttpClient) WithHeader(header HeaderMap) *HttpClient {
	if header == nil {
		c.header = nil
	} else {
		c.header = make(http.Header)
		for hk, hv := range header {
			c.header[hk] = []string{hv}
		}
	}
	return c
}

func (c *HttpClient) SetHeader(name string, value string) {
	if c.header == nil {
		c.header = make(http.Header)
	}
	c.header[name] = []string{value}
}

func (c *HttpClient) SetUserAgent(userAgent string) {
	c.SetHeader("User-Agent", userAgent)
}

func (c *HttpClient) SetProxy(proxy *url.URL) {
	c.Proxy = proxy
}

func (c *HttpClient) NewWithCheckRedirect(checkRedirect func(req *http.Request, via []*http.Request) error) *HttpClient {
	hc := c.NewUse(true)
	hc.CheckRedirect = checkRedirect
	return hc
}

func (c *HttpClient) NewWithJar() *HttpClient {
	hc := c.NewUse(true)
	hc.Jar, _ = cookiejar.New(nil)
	return hc
}

func (c *HttpClient) SkipInsecureVerify() {
	transport := c.Transport.(*http.Transport)
	if ts := transport.TLSClientConfig; ts == nil {
		transport.TLSClientConfig = &tls.Config{
			InsecureSkipVerify: true,
		}
	} else {
		ts.InsecureSkipVerify = true
	}
}

func (c *HttpClient) Request(method string, url string, header HeaderMap, body []byte, assert20x bool, timeout time.Duration) (*http.Response, error) {
	ctx := newRequestContext(timeout, c.Proxy)
	var requestBody io.Reader = nil
	if body != nil {
		requestBody = bytes.NewReader(body)
	}
	req, err := http.NewRequestWithContext(ctx, method, url, requestBody)
	if err != nil {
		return nil, err
	}
	for hk, hv := range c.header {
		req.Header[hk] = hv
	}
	for hk, hv := range header {
		req.Header[hk] = []string{hv}
	}
	if host := req.Header["Host"]; len(host) > 0 {
		req.Host = host[0]
	}

	var resp *http.Response
	resp, err = c.Do(req)
	if mc, ok := ctx.(*requestContext); ok {
		mc.Release()
	}
	if assert20x && err == nil {
		err = AssertHttp20x(resp)
	}
	if err != nil {
		httputil.CloseBody(resp)
	}
	return resp, err
}

func (c *HttpClient) RequestAndReadBody(method string, url string, header HeaderMap, body []byte, assert20x bool, timeout time.Duration) ([]byte, error) {
	resp, err := c.Request(method, url, header, body, assert20x, timeout)
	if err != nil {
		return nil, err
	}
	return httputil.ReadBodyAndClose(resp)
}

func (c *HttpClient) RequestGET(url string, header HeaderMap, timeout time.Duration) (*http.Response, error) {
	return c.Request(http.MethodGet, url, header, nil, true, timeout)
}

func (c *HttpClient) RequestPOST(url string, header HeaderMap, body []byte, timeout time.Duration) (*http.Response, error) {
	return c.Request(http.MethodPost, url, header, body, true, timeout)
}

func (c *HttpClient) GET(url string, header HeaderMap, timeout time.Duration) ([]byte, error) {
	return c.RequestAndReadBody(http.MethodGet, url, header, nil, true, timeout)
}

func (c *HttpClient) POST(url string, header HeaderMap, body []byte, timeout time.Duration) ([]byte, error) {
	return c.RequestAndReadBody(http.MethodPost, url, header, body, true, timeout)
}

func (c *HttpClient) PUT(url string, header HeaderMap, body []byte, timeout time.Duration) ([]byte, error) {
	return c.RequestAndReadBody(http.MethodPut, url, header, body, true, timeout)
}

func (c *HttpClient) POSTJson(url string, header HeaderMap, body any, timeout time.Duration) ([]byte, error) {
	if header == nil {
		header = HeaderMap{}
	}
	header["Content-Type"] = "application/json; charset=utf-8"
	return c.RequestAndReadBody(http.MethodPost, url, header, MarshalJSON(body), true, timeout)
}

func (c *HttpClient) RequestForPing(method string, url string, header HeaderMap, body []byte, timeout time.Duration) (int, error) {
	resp, err := c.Request(method, url, header, body, false, timeout)
	if err != nil {
		return 0, err
	}
	err = httputil.ConsumeAndCloseBody(resp)
	return resp.StatusCode, err
}

func (c *HttpClient) RequestForEstablishNetwork(url string, header HeaderMap, timeout time.Duration) error {
	if header == nil {
		header = map[string]string{
			"Connection": "keep-alive",
		}
	} else if _, ok := header["Connection"]; !ok {
		header["Connection"] = "keep-alive"
	}
	resp, err := c.Request(http.MethodHead, url, header, nil, false, timeout)
	if resp != nil {
		_ = httputil.ConsumeAndCloseBody(resp)
	}
	return err
}
