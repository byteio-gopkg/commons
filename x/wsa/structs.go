package wsa

type Route struct {
	Url  string `json:"addr"`
	Host string `json:"host"`
}
