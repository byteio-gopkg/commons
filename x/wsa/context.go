package wsa

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"sync/atomic"
	"time"
)

type requestContext struct {
	timer    *time.Timer
	timeout  time.Duration
	deadline time.Time
	proxy    *url.URL
	err      error
	done     int32
	doneChan chan struct{}
}

func newRequestContext(timeout time.Duration, proxy *url.URL) context.Context {
	if timeout <= 0 && proxy == nil {
		return context.Background()
	} else {
		ctx := &requestContext{
			timeout: timeout,
			proxy:   proxy,
			done:    0,
		}
		if ctx.timeout > 0 {
			ctx.deadline = time.Now().Add(timeout)
			ctx.timer = time.AfterFunc(timeout, ctx.doTimeout)
			ctx.doneChan = make(chan struct{})
		}
		return ctx
	}
}

func (c *requestContext) Deadline() (deadline time.Time, ok bool) {
	return c.deadline, c.timeout > 0
}

func (c *requestContext) Done() <-chan struct{} {
	return c.doneChan
}

func (c *requestContext) doTimeout() {
	if c == nil {
		return
	}
	if atomic.CompareAndSwapInt32(&c.done, 0, 1) {
		c.err = timeoutErrorError{fmt.Sprint("Request timeout ", c.timeout)}
		close(c.doneChan)
		c.doneChan = nil
		c.timer = nil
	}
}

func (c *requestContext) Release() {
	if c.timer == nil {
		return
	}
	if atomic.CompareAndSwapInt32(&c.done, 0, 1) {
		if c.timer != nil {
			c.timer.Stop()
			c.timer = nil
			// close(c.doneChan)
		}
	}
}

func (c *requestContext) Err() error    { return c.err }
func (c *requestContext) Value(any) any { return nil }

func getProxyFromContext(req *http.Request) (*url.URL, error) {
	if ctx, ok := req.Context().(*requestContext); ok {
		return ctx.proxy, nil
	} else {
		return nil, nil
	}
}
