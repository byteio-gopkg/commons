package wsa

import (
	"byteio.org/commons/utils/net/httputil"
	"fmt"
	"net/http"
	"net/url"
)

type timeoutErrorError struct{ err string }

func (e timeoutErrorError) Error() string { return e.err }
func (timeoutErrorError) Timeout() bool   { return true }
func (timeoutErrorError) Temporary() bool { return true }

// --------------------------------------------------------

type StatusError struct {
	Url         *url.URL
	StatusCode  int
	Status      string
	ContentType string
	Body        string
}

func NewHttpStatusError(resp *http.Response, body string) error {
	return &StatusError{
		Url:         resp.Request.URL,
		StatusCode:  resp.StatusCode,
		Status:      resp.Status,
		ContentType: resp.Header.Get("Content-Type"),
		Body:        body,
	}
}

func (e *StatusError) Error() string {
	return e.Status + ", " + e.Body
}

func (e *StatusError) TrimBody() {
	size := len(e.Body)
	hu := "B"
	if size > 1024 {
		hu = "KB"
		size = size / 1024
	}
	e.Body = fmt.Sprint("[", size, " ", hu, "]...")
}

func (e *StatusError) AbbreviateBody(maxLength int) {
	if len(e.Body) > maxLength {
		e.Body = e.Body[:maxLength] + "..."
	}
}

func IsHttp20x(resp *http.Response) bool {
	code := resp.StatusCode
	return code >= 200 && code <= 209
}

func AssertHttp20x(resp *http.Response) error {
	code := resp.StatusCode
	if code < 200 || code > 209 {
		return NewHttpStatusError(resp, httputil.ReadBodyAsString(resp))
	}
	return nil
}

// --------------------------------------------------------

type UnauthorizedError struct {
	Reason   string
	Redirect string
}

func (e *UnauthorizedError) Error() string {
	return "unauthorized, " + e.Reason
}

func NewUnauthorizedError(reason string) error {
	return &UnauthorizedError{Reason: reason}
}

func NewUnauthorizedErrorWithRedirect(reason string, redirect string) error {
	return &UnauthorizedError{reason, redirect}
}

type InvalidUserError struct {
	Content string
}

func (e *InvalidUserError) Error() string {
	return "invalid user, " + e.Content
}

func NewInvalidUserError(content string) error {
	return &InvalidUserError{content}
}

//goland:noinspection ALL
const (
	FAILED_CAUSE_TOO_MANY     = "TOO_MANY"
	FAILED_CAUSE_NOT_WHITE_IP = "NOT_WHITE_IP"
	FAILED_CAUSE_FORBIDEN     = "FORBIDEN"
)

type FailedError struct {
	Content string
	Cause   string
}

func (e *FailedError) Error() string {
	if e.Cause == "" {
		return "failed, " + e.Content
	} else {
		return "failed[" + e.Cause + "], " + e.Content
	}
}

func NewFailedError(content string) error {
	return &FailedError{Content: content}
}

func NewFailedErrorWithCause(content string, cause string) error {
	return &FailedError{content, cause}
}

type BadResponseError struct {
	Reason string
}

func (e *BadResponseError) Error() string {
	return "bad response, " + e.Reason
}

func NewBadResponseError(reason string) error {
	return &BadResponseError{reason}
}

type IllegalArgumentsError struct {
	Reason string
}

func (e *IllegalArgumentsError) Error() string {
	return "illegal arguments, " + e.Reason
}

func NewIllegalArgumentsError(reason string) error {
	return &IllegalArgumentsError{reason}
}

type FatalError struct {
	Reason string
}

func (e *FatalError) Error() string {
	return "fatal, " + e.Reason
}

func NewFatalError(reason string) error {
	return &FatalError{reason}
}

type CaptchaError struct {
	Reason string
}

func (e *CaptchaError) Error() string {
	return "captcha, " + e.Reason
}

func NewCaptchaError(reason string) error {
	return &CaptchaError{reason}
}
