package wsa

import (
	"net/url"
	"strings"
)

func QueryString(form map[string]string) string {
	if form == nil {
		return ""
	}
	var buf strings.Builder
	for k, v := range form {
		if buf.Len() > 0 {
			buf.WriteByte('&')
		}
		buf.WriteString(k)
		buf.WriteByte('=')
		buf.WriteString(v)
	}
	return buf.String()
}

func UrlEncoded(form map[string]string) string {
	if form == nil {
		return ""
	}
	var buf strings.Builder
	for k, v := range form {
		if buf.Len() > 0 {
			buf.WriteByte('&')
		}
		buf.WriteString(url.QueryEscape(k))
		buf.WriteByte('=')
		buf.WriteString(url.QueryEscape(v))
	}
	return buf.String()
}

func UrlEncodedB(form map[string]string) []byte {
	return []byte(UrlEncoded(form))
}
