package uuid

import (
	"crypto/rand"
	"fmt"
	"sync"
	"time"
)

func randBytes(size int, offset int) []byte {
	buf := make([]byte, size)
	if _, err := rand.Read(buf[offset:]); err != nil {
		return nil
	}
	return buf
}

func toString(uuid []byte) string {
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:16])
}

// Rand generate a random UUID/v4
func Rand() string {
	return RandV4()
}

// --------------------------------------------------------

// RandV4 generate a random UUID/v4
func RandV4() string {
	uuid := randBytes(16, 0)
	if uuid == nil {
		return ""
	}
	uuid[6] = uuid[6]&0x0f | 0x40 // set to version 4
	uuid[8] = uuid[8]&0x3f | 0x80 // set to IETF variant
	return toString(uuid)
}

// --------------------------------------------------------

// RandV7 generate a random UUID/v7
// refer "github.com/google/uuid" uuid.NewV7()
func RandV7() string {
	uuid := randBytes(16, 8)
	if uuid == nil {
		return ""
	}
	t, s := nextV7Time()

	// Time
	uuid[0] = byte(t >> 40)
	uuid[1] = byte(t >> 32)
	uuid[2] = byte(t >> 24)
	uuid[3] = byte(t >> 16)
	uuid[4] = byte(t >> 8)
	uuid[5] = byte(t)

	// Version, Variant
	uuid[6] = 0x70 | (0x0F & byte(s>>8))
	uuid[7] = byte(s)

	return toString(uuid)
}

// lastV7time is the last time we returned stored as:
//
//	52 bits of time in milliseconds since epoch
//	12 bits of (fractional nanoseconds) >> 8
var lastV7time int64
var nextV7TimeMu sync.Mutex

// nextV7Time returns the time in milliseconds and nanoseconds / 256.
// The returned (milli << 12 + seq) is guaranteed to be greater than
// (milli << 12 + seq) returned by any previous call to getV7Time.
func nextV7Time() (milli, seq int64) {
	nano := time.Now().UnixNano()
	milli = nano / int64(time.Millisecond)
	// Sequence number is between 0 and 3906 (nanoPerMilli>>8)
	seq = (nano - milli*int64(time.Millisecond)) >> 8
	now := milli<<12 + seq

	nextV7TimeMu.Lock()
	if now <= lastV7time {
		now = lastV7time + 1
		milli = now >> 12
		seq = now & 0xfff
	}
	lastV7time = now
	nextV7TimeMu.Unlock()

	return milli, seq
}
