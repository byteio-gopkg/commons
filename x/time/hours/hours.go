package hours

import (
	"byteio.org/commons/utils/timeutil"
	"errors"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Period struct {
	From time.Time
	To   time.Time
}

type Hours interface {
	IsAt(now time.Time) bool
	Next(now time.Time) Period
	periods() []period
}

type period struct {
	from int
	to   int
}

func parseDayTime(value string, defaultVal int) int {
	value = strings.TrimSpace(value)
	if value == "" {
		return defaultVal
	}
	te, err := time.Parse("15:04", value)
	if err != nil {
		return -1
	}
	return te.Hour()*3600 + te.Minute()*60
}

// --------------------------------------------------------

// Parse expression to hours
//
// Format: "weekday1,weekday2|fromTime1-toTime1,fromTime2-toTime2"
//
// Examples:
//  1. "1,2,3,4,5|09:00-16:00" 			// weekday (1-5)'s 09:00-16:00
//  2. "09:00-16:00" 		   			// every day 09:00-16:00
//  3. "1,2|"				   			// weekday (1-2) 00:00-24:00
//  4. "6,7|12:00-12:30,16:00-16:30"	// weekday (6,7)'s 12:00-12:30 and 16:00-16:30
func Parse(value string) (Hours, error) {
	elements := strings.Split(value, ";")
	if len(elements) == 1 {
		return parseWeekly(elements[0], false)
	} else {
		all := make([]period, 0)
		for _, element := range elements {
			_hours, err := parseWeekly(element, true)
			if err != nil {
				return nil, err
			}
			all = append(all, _hours.periods()...)
		}
		return &weeklyHours{hours{resolvePeriods(all)}}, nil
	}
}

func parseWeekly(value string, weeklyOnly bool) (Hours, error) {
	parts := strings.Split(value, "|")
	if len(parts) == 1 {
		parts = []string{"", parts[0]}
	} else if len(parts) != 2 {
		return nil, errors.New("invalid hours: " + value)
	}

	var weekdays []int = nil
	for _, d0 := range strings.Split(parts[0], ",") {
		d0 = strings.TrimSpace(d0)
		if d0 == "" {
			continue
		}
		d, _ := strconv.Atoi(d0)
		if d < 1 || d > 7 {
			return nil, errors.New("invalid hours: " + d0)
		}
		weekdays = append(weekdays, d)
	}

	var dayTimes []period = nil
	for _, t0 := range strings.Split(parts[1], ",") {
		t0 = strings.TrimSpace(t0)
		if t0 == "" {
			continue
		}

		tbe := strings.Split(t0, "-")
		if len(tbe) != 2 {
			return nil, errors.New("invalid hours: " + t0)
		}
		wt := period{}
		if wt.from = parseDayTime(tbe[0], 0); wt.from < 0 {
			return nil, errors.New("invalid hours: " + t0)
		}
		if wt.to = parseDayTime(tbe[1], 24*3600); wt.to < 0 {
			return nil, errors.New("invalid hours: " + t0)
		}
		if wt.from > wt.to {
			return nil, errors.New("invalid hours: " + t0)
		}

		dayTimes = append(dayTimes, wt)
	}
	if len(dayTimes) < 1 {
		return nil, errors.New("invalid hours: " + value)
	}

	if weekdays == nil {
		if !weeklyOnly {
			return &dailyHours{hours{resolvePeriods(dayTimes)}}, nil
		} else {
			weekdays = make([]int, 0, 7)
			for i := 1; i <= 7; i++ {
				weekdays = append(weekdays, i)
			}
		}
	}
	all := make([]period, 0)
	for _, wd := range weekdays {
		for _, dt0 := range dayTimes {
			dt := dt0
			offset := (wd - 1) * 24 * 3600
			dt.from += offset
			dt.to += offset
			all = append(all, dt)
		}
	}
	return &weeklyHours{hours{resolvePeriods(all)}}, nil
}

func resolvePeriods(periods []period) []period {
	sort.Slice(periods, func(i, j int) bool {
		return periods[i].from < periods[j].from
	})
	for len(periods) > 1 {
		changed := false
		end := len(periods) - 1
		for i := 0; i < end; i++ {
			prev := &periods[i]
			next := &periods[i+1]
			// concat if overlapping
			if prev.to >= next.from {
				if next.to > prev.to {
					prev.to = next.to
				}
				if i+1 < end {
					copy(periods[i+1:], periods[i+2:])
				}
				periods = periods[:end]
				changed = true
				break
			}
		}
		if !changed {
			break
		}
	}
	return periods
}

type hours struct {
	hours []period
}

func (r *hours) periods() []period {
	return r.hours
}

func (r *hours) isAt(now time.Time, weekly bool) bool {
	loopTime := now.Hour()*3600 + now.Minute()*60 + now.Second()
	if weekly {
		weekday := timeutil.ISOWeekday(now.Weekday())
		loopTime += (weekday - 1) * 24 * 3600
	}
	for _, offset := range r.hours {
		if offset.from <= loopTime && loopTime < offset.to {
			return true
		}
	}
	return false
}

func (r *hours) next(now time.Time, weekly bool) Period {
	loopTime := now.Hour()*3600 + now.Minute()*60 + now.Second()
	if weekly {
		weekday := timeutil.ISOWeekday(now.Weekday())
		loopTime += (weekday - 1) * 24 * 3600
	}
	nowUnix := now.Unix()
	for _, offset := range r.hours {
		if offset.from <= loopTime && loopTime < offset.to {
			return Period{
				From: now,
				To:   time.Unix(nowUnix+int64(offset.to-loopTime), 0),
			}
		} else if offset.from > loopTime {
			return Period{
				From: time.Unix(nowUnix+int64(offset.from-loopTime), 0),
				To:   time.Unix(nowUnix+int64(offset.to-loopTime), 0),
			}
		}
	}
	nextLoopFirst := r.hours[0]
	nextLoopUnix := nowUnix - int64(loopTime)
	if weekly {
		nextLoopUnix += 7 * 24 * 3600
	} else {
		nextLoopUnix += 24 * 3600
	}
	return Period{
		From: time.Unix(nextLoopUnix+int64(nextLoopFirst.from), 0),
		To:   time.Unix(nextLoopUnix+int64(nextLoopFirst.to), 0),
	}
}

type weeklyHours struct {
	hours
}

func (r *weeklyHours) IsAt(now time.Time) bool {
	return r.isAt(now, true)
}

func (r *weeklyHours) Next(now time.Time) Period {
	return r.next(now, true)
}

type dailyHours struct {
	hours
}

func (r *dailyHours) IsAt(now time.Time) bool {
	return r.isAt(now, false)
}

func (r *dailyHours) Next(now time.Time) Period {
	return r.next(now, false)
}
