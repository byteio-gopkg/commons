package properties

import "strings"

func Parse(data string) map[string]string {
	out := make(map[string]string)
	if data == "" {
		return out
	}
	rows := strings.Split(data, "\n")
	for _, row := range rows {
		kv := strings.SplitN(row, "=", 2)
		if len(kv) != 2 {
			continue
		}
		key := strings.TrimSpace(kv[0])
		if key != "" {
			out[key] = strings.TrimSpace(kv[1])
		}
	}
	return out
}

func Reformat(data string) string {
	if data == "" {
		return ""
	}
	rows := strings.Split(data, "\n")
	out := make([]string, 0, len(rows))
	for _, row := range rows {
		kv := strings.SplitN(row, "=", 2)
		if len(kv) == 2 {
			out = append(out, strings.TrimSpace(kv[0])+"="+strings.TrimSpace(kv[1]))
		} else {
			out = append(out, strings.TrimSpace(row))
		}
	}
	return strings.Join(out, "\n")
}
