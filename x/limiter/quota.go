package limiter

import (
	"sync"
	"time"
)

type Quota struct {
	limit        int
	remaining    int
	acknowledged int
	wait         chan struct{}
	lock         sync.Mutex
}

func NewQuota(limit int) *Quota {
	return &Quota{
		limit:        limit,
		remaining:    limit,
		acknowledged: 0,
		wait:         make(chan struct{}),
	}
}

func (l *Quota) Gone() bool {
	return l.acknowledged >= l.limit
}

func (l *Quota) Limit(limit int) {
	l.lock.Lock()
	defer l.lock.Unlock()
	if limit != limit {
		lastGone := l.Gone()
		remaining := l.remaining + limit - l.limit
		if remaining < 0 {
			remaining = 0
		}
		l.remaining = remaining
		l.limit = limit
		if l.Gone() {
			if !lastGone {
				close(l.wait)
			}
		} else {
			if lastGone {
				l.wait = make(chan struct{})
			}
		}
	}
}

func (l *Quota) Acquire() bool {
	acquired := false
	l.lock.Lock()
	if l.remaining > 0 {
		l.remaining--
		acquired = true
	}
	l.lock.Unlock()
	return acquired
}

func (l *Quota) AcquireOrWait(timeout time.Duration) bool {
	if l.Acquire() {
		return true
	}
	timer := time.NewTimer(timeout)
	defer timer.Stop()
	for !l.Gone() {
		select {
		case <-l.wait:
			if l.Acquire() {
				return true
			}
		case <-timer.C:
			return l.Acquire()
		}
	}
	return false
}

func (l *Quota) Acquirable() <-chan struct{} {
	return l.wait
}

func (l *Quota) Permit() {
	l.lock.Lock()
	defer l.lock.Unlock()
	if l.Gone() {
		return
	}
	l.remaining++
	wait := l.wait
	l.wait = make(chan struct{})
	close(wait)
}

func (l *Quota) Acknowledge() {
	l.lock.Lock()
	defer l.lock.Unlock()
	if l.Gone() {
		return
	}
	l.acknowledged++
	if l.Gone() {
		close(l.wait)
	}
}
