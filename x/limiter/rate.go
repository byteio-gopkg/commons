package limiter

import (
	"byteio.org/commons/utils/timeutil"
	"math"
	"sync"
	"time"
)

type Rate struct {
	limit          int
	period         int64
	rate           int
	nextPeriodTime int64
	mu             sync.Mutex
}

func NewRate(limit int, period time.Duration) *Rate {
	return &Rate{
		limit:          limit,
		period:         period.Nanoseconds(),
		rate:           0,
		nextPeriodTime: 0,
	}
}

func (r *Rate) Rate() int {
	if timeutil.RuntimeNano() >= r.nextPeriodTime {
		return 0
	} else {
		return r.rate
	}
}

func (r *Rate) Limit() int {
	return r.limit
}

func (r *Rate) Acquire() {
retry:
	now := timeutil.RuntimeNano()
	r.mu.Lock()
	if now >= r.nextPeriodTime {
		r.rate = 0
		r.nextPeriodTime = now + r.period
	}
	newRate := r.rate + 1
	if newRate <= r.limit {
		r.rate = newRate
		r.mu.Unlock()
		return
	}
	wait := r.nextPeriodTime - now
	r.mu.Unlock()

	<-time.After(time.Duration(wait))
	goto retry
}

func (r *Rate) TryAcquire(timeout time.Duration) bool {
	var deadline int64 = 0
retry:
	now := timeutil.RuntimeNano()
	r.mu.Lock()
	if now >= r.nextPeriodTime {
		r.rate = 0
		r.nextPeriodTime = now + r.period
	}
	newRate := r.rate + 1
	if newRate <= r.limit {
		r.rate = newRate
		r.mu.Unlock()
		return true
	}
	nextTime := r.nextPeriodTime
	r.mu.Unlock()

	if timeout == 0 {
		return false
	}
	wait := nextTime - now
	if deadline == 0 {
		if timeout > 0 {
			deadline = now + timeout.Nanoseconds()
		} else {
			deadline = math.MaxInt64
		}
	}
	if wait1 := deadline - now; wait1 < wait {
		wait = wait1
	}
	if wait > 0 {
		<-time.After(time.Duration(wait))
		goto retry
	}
	return false
}
