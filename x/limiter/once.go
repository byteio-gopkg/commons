package limiter

import (
	"byteio.org/commons/utils/timeutil"
	"sync"
	"time"
)

type Once struct {
	next     int64
	duration int64
	lock     sync.Mutex
}

func NewOnce(duration time.Duration) *Once {
	return &Once{duration: int64(duration)}
}

func (l *Once) Duration() time.Duration {
	return time.Duration(l.duration)
}

func (l *Once) Reset(duration time.Duration) {
	l.lock.Lock()
	l.duration = int64(duration)
	l.next = 0
	l.lock.Unlock()
}

func (l *Once) Acquire() (wait time.Duration) {
	now := timeutil.RuntimeNano()
	l.lock.Lock()
	remaining := l.next - now
	if remaining <= 0 {
		l.next = now + l.duration
		wait = 0
	} else {
		wait = time.Duration(remaining)
	}
	l.lock.Unlock()
	return
}

func (l *Once) Permit() {
	l.lock.Lock()
	l.next = 0
	l.lock.Unlock()
}
