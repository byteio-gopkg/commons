package limiter

import (
	"sync"
	"sync/atomic"
	"time"
)

type Completions[K comparable] struct {
	t    map[K]*Completion
	lock sync.Mutex
}

func NewCompletions[K comparable]() *Completions[K] {
	return &Completions[K]{
		t: make(map[K]*Completion),
	}
}

func (t *Completions[K]) Obtain(key K, limit int) *Completion {
	t.lock.Lock()
	s := t.t[key]
	if s == nil {
		s = newCompletion(limit, func() { t.Remove(key) })
		t.t[key] = s
	}
	t.lock.Unlock()
	return s
}

func (t *Completions[K]) Remove(key K) {
	t.lock.Lock()
	delete(t.t, key)
	t.lock.Unlock()
}

func (t *Completions[K]) Size() int {
	return len(t.t)
}

func (t *Completions[K]) Clear() {
	t.lock.Lock()
	clear(t.t)
	t.lock.Unlock()
}

// --------------------------------------------------------

type Completion struct {
	doing   int
	limit   int
	done    bool
	doneC   chan struct{}
	ctl     int32
	removal func()
	mutex   sync.Mutex
}

func newCompletion(limit int, removal func()) *Completion {
	return &Completion{
		doing:   0,
		limit:   limit,
		done:    false,
		doneC:   make(chan struct{}),
		ctl:     0,
		removal: removal,
	}
}

func (l *Completion) Enter() bool {
	l.mutex.Lock()
	acquired := l.doing < l.limit && !l.done
	if acquired {
		l.doing++
	}
	l.mutex.Unlock()
	return acquired
}

func (l *Completion) Exit(done bool) bool {
	if l.done {
		return true
	}

	doneFirst := false
	l.mutex.Lock()
	if l.doing > 0 {
		l.doing--
	}
	if (done || l.doing == 0) && !l.done {
		done = true
		l.done = true
		doneFirst = true
	}
	l.mutex.Unlock()

	if doneFirst {
		l.removal()
		l.removal = nil
		close(l.doneC)
	}
	return done
}

func (l *Completion) Done() bool {
	return l.done
}

func (l *Completion) DoneC() <-chan struct{} {
	return l.doneC
}

func (l *Completion) WaitDone(timeout time.Duration) bool {
	if l.done {
		return true
	}
	timer := time.NewTimer(timeout)
	select {
	case <-l.doneC:
		timer.Stop()
	case <-timer.C:
	}
	return l.done
}

func (l *Completion) SetCtl(expect int32, update int32) bool {
	return atomic.CompareAndSwapInt32(&l.ctl, expect, update)
}

func (l *Completion) AddCtl(delta int32) int32 {
	return atomic.AddInt32(&l.ctl, delta)
}
