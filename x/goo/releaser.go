package goo

import (
	"io"
	"time"
)

type Releaser interface {
	Release()
}

// ----------------------------------------------

type funcReleaser struct {
	f func()
}

func NewFuncReleaser(f func()) Releaser {
	return &funcReleaser{f}
}

func (r *funcReleaser) Release() {
	r.f()
}

type closerReleaser struct {
	c io.Closer
}

func NewCloserReleaser(c io.Closer) Releaser {
	return &closerReleaser{c}
}

func (r *closerReleaser) Release() {
	_ = r.c.Close()
}

type timerReleaser struct {
	t *time.Timer
}

func NewTimerReleaser(timer *time.Timer) Releaser {
	return &timerReleaser{timer}
}

func (r *timerReleaser) Release() {
	r.t.Stop()
}
