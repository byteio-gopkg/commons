package goo

import (
	"io"
	"sync/atomic"
)

//goland:noinspection GoSnakeCaseUsage
const (
	STATE_CREATED     = 0
	STATE_INITIALIZED = 1
	STATE_CLOSED      = 2
)

type Service interface {
	Name() string
	Initialize() error
	Destroy() error
}

type ServiceReady struct{}

func (ServiceReady) Initialize() error {
	return nil
}

func (ServiceReady) Destroy() error {
	return nil
}

type Initializer interface {
	SetInitialized() bool
	Initialize() error
}

type InitializerBase struct {
	initialized int32
}

func (i *InitializerBase) SetInitialized() bool {
	return atomic.CompareAndSwapInt32(&i.initialized, 0, 1)
}

func (i *InitializerBase) Initialize() error {
	return nil
}

type StatsGetter interface {
	GetStats(out map[string]any)
}

type Closers struct {
	closers []io.Closer
}

func (c *Closers) AddCloser(closer io.Closer) {
	if c != nil && closer != nil {
		c.closers = append(c.closers, closer)
	}
}

func (c *Closers) Close() error {
	if c == nil {
		return nil
	}
	for _, closer := range c.closers {
		_ = closer.Close()
	}
	c.closers = nil
	return nil
}

type Sources[K comparable] struct {
	sources map[K]any
}

func (s *Sources[K]) GetSource(key K) any {
	if s != nil {
		return s.sources[key]
	}
	return nil
}

func (s *Sources[K]) SetSource(key K, source any) {
	if s != nil {
		m := s.sources
		if m == nil {
			m = make(map[K]any)
			s.sources = m
		}
		if source == nil {
			delete(m, key)
		} else {
			m[key] = source
		}
	}
}

func (s *Sources[K]) Close() error {
	if s == nil {
		return nil
	}
	for _, sc := range s.sources {
		if closer, ok := sc.(io.Closer); ok {
			_ = closer.Close()
		}
	}
	clear(s.sources)
	return nil
}
