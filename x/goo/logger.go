package goo

type Logger interface {
	Debug(content ...any)
	Info(content ...any)
	Warn(content ...any)
	Error(content ...any)
}
