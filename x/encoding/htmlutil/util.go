package htmlutil

import (
	"bytes"
	"encoding/xml"
)

func Unmarshal(data []byte, v any) error {
	d := xml.NewDecoder(bytes.NewReader(data))
	d.Strict = false
	d.Entity = xml.HTMLEntity
	d.AutoClose = xml.HTMLAutoClose
	return d.Decode(v)
}
