package htmlutil

import (
	"byteio.org/commons/utils/stringutil"
	"byteio.org/commons/x/encoding/html"
	"encoding/xml"
	"net/url"
	"strings"
)

var autoCloseTags = map[string]bool{}

func init() {
	for _, tag := range xml.HTMLAutoClose {
		autoCloseTags[tag] = true
		autoCloseTags[strings.ToUpper(tag)] = true
	}
}

// --------------------------------------------------------

func TagNameOf(data string) string {
	started := -2
	for i, ch := range data {
		if ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r' {
			if started >= 0 {
				return data[started:i]
			} else if started == -1 {
				return ""
			}
		} else if ch == '<' {
			if started == -2 {
				started = -1
			} else if started == -1 {
				return ""
			} else {
				return data[started:i]
			}
		} else if ch == '>' || ch == '/' {
			if started >= 0 {
				return data[started:i]
			} else {
				return ""
			}
		} else {
			if started == -2 {
				return ""
			} else if started == -1 {
				started = i
			}
		}
	}
	if started < 0 {
		return ""
	}
	return data[started:]
}

func TrimTag(data string) string {
	tagName := TagNameOf(data)
	if tagName == "" {
		return data
	}
	bi := strings.Index(data, "<"+tagName)
	if bi < 0 {
		return data
	}
	ri := bi + len(tagName) + 1
	bci := tagEndIndex(data[ri:])
	if bci < 0 {
		return data
	}
	bci += ri + 1
	if bci >= len(data) {
		return ""
	}
	ri = bci
	right := data[ri:]
	if data[ri-2] == '/' {
		if isSpace(right) {
			return ""
		} else {
			return data
		}
	}

	endToken := "</" + tagName + ">"
	ei := strings.LastIndex(right, endToken)
	if ei < 0 {
		if isSpace(right) {
			return ""
		}
		return data
	}
	ei += ri
	eei := ei + len(endToken)
	if eei < len(data) && !isSpace(data[eei:]) {
		return data
	}
	return data[bci:ei]
}

func isSpace(data string) bool {
	for _, ch := range data {
		if ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r' {
		} else {
			return false
		}
	}
	return true
}

func TrimTagAndSpace(data string) string {
	return strings.TrimSpace(TrimTag(data))
}

// --------------------------------------------------------

func tagEndIndex(data string) int {
	var attr rune = 0
	for i, ch := range data {
		if ch == '>' {
			if attr == 0 {
				return i
			}
		} else if ch == '"' || ch == '\'' {
			if attr == 0 {
				attr = ch
			} else if ch == attr {
				attr = 0
			}
		} else if ch == '<' {
			if attr == 0 {
				return -1
			}
		}
	}
	return -1
}

// Extract tag fragment in first
//
//	tag: optional
//	keyword: optional
func Extract(data, tag, keyword string, withTag bool) (string, int) {
	bi := -1
	ki := -1
	kei := -1
	ei := -1
	startToken := "<" + tag
NEXT:
	// find start
	if keyword == "" {
		if bi < 0 {
			bi = strings.Index(data, startToken)
			if bi < 0 {
				return "", -1
			}
		} else {
			offset := bi + 1
			bi = strings.Index(data[offset:], startToken)
			if bi < 0 {
				return "", -1
			}
			bi += offset
		}
	} else {
		if ei < 0 {
			if kei < 0 {
				ki = strings.Index(data, keyword)
				if ki < 2 {
					return "", -1
				}
			} else {
				ki = strings.Index(data[kei:], keyword)
				if ki < 2 {
					return "", -1
				}
				ki += kei
			}
			kei = ki + len(keyword)
			bi = strings.LastIndex(data[:ki], startToken)
			if bi < 0 {
				goto NEXT
			}
		} else {
			ei = -1
			if bi < 1 {
				goto NEXT
			}
			bi = strings.LastIndex(data[:bi], startToken)
			if bi < 0 {
				goto NEXT
			}
		}
	}

	// delimit start tag
	if startToken == "<" {
		tag = TagNameOf(data[bi:])
		if tag == "" {
			if kei > 0 {
				ei = 0
			}
			goto NEXT
		}
	}
	tgl := len(tag)
	ri := bi + tgl + 1
	right := data[ri:]
	bci := tagEndIndex(right)
	if bci < 0 {
		return "", -1
	}
	ri = ri + bci + 1
	// empty tag or auto close tag
	if (bci > 0 && right[bci-1] == '/') || autoCloseTags[tag] {
		if kei > 0 && ri < kei {
			goto NEXT
		}
		if withTag {
			return data[bi:ri], ri
		} else {
			return "", ri
		}
	}

	// find closing tag
	right = data[ri:]
	endToken := "</" + tag + ">"
	closing := 1
	clt := len(right) - 2
	for i, ch := range right {
		if ch != '<' || i >= clt {
			continue
		}
		seg := right[i:]
		if seg[1] == '/' {
			if strings.HasPrefix(seg, endToken) {
				closing--
			}
		} else if i+tgl < clt {
			seg = seg[1:]
			if seg[:tgl] == tag {
				if ste := seg[tgl]; ste == ' ' || ste == '>' {
					closing++
				}
			}
		}
		if closing < 1 {
			ei = i
			break
		}
	}
	if ei < 0 || kei > ei+ri {
		goto NEXT
	}
	fei := ri + ei + len(endToken)
	if withTag {
		return data[bi:fei], fei
	} else {
		return right[:ei], fei
	}
}

// ExtractAll extract all node fragments
func ExtractAll(data, tag, keyword string, withTag bool) []string {
	out := make([]string, 0)
	for {
		fragment, ei := Extract(data, tag, keyword, withTag)
		if ei < 1 {
			break
		}
		out = append(out, fragment)
		if ei >= len(data) {
			break
		}
		data = data[ei:]
	}
	return out
}

// ExtractOuter extract node outer fragment
func ExtractOuter(data, tag, keyword string) string {
	ret, _ := Extract(data, tag, keyword, true)
	return ret
}

// ExtractInner extract node inner fragment
func ExtractInner(data, tag, keyword string) string {
	ret, _ := Extract(data, tag, keyword, false)
	return ret
}

var inputPrefixes = []string{"<input ", "<button ", "<textarea ", "<select "}

// ExtractInput extract input element
func ExtractInput(data, keyword string) *html.InputNode {
	for {
		fragment, ei := Extract(data, "", keyword, true)
		if fragment == "" {
			return nil
		}
		if stringutil.HasAnyPrefix(fragment, inputPrefixes) {
			node := new(html.InputNode)
			if Unmarshal([]byte(fragment), node) == nil {
				return node
			}
		}
		if ei >= len(data) {
			break
		}
		data = data[ei:]
	}
	return nil
}

// ExtractInputs extract inputs
func ExtractInputs(data string) []*html.InputNode {
	fragments := ExtractAll(data, "", "name=", true)
	out := make([]*html.InputNode, 0, len(fragments))
	for _, fragment := range fragments {
		if stringutil.HasAnyPrefix(fragment, inputPrefixes) {
			input := new(html.InputNode)
			if err := Unmarshal([]byte(fragment), input); err == nil {
				out = append(out, input)
			}
		}
	}
	return out
}

// ExtractInputValues extract input values
func ExtractInputValues(data string, names ...string) url.Values {
	values := make(url.Values)
	for _, name := range names {
		input := ExtractInput(data, "name=\""+name+"\"")
		if input != nil {
			values.Set(name, input.Value)
		}
	}
	return values
}

// ExtractForm extract form
func ExtractForm(data, keyword string) *html.FormNode {
	fragment, _ := Extract(data, "form", keyword, true)
	if fragment == "" {
		return nil
	}
	node := new(html.FormNode)
	if Unmarshal([]byte(fragment), node) != nil {
		return nil
	}
	node.Inputs = ExtractInputs(fragment)
	return node
}
