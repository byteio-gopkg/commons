package jsonify

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

type JSONField struct {
	Name  string
	Value any
}

type JSON []JSONField

func (j JSON) FieldIndexOf(key string) int {
	for i, field := range j {
		if field.Name == key {
			return i
		}
	}
	return -1
}

func (j JSON) Update(fields ...JSONField) JSON {
	nj := j
	for _, f := range fields {
		index := nj.FieldIndexOf(f.Name)
		if index >= 0 {
			nj[index].Value = f.Value
		} else {
			nj = append(nj, f)
		}
	}
	return nj
}

func (j JSON) Copy() JSON {
	dest := make(JSON, len(j))
	for i, field := range j {
		dest[i] = field
	}
	return dest
}

// --------------------------------------------------------

func Stringify(value any) string {
	buf := &strings.Builder{}
	writeValue(value, buf)
	return buf.String()
}

func writeValue(value any, out *strings.Builder) {
	switch v := value.(type) {
	case string:
		WriteString(v, out)
	case int:
		out.WriteString(strconv.FormatInt(int64(v), 10))
	case int8:
		out.WriteString(strconv.FormatInt(int64(v), 10))
	case int16:
		out.WriteString(strconv.FormatInt(int64(v), 10))
	case int32:
		out.WriteString(strconv.FormatInt(int64(v), 10))
	case int64:
		out.WriteString(strconv.FormatInt(v, 10))
	case uint:
		out.WriteString(strconv.FormatUint(uint64(v), 10))
	case uint8:
		out.WriteString(strconv.FormatUint(uint64(v), 10))
	case uint16:
		out.WriteString(strconv.FormatUint(uint64(v), 10))
	case uint32:
		out.WriteString(strconv.FormatUint(uint64(v), 10))
	case uint64:
		out.WriteString(strconv.FormatUint(v, 10))
	case float32:
		out.WriteString(strconv.FormatFloat(float64(v), 'f', -1, 32))
	case float64:
		out.WriteString(strconv.FormatFloat(v, 'f', -1, 64))
	case bool:
		out.WriteString(strconv.FormatBool(v))
	case nil:
		out.WriteString("null")
	case JSON:
		writeJSON(v, out)
	case []string:
		out.WriteByte('[')
		for index, ele := range v {
			if index > 0 {
				out.WriteByte(',')
			}
			WriteString(ele, out)
		}
		out.WriteByte(']')
	case []int:
		out.WriteByte('[')
		for index, ele := range v {
			if index > 0 {
				out.WriteByte(',')
			}
			out.WriteString(strconv.Itoa(ele))
		}
		out.WriteByte(']')
	case []float64:
		out.WriteByte('[')
		for index, ele := range v {
			if index > 0 {
				out.WriteByte(',')
			}
			out.WriteString(strconv.FormatFloat(ele, 'f', -1, 64))
		}
		out.WriteByte(']')
	case []JSON:
		out.WriteByte('[')
		for index, ele := range v {
			if index > 0 {
				out.WriteByte(',')
			}
			writeJSON(ele, out)
		}
		out.WriteByte(']')
	case []any:
		out.WriteByte('[')
		for index, ele := range v {
			if index > 0 {
				out.WriteByte(',')
			}
			writeValue(ele, out)
		}
		out.WriteByte(']')
	default:
		rv := reflect.ValueOf(v)
		switch rv.Kind() {
		case reflect.Slice, reflect.Array:
			out.WriteByte('[')
			length := rv.Len()
			for index := 0; index < length; index++ {
				if index > 0 {
					out.WriteByte(',')
				}
				writeValue(rv.Index(index).Interface(), out)
			}
			out.WriteByte(']')
		case reflect.Map:
			writeMap(rv, out)
		default:
			WriteString(fmt.Sprint(value), out)
		}
	}
}

func writeJSON(value JSON, out *strings.Builder) {
	out.WriteByte('{')
	for index, field := range value {
		if index > 0 {
			out.WriteString(",\"")
		} else {
			out.WriteByte('"')
		}
		out.WriteString(field.Name)
		out.WriteString("\":")
		writeValue(field.Value, out)
	}
	out.WriteByte('}')
}

func writeMap(value reflect.Value, out *strings.Builder) {
	out.WriteByte('{')
	iter := value.MapRange()
	first := true
	for iter.Next() {
		if first {
			out.WriteByte('"')
			first = false
		} else {
			out.WriteString(",\"")
		}
		out.WriteString(iter.Key().String())
		out.WriteString("\":")
		writeValue(iter.Value().Interface(), out)
	}
	out.WriteByte('}')
}
