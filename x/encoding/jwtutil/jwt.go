package jwtutil

import (
	"encoding/base64"
	"encoding/json"
	"strings"
)

func ParsePayload(token string) map[string]any {
	if token == "" {
		return nil
	}
	parts := strings.Split(token, ".")
	if len(parts) != 3 {
		return nil
	}
	payload0 := parts[1]
	if l := len(payload0) % 4; l > 0 {
		payload0 += strings.Repeat("=", 4-l)
	}
	payload1, err := base64.StdEncoding.DecodeString(payload0)
	if err != nil {
		return nil
	}
	out := make(map[string]any)
	if json.Unmarshal(payload1, &out) != nil {
		return nil
	}
	return out
}
