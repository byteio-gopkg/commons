package html

import "net/url"

type Element struct {
	Id    string `xml:"id,attr"`
	Class string `xml:"class,attr"`
}

type FormNode struct {
	Element
	Method string `xml:"method,attr"`
	Action string `xml:"action,attr"`
	Inputs []*InputNode
}

type InputNode struct {
	Element
	Type  string `xml:"type,attr"`
	Name  string `xml:"name,attr"`
	Value string `xml:"value,attr"`
}

func (f *FormNode) UrlValues() url.Values {
	values := make(url.Values)
	for _, input := range f.Inputs {
		if input.Name != "" {
			values.Add(input.Name, input.Value)
		}
	}
	return values
}

type SelectOption struct {
	Element
	Value string `xml:"value,attr"`
	Text  string `xml:",innerxml"`
}

type Select struct {
	Element
	Name    string          `xml:"name,attr"`
	Options []*SelectOption `xml:"option"`
}
