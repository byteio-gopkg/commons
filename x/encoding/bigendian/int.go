package bigendian

func Int16(bytes []byte) int16 {
	return int16(bytes[1]) |
		int16(bytes[0])<<8
}

func PutInt16(out []byte, value int16) {
	out[0] = byte(value >> 8)
	out[1] = byte(value)
}

func Int16Bytes(value int16) []byte {
	bytes := make([]byte, 2)
	PutInt16(bytes, value)
	return bytes
}

func Int32(bytes []byte) int32 {
	return int32(bytes[3]) |
		int32(bytes[2])<<8 |
		int32(bytes[1])<<16 |
		int32(bytes[0])<<24
}

func PutInt32(out []byte, value int32) {
	out[0] = byte(value >> 24)
	out[1] = byte(value >> 16)
	out[2] = byte(value >> 8)
	out[3] = byte(value)
}

func Int32Bytes(value int32) []byte {
	bytes := make([]byte, 4)
	PutInt32(bytes, value)
	return bytes
}

func Int64(bytes []byte) int64 {
	return int64(bytes[7]) |
		int64(bytes[6])<<8 |
		int64(bytes[5])<<16 |
		int64(bytes[4])<<24 |
		int64(bytes[3])<<32 |
		int64(bytes[2])<<40 |
		int64(bytes[1])<<48 |
		int64(bytes[0])<<56
}

func PutInt64(out []byte, value int64) {
	out[0] = byte(value >> 56)
	out[1] = byte(value >> 48)
	out[2] = byte(value >> 40)
	out[3] = byte(value >> 32)
	out[4] = byte(value >> 24)
	out[5] = byte(value >> 16)
	out[6] = byte(value >> 8)
	out[7] = byte(value)
}

func Int64Bytes(value int64) []byte {
	bytes := make([]byte, 8)
	PutInt64(bytes, value)
	return bytes
}
