package buffer

import (
	"slices"
	"sync"
)

type Pool struct {
	pools []*sync.Pool
	sizes []int
	Init  func(b []byte)
}

func NewPool(sizeList []int) *Pool {
	if sizeList == nil {
		sizeList = []int{1024, 4096, 8192}
	}
	pools := make([]*sync.Pool, len(sizeList))
	sizes := make([]int, len(sizeList))
	copy(sizes, sizeList)
	slices.Sort(sizes)
	p := &Pool{sizes: sizes}
	for i, size := range sizes {
		pools[i] = newPool(p, size)
	}
	p.pools = pools
	return p
}

func newPool(p *Pool, size int) *sync.Pool {
	return &sync.Pool{
		New: func() any {
			return p.newBuffer(size)
		},
	}
}

func (p *Pool) newBuffer(size int) []byte {
	b := make([]byte, size)
	if init := p.Init; init != nil {
		init(b)
	}
	return b
}

func (p *Pool) Alloc(size int) []byte {
	for slot, bufSize := range p.sizes {
		if bufSize >= size {
			return p.pools[slot].Get().([]byte)[:size]
		}
	}
	return p.newBuffer(size)
}

func (p *Pool) Put(buf []byte) {
	size := cap(buf)
	slot := -1
	for i, bufSize := range p.sizes {
		if bufSize <= size {
			slot = i
		} else {
			break
		}
	}
	if slot >= 0 {
		p.pools[slot].Put(buf)
	}
}
