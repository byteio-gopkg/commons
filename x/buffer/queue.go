package buffer

import (
	"byteio.org/commons/utils/fileutil"
	"byteio.org/commons/utils/ioutil"
	"byteio.org/commons/utils/timeutil"
	"byteio.org/commons/x/collections"
	"byteio.org/commons/x/encoding/bigendian"
	"byteio.org/commons/x/id/uuid"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"sync"
	"sync/atomic"
)

type Queue interface {
	IsEmpty() bool
	Push(data []byte) error
	Poll() []byte
	Close() error
}

var queueClosedErr = errors.New("queue closed")

// --------------------------------------------------------

var queueCapacityLimitErr = errors.New("queue capacity limited")

type memoryQueue struct {
	*collections.Queue
	limit int
}

func NewMemoryQueue(limit int) Queue {
	return &memoryQueue{
		Queue: collections.NewQueue(),
		limit: limit,
	}
}

func (q *memoryQueue) Push(data []byte) error {
	if q.limit > 0 && q.Len() >= q.limit {
		return queueCapacityLimitErr
	}
	q.Queue.Push(data)
	return nil
}

func (q *memoryQueue) Poll() []byte {
	ret := q.Queue.Poll()
	if ret == nil {
		return nil
	}
	return ret.([]byte)
}

func (q *memoryQueue) Close() error {
	q.Queue.Clear()
	return nil
}

// --------------------------------------------------------

const (
	fileQueueMetadataSize                  = 20
	fileQueueMetadataOffsetMagic           = 0
	fileQueueMetadataOffsetReadOffset      = 12
	fileQueueResize                        = 1024 * 1024 * 16
	fileQueueBlockSizeLimit                = 1024 * 1024 * 128
	fileQueueMagic                         = "bbq5"
	fileQueueBlockHeadSize                 = 5
	fileQueueBlockMagic               byte = 123
	fileQueueBlockMagicOffset              = 0
	fileQueueBlockLenOffset                = 1
)

type fileQueue struct {
	filepath     string
	fileW        *os.File
	fileR        *os.File
	fileROffset  int64
	fileRHasNext bool
	fileMutex    sync.Mutex
	bufferPool   *Pool
	hb           []byte
	rob          []byte
	closed       int32
}

func NewFileQueue(path string, bufferPool *Pool) Queue {
	dir := filepath.Dir(path)
	if !fileutil.Exist(dir) {
		_ = os.MkdirAll(dir, 0766)
	}
	if bufferPool == nil {
		bufferPool = NewPool([]int{256, 1024, 4096, 8192})
	}
	q := &fileQueue{
		filepath:     path,
		bufferPool:   bufferPool,
		fileRHasNext: true,
		hb:           make([]byte, fileQueueBlockHeadSize),
		rob:          make([]byte, 8),
		closed:       0,
	}
	runtime.SetFinalizer(q, func(q *fileQueue) {
		_ = q.Close()
	})
	return q
}

func (q *fileQueue) closeFiles() {
	ioutil.Close(q.fileW)
	ioutil.Close(q.fileR)
	q.fileW = nil
	q.fileR = nil
}

func (q *fileQueue) Close() error {
	if !atomic.CompareAndSwapInt32(&q.closed, 0, 1) {
		return nil
	}
	q.fileMutex.Lock()
	defer q.fileMutex.Unlock()
	fileRHasNext := q.fileRHasNext
	q.fileRHasNext = false
	q.closeFiles()
	if fileSize, _ := fileutil.Size(q.filepath); fileSize > 0 {
		if fileSize == fileQueueMetadataSize || (!fileRHasNext && q.fileROffset == fileSize) {
			_ = os.Remove(q.filepath)
		}
	}
	return nil
}

func (q *fileQueue) resolveDamaged() {
	q.closeFiles()
	newName := fmt.Sprint(q.filepath + ".damaged-" + uuid.Rand())
	_ = os.Rename(q.filepath, newName)
	println(timeutil.NowDateTimeMS(), " buffer queue file damaged: ", newName)
}

func newFileQueueMetadata() []byte {
	md := make([]byte, fileQueueMetadataSize)
	copy(md[fileQueueMetadataOffsetMagic:], fileQueueMagic)
	bigendian.PutInt64(md[fileQueueMetadataOffsetReadOffset:], fileQueueMetadataSize) // Read offset
	return md
}

type fileQueueFile struct {
	file     *os.File
	size     int64
	metadata []byte
}

func (q *fileQueue) openFile(flag int) (*fileQueueFile, error) {
	if q.closed != 0 {
		return nil, queueClosedErr
	}
	file, err := os.OpenFile(q.filepath, flag, 0666)
	if file == nil {
		return nil, err
	}
	valid := false
	defer func() {
		if !valid {
			_ = file.Close()
		}
	}()

	fileStat, err := file.Stat()
	if fileStat == nil {
		return nil, err
	}
	fileSize := fileStat.Size()
	var metadata []byte
	if flag&os.O_CREATE != 0 && fileSize == 0 {
		metadata = newFileQueueMetadata()
		_, err = file.Write(metadata)
		if err != nil {
			return nil, err
		}
	}
	if metadata == nil {
		if fileSize < fileQueueMetadataSize {
			q.resolveDamaged()
			return nil, errors.New("damaged queue file")
		}
		metadata = make([]byte, fileQueueMetadataSize)
		n, _ := file.ReadAt(metadata, 0)
		if n != len(metadata) {
			q.resolveDamaged()
			return nil, errors.New("read queue file metadata failed")
		}
		magic := string(metadata[fileQueueMetadataOffsetMagic : fileQueueMetadataOffsetMagic+len(fileQueueMagic)])
		if magic != fileQueueMagic {
			q.resolveDamaged()
			return nil, errors.New("invalid queue file magic")
		}
	}
	valid = true
	return &fileQueueFile{
		file, fileSize, metadata,
	}, nil
}

var blockSizeLimitedErr = errors.New("queue block size limited")

func (q *fileQueue) Push(data []byte) error {
	dataLen := len(data)
	if dataLen > fileQueueBlockSizeLimit {
		return blockSizeLimitedErr
	}

	q.fileMutex.Lock()
	defer q.fileMutex.Unlock()
	if q.closed != 0 {
		return queueClosedErr
	}
	if q.fileW == nil {
		fileW, err := q.openFile(os.O_RDWR | os.O_CREATE | os.O_APPEND)
		if err != nil {
			return err
		}
		q.fileW = fileW.file
	}
	head := q.hb
	head[fileQueueBlockMagicOffset] = fileQueueBlockMagic
	bigendian.PutInt32(head[fileQueueBlockLenOffset:], int32(dataLen))
	_, err := q.fileW.Write(head)
	if err != nil {
		return err
	}
	if dataLen > 0 {
		_, err = q.fileW.Write(data)
		if err != nil {
			return err
		}
	}
	q.fileRHasNext = true
	return nil
}

func (q *fileQueue) openReadFile() (*os.File, int64) {
	if !fileutil.Exist(q.filepath) {
		return nil, 0
	}

	file, _ := q.openFile(os.O_RDWR)
	if file == nil {
		return nil, 0
	}
	valid := false
	defer func() {
		if !valid {
			_ = file.file.Close()
		}
	}()
	readOffset := bigendian.Int64(file.metadata[fileQueueMetadataOffsetReadOffset:])
	if readOffset < 0 || readOffset > file.size {
		q.resolveDamaged()
		return nil, 0
	}
	_, _ = file.file.Seek(readOffset, io.SeekStart)
	valid = true
	return file.file, readOffset
}

func (q *fileQueue) IsEmpty() bool {
	return !q.fileRHasNext
}

func (q *fileQueue) Poll() []byte {
	q.fileMutex.Lock()
	defer q.fileMutex.Unlock()

	if !q.fileRHasNext || q.closed != 0 {
		return nil
	}
	fileR := q.fileR
	if fileR == nil {
		q.fileR, q.fileROffset = q.openReadFile()
		if q.fileR == nil {
			q.fileRHasNext = false
			return nil
		}
		fileR = q.fileR
	}

	bigendian.PutInt64(q.rob, q.fileROffset)
	row, _ := fileR.WriteAt(q.rob, fileQueueMetadataOffsetReadOffset)
	if row != len(q.rob) {
		_ = fileR.Close()
		q.fileR = nil
		return nil
	}
	head := q.hb
	read, err := fileR.Read(head)
	if read != len(head) {
		q.fileRHasNext = false
		if read > 0 {
			_, _ = fileR.Seek(q.fileROffset, io.SeekStart)
		} else if read == 0 && err == io.EOF && q.fileROffset >= fileQueueResize {
			q.closeFiles()
			_ = os.Remove(q.filepath)
		}
		return nil
	}
	dataLen := int(bigendian.Int32(head[fileQueueBlockLenOffset:]))
	if head[fileQueueBlockMagicOffset] != fileQueueBlockMagic || dataLen < 0 || dataLen > fileQueueBlockSizeLimit {
		q.resolveDamaged()
		return nil
	}
	var data []byte
	if dataLen > 0 {
		data = q.bufferPool.Alloc(dataLen)
		read, _ = fileR.Read(data)
		if read != dataLen {
			if read > 0 {
				_, _ = q.fileW.Seek(q.fileROffset, io.SeekStart)
			}
			return nil
		}
	}
	q.fileROffset += int64(fileQueueBlockHeadSize) + int64(dataLen)
	return data
}
