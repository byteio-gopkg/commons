package ntts

import (
	"byteio.org/commons/x/errors"
	"strings"
)

type Error struct {
	Message string
	Cause   error
}

func newError(message string) error {
	return &Error{
		Message: message,
	}
}

func newErrorWithCause(message string, cause error) error {
	return &Error{
		Message: message,
		Cause:   cause,
	}
}

func (e *Error) Error() string {
	if e.Cause == nil {
		return "[NTTS]" + e.Message
	} else {
		return "[NTTS]" + e.Message + ", " + e.Cause.Error()
	}
}

func IsNttsError(e error) bool {
	_, ok := e.(*Error)
	return ok
}

func resolveReplyError(reply *packet) error {
	message := string(reply.body)
	if reply.command == replyCodeNttsError {
		return newError(message)
	} else {
		return errors.New(message)
	}
}

// --------------------------------------------------------

var (
	unacceptableCommandErr        = newError("[PROTOCOL]unacceptable command")
	packetBodyTooLargeErr         = newError("[PROTOCOL]body too large")
	packetCannotWithStreamErr     = newError("[PROTOCOL]can't with stream")
	packetInvalidBodyErr          = newError("[PROTOCOL]invalid body")
	packetInvalidEncryptedFlagErr = newError("[PROTOCOL]invalid encrypted flag")
	packetStreamInvalidChunkErr   = newError("[PROTOCOL]invalid stream chunk")

	invalidJsonPayloadErr = newError("[PROTOCOL]invalid json payload")
	invalidJsonReplyErr   = newError("[PROTOCOL]invalid json reply payload")

	clientClosedErr             = newError("[IO]client yet closed")
	channelClosedErr            = newError("[IO]channel yet closed")
	connectionNotAvailableErr   = newError("[IO]connection not available")
	writeTimeoutErr             = newError("[IO]write timeout")
	waitReplyTimeoutErr         = newError("[IO]wait reply timeout")
	requestWindowSizeLimitedErr = newError("[IO]request window size limited")
	pushTimeoutErr              = newError("[IO]push timeout")
	pushWithSaveUnavailableErr  = newError("[IO]push with save unavailable")

	unexpectedReplyErr              = newError("[PROTOCOL]unexpected reply")
	unexpectedConnectReplyStatusErr = newError("[PROTOCOL]unexpected connect reply status")
	invalidConnectReplyTokenErr     = newError("[PROTOCOL]invalid connect reply token")

	contentConsumeTimeoutErr = errors.New("content consume timeout")
	contentConsumeAbortedErr = errors.New("content consume aborted")
	streamClosedErr          = errors.New("stream closed")
	streamClosedLNRErr       = errors.New("stream closed(long time no read)")
	nilWriterErr             = errors.New("nil writer")
)

func whatClosedErr(c *ClientChannel) error {
	if c.client.closed == 0 {
		return channelClosedErr
	}
	return clientClosedErr
}

func isUseClosedNetworkError(err error) bool {
	return strings.Contains(err.Error(), "use of closed network")
}
