package ntts

import (
	"byteio.org/commons/utils/randutil"
	"net"
	"sync/atomic"
	"time"
)

const (
	quickRequestTimeout = 5 * time.Minute
)

type Quick struct {
	address      string
	encoding     *Encoding
	withTls      bool
	nextSequence int32
}

func NewQuick(address string, secret string, withTls bool) (*Quick, error) {
	encoding, err := newEncoding(secret)
	if err != nil {
		return nil, err
	}
	return &Quick{
		address:  address,
		encoding: encoding,
		withTls:  withTls,
	}, err
}

func (r *Quick) Request(command Command, payload *Content, timeout time.Duration) (*Content, error) {
	if timeout <= 0 {
		timeout = quickRequestTimeout
	}
	dailTimeout := connectTimeout
	if dailTimeout > timeout {
		dailTimeout = timeout
	}
	deadline := time.Now().Add(timeout)
	conn, err := net.DialTimeout("tcp", r.address, dailTimeout)
	if err != nil {
		_ = payload.Close()
		return nil, newErrorWithCause("connect failed", err)
	}
	connCloseable := true
	defer func() {
		if connCloseable {
			_ = conn.Close()
		}
	}()
	_ = conn.SetDeadline(deadline)
	encoding := r.encoding

	// Execute 'CONNECT'
	cpd := &connectPayload{
		uid:       QUICK_UID,
		token:     randutil.AlphanumericString(16),
		tlsEnable: r.withTls,
		quick:     true,
	}
	pkt := &packet{sequence: atomic.AddInt32(&r.nextSequence, 1), command: COMMAND_CONNECT}
	pkt.setBody(cpd.encode(), true)
	err = requestConnect(conn, r.encoding, pkt, cpd.token)
	if err != nil {
		return nil, newErrorWithCause("connect failed", err)
	}

	// Write request and read reply
	pkt = &packet{sequence: atomic.AddInt32(&r.nextSequence, 1), command: command}
	pkt.setContent(payload, r.withTls)
	var reply *packet
	reply, err = requestWriteAndRead(conn, encoding, pkt)
	if err != nil {
		_ = payload.Close()
		return nil, newErrorWithCause("request failed", err)
	}
	if reply.command != replyCodeOk {
		return nil, resolveReplyError(reply)
	}
	if reply.stream != nil {
		connCloseable = false
		go func() {
			_ = waitStreamConsumed(reply.stream.(*ContentStream), nil)
			_ = conn.Close()
		}()
	}

	return reply.getContent(), err
}

func (r *Quick) RequestJSON(command Command, payload any, retPtr any, timeout time.Duration) error {
	var requestContent *Content
	if payload != nil {
		body, _ := MarshalJSONFunc(payload)
		requestContent = NewContent(body)
	}
	ret, err := r.Request(command, requestContent, timeout)
	if err != nil {
		return err
	}
	retBody := ret.GetBody()
	if retBody != nil && retPtr != nil {
		err = UnmarshalJSONFunc(retBody, retPtr)
		if err != nil {
			return invalidJsonReplyErr
		}
		r.encoding.bufferPool.Put(retBody)
	}
	return nil
}

type QuickRequester struct {
	conn net.Conn
}

func (r *QuickRequester) Client() *Client {
	return nil
}

func (r *QuickRequester) Uid() string {
	return QUICK_UID
}

func (r *QuickRequester) RemoteIp() net.IP {
	return r.conn.RemoteAddr().(*net.TCPAddr).IP
}

func (r *QuickRequester) Context() any {
	return nil
}

func (r *QuickRequester) SetContext(_ any) {
}

func (s *Server) handleQuickRequest(conn net.Conn, withEncrypted bool) {
	_ = conn.SetDeadline(time.Now().Add(quickRequestTimeout))

	// Read and handle
	pkt, _ := readPacket(conn, s.encoding, withEncrypted, nil)
	if pkt == nil || pkt.flags&packetFlagReply != 0 {
		return
	}
	reply := handleRequest(&QuickRequester{conn: conn}, s.handlers[pkt.command], pkt)
	pkt.autoCloseNow()

	// Write reply
	encoding := s.encoding
	_ = writePacket(conn, encoding, reply)
	reply.autoCloseNow()
}
