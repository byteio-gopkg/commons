package ntts

import (
	"byteio.org/commons/x/errors"
	"byteio.org/commons/x/logging"
	"byteio.org/commons/x/schedule"
	"fmt"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

const (
	serverBacklog              = 64
	serverAcceptSleepIfBacklog = 100 * time.Millisecond
)

type Server struct {
	address       string
	config        *ServerConfig
	listener      net.Listener
	clients       map[string]*Client
	clientsLock   sync.RWMutex
	clientBacklog int32
	encoding      *Encoding
	handlers      Handlers
	logger        logging.Logger
	closed        int32
	cleaner       schedule.Schedule
}

type ServerConfig struct {
	Address       string
	Secret        string
	PushSavePath  string
	ClientAllowed ClientAllowed
}

type ClientAllowed func(uid string, ip net.IP) (bool, string)

func Listen(config *ServerConfig, handlers Handlers, logger logging.Logger) (*Server, error) {
	if config == nil {
		return nil, errors.NewIllegalArgumentError("config is nil")
	}
	encoding, err := newEncoding(config.Secret)
	if err != nil {
		return nil, err
	}
	listener, err := net.Listen("tcp", config.Address)
	if err != nil {
		return nil, err
	}
	if handlers == nil {
		handlers = make(Handlers)
	}
	s := &Server{
		address:  config.Address,
		config:   config,
		listener: listener,
		clients:  make(map[string]*Client),
		encoding: encoding,
		handlers: handlers,
		logger:   logger.NewChild("NttsServer[" + config.Address + "]"),
		closed:   0,
	}
	s.cleaner = schedule.Repeating(s.cleanUp, clientCleanupInterval, clientCleanupInterval)
	go s.loopAccept()
	return s, nil
}

func (s *Server) Close() error {
	if !atomic.CompareAndSwapInt32(&s.closed, 0, 1) {
		return nil
	}
	_ = s.listener.Close()
	s.listener = nil

	s.cleaner.Cancel()
	s.cleaner = nil

	clients := make([]*Client, 0, len(s.clients))
	s.clientsLock.Lock()
	for _, client := range s.clients {
		clients = append(clients, client)
	}
	clear(s.clients)
	s.clientsLock.Unlock()
	for _, client := range clients {
		_ = client.Close()
	}

	s.logger.Info("closed")
	_ = s.logger.Flush()
	_ = s.logger.Close()
	return nil
}

func (s *Server) FindClient(uid string) *Client {
	s.clientsLock.RLock()
	client := s.clients[uid]
	s.clientsLock.RUnlock()
	return client
}

func (s *Server) ListClients(filter func(*Client) bool) []*Client {
	clients := make([]*Client, 0)
	s.clientsLock.RLock()
	defer s.clientsLock.RUnlock()
	for _, client := range s.clients {
		if filter == nil || filter(client) {
			clients = append(clients, client)
		}
	}
	return clients
}

func (s *Server) ensureClient(uid string) (*Client, bool) {
	isNew := false
	s.clientsLock.Lock()
	defer s.clientsLock.Unlock()
	client := s.clients[uid]
	if client == nil {
		logger := s.logger.NewChild(fmt.Sprintf("NttsClient[%s<-%s]", s.address, uid))
		client = newClient(uid, s.encoding, s.handlers, s.config.PushSavePath, logger)
		client.server = s
		s.clients[uid] = client
		isNew = true
	}
	client.cleanable = false
	return client, isNew
}

func (s *Server) removeClient(c *Client) {
	s.clientsLock.Lock()
	delete(s.clients, c.uid)
	s.clientsLock.Unlock()
}

func (s *Server) Handlers() Handlers {
	return s.handlers
}

func (s *Server) Logger() logging.Logger {
	return s.logger
}

func (s *Server) loopAccept() {
	ln := s.listener
	for s.closed == 0 {
		if s.clientBacklog >= serverBacklog {
			time.Sleep(serverAcceptSleepIfBacklog)
			continue
		}
		conn, err := ln.Accept()
		if err != nil {
			if isUseClosedNetworkError(err) {
				return
			}
			s.logger.Warn("accept error: ", err.Error())
			time.Sleep(connectRetryInterval)
			continue
		}
		go s.connAccepted(conn)
		s.logger.Info("accepted new connection ", conn.RemoteAddr().String())
	}
}

func (s *Server) connAccepted(conn net.Conn) {
	atomic.AddInt32(&s.clientBacklog, 1)
	defer atomic.AddInt32(&s.clientBacklog, -1)

	connected := false
	encoding := s.encoding
	defer func() {
		if !connected {
			_ = conn.Close()
		}
	}()
	addr := conn.RemoteAddr().(*net.TCPAddr)

	// Read first packet 'CONNECT'
	_ = conn.SetDeadline(time.Now().Add(connectTimeout))
	pkt, err := readPacket(conn, encoding, true, nil)
	if err != nil || s.closed != 0 {
		return
	}
	if pkt.command != COMMAND_CONNECT || pkt.flags&packetFlagReply != 0 || pkt.stream != nil {
		return
	}
	payload := &connectPayload{}
	if !payload.decode(pkt.body) {
		return
	}
	if payload.uid == "" || len(payload.uid) > 256 {
		_ = writePacket(conn, encoding, newReplyPacketWithBody(pkt, replyCodeNttsError, []byte("[PROTOCOL]invalid client UID")))
		return
	}
	if clientAllowed := s.config.ClientAllowed; clientAllowed != nil {
		allowed, reason := clientAllowed(payload.uid, addr.IP)
		if !allowed {
			if reason == "" {
				reason = "client not allowed"
			}
			_ = writePacket(conn, encoding, newReplyPacketWithBody(pkt, replyCodeNttsError, []byte("[PROTOCOL]"+reason)))
			return
		}
	}
	if s.closed != 0 {
		return
	}
	if payload.quick {
		err = writePacket(conn, encoding, newReplyPacketWithBody(pkt, replyCodeOk, []byte(payload.token)))
		if err != nil {
			return
		}
		s.handleQuickRequest(conn, payload.tlsEnable)
		return
	}

	// Connected
	client, isNewClient := s.ensureClient(payload.uid)
	if isNewClient {
		client.tlsEnabled = payload.tlsEnable
		if handler := s.handlers[EVENT_CLIENT_CREATED]; handler != nil {
			_, _ = handler(client, nil)
		}
	}
	channel := client.ensureChannel(payload.channel)
	if channel == nil {
		return
	}
	if !client.remoteCNIncrease(addr.IP) {
		_ = writePacket(conn, encoding, newReplyPacketWithBody(pkt, replyCodeNttsError, []byte("[PROTOCOL]only 1 ip allowed for per UID")))
		return
	}
	err = writePacket(conn, encoding, newReplyPacketWithBody(pkt, replyCodeOk, []byte(payload.token)))
	if err != nil {
		client.remoteCNDecrease()
		return
	}
	connected = true
	c := channel.newConn(false)
	c.logger = s.logger.NewChild(fmt.Sprintf("NttsConn[%s<-%s]", conn.LocalAddr().String(), conn.RemoteAddr().String()))
	c.logger.Info("connected, client: ", client.uid, ", channel: ", channel.key)
	c.connected(conn)
}

func (s *Server) cleanUp() {
	if s.closed != 0 {
		return
	}

	var closeClients []*Client
	s.clientsLock.Lock()
	for _, c := range s.clients {
		c.cleanUp()
		if len(c.channels) < 1 {
			if c.cleanable {
				delete(s.clients, c.uid)
				closeClients = append(closeClients, c)
			} else {
				c.cleanable = true
			}
		} else {
			c.cleanable = false
		}
	}
	s.clientsLock.Unlock()

	for _, c := range closeClients {
		_ = c.Close()
	}
	_ = s.logger.Flush()
}

func (s *Server) String() string {
	return "NttsServer/" + s.address
}

func (s *Server) Stats() map[string]any {
	ret := map[string]any{
		"address": s.address,
		"clients": len(s.clients),
	}
	channels := 0
	connections := 0
	s.clientsLock.Lock()
	for _, client := range s.clients {
		channels++
		client.channelsLock.Lock()
		for _, channel := range client.channels {
			connections += len(channel.conns)
		}
		client.channelsLock.Unlock()
	}
	s.clientsLock.Unlock()
	ret["channels"] = channels
	ret["connections"] = connections
	return ret
}
