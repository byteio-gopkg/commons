package ntts

import (
	"byteio.org/commons/utils/ioutil"
	"byteio.org/commons/x/buffer"
	"byteio.org/commons/x/encoding/bigendian"
	"errors"
	"io"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

type ContentStream struct {
	conn            net.Conn
	encoding        *Encoding
	bufferPool      *buffer.Pool
	flags           uint8
	readLock        sync.Mutex
	err             error
	errCtl          int32
	consumedC       chan error
	consumedN       int
	chunkHead       []byte
	lastChunk       []byte
	lastChunkOffset int
}

func newContentStream(conn net.Conn, encoding *Encoding, packetFlags uint8) *ContentStream {
	return &ContentStream{
		conn:       conn,
		encoding:   encoding,
		bufferPool: encoding.bufferPool,
		flags:      packetFlags,
		errCtl:     0,
		consumedC:  make(chan error, 1),
		chunkHead:  make([]byte, 4),
	}
}

func (s *ContentStream) readChunk() ([]byte, error) {
	s.consumedN++
	if err := ioutil.ReadFully(s.conn, s.chunkHead); err != nil {
		if err == io.EOF {
			return nil, io.ErrUnexpectedEOF
		}
		return nil, err
	}
	chunkLen := bigendian.Int32(s.chunkHead)
	if chunkLen < 0 || chunkLen > packetStreamChunkSizeLimit {
		return nil, packetStreamInvalidChunkErr
	}
	if chunkLen > 0 {
		chunk := s.bufferPool.Alloc(int(chunkLen))
		if err := ioutil.ReadFully(s.conn, chunk); err != nil {
			if err == io.EOF {
				return nil, io.ErrUnexpectedEOF
			}
			return nil, err
		}
		chunk = s.encoding.decode(chunk, s.flags)
		if chunk == nil {
			return nil, packetStreamInvalidChunkErr
		}
		return chunk, nil
	} else {
		return nil, nil
	}
}

func (s *ContentStream) Read(buf []byte) (n int, err error) {
	s.readLock.Lock()
	defer s.readLock.Unlock()
	chunk := s.lastChunk
	if chunk != nil {
		n = copy(buf, chunk[s.lastChunkOffset:])
		s.lastChunkOffset += n
		if s.lastChunkOffset >= len(chunk) {
			s.lastChunk = nil
			s.lastChunkOffset = 0
			s.bufferPool.Put(chunk)
		}
		if n == len(buf) {
			return
		}
	}
	var copied int
	for {
		if err = s.err; err != nil {
			break
		}
		chunk, err = s.readChunk()
		if err != nil {
			s.finishConsume(err, true)
			break
		}
		if chunk != nil {
			copied = copy(buf[n:], chunk)
			n += copied
			if copied < len(chunk) {
				s.lastChunk = chunk
				s.lastChunkOffset = copied
				break
			} else {
				s.bufferPool.Put(chunk)
			}
		} else {
			s.finishConsume(nil, true)
		}
	}
	return
}

func (s *ContentStream) WriteTo(w io.Writer) (n int64, err error) {
	if w == nil {
		return 0, nilWriterErr
	}
	s.readLock.Lock()
	defer s.readLock.Unlock()
	var written int
	chunk := s.lastChunk
	if chunk != nil {
		written, err = w.Write(chunk[s.lastChunkOffset:])
		s.lastChunkOffset += written
		n += int64(written)
		if s.lastChunkOffset >= len(chunk) {
			s.lastChunk = nil
			s.lastChunkOffset = 0
			s.bufferPool.Put(chunk)
		} else {
			return
		}
		if err != nil {
			return
		}
	}
	for {
		if err = s.err; err != nil {
			if err == io.EOF {
				err = nil
			}
			break
		}
		chunk, err = s.readChunk()
		if err != nil {
			s.finishConsume(err, true)
			break
		}
		if chunk != nil {
			written, err = w.Write(chunk)
			n += int64(written)
			if written < len(chunk) {
				s.lastChunk = chunk
				s.lastChunkOffset = written
				break
			} else {
				s.bufferPool.Put(chunk)
			}
			if err != nil {
				break
			}
		} else {
			s.finishConsume(nil, true)
		}
	}
	return
}

func (s *ContentStream) finishConsume(err error, consumed bool) {
	if atomic.CompareAndSwapInt32(&s.errCtl, 0, 1) {
		if err == nil {
			s.err = io.EOF
		} else {
			s.err = err
		}
		if consumed {
			s.consumedC <- err
		}
	}
}

func (s *ContentStream) Close() error {
	if atomic.CompareAndSwapInt32(&s.errCtl, 0, 1) {
		s.err = streamClosedErr
		s.consumedC <- contentConsumeAbortedErr
	}
	return nil
}

func waitStreamConsumed(stream *ContentStream, closedC <-chan struct{}) error {
	stagnated := time.NewTimer(packetStreamConsumeStagnated)
	defer stagnated.Stop()
	var consumedN int
	var err error
	for {
		consumedN = stream.consumedN
		select {
		case err = <-stream.consumedC:
			return err
		case <-closedC:
			stream.finishConsume(io.ErrClosedPipe, false)
			return nil
		case <-stagnated.C:
			if consumedN != stream.consumedN {
				stagnated.Reset(packetStreamConsumeStagnated)
				continue
			}
			stream.finishConsume(streamClosedLNRErr, false)
			return contentConsumeTimeoutErr
		}
	}
}

func writeStream(conn net.Conn, encoding *Encoding, pkt *packet, closedPtr *int32) error {
	stream := pkt.stream
	if stream == nil {
		return ioutil.WriteFully(conn, bigendian.Int32Bytes(0))
	}
	chunk := encoding.bufferPool.Alloc(4096)
	for *closedPtr == 0 {
		chunkLen, readErr := stream.Read(chunk)
		if chunkLen > 0 {
			chunkOut, chunkOutCanFree := encoding.encode(chunk[:chunkLen], pkt.flags)
			if err := ioutil.WriteFully(conn, bigendian.Int32Bytes(int32(len(chunkOut)))); err != nil {
				return err
			}
			if err := ioutil.WriteFully(conn, chunkOut); err != nil {
				return err
			}
			if chunkOutCanFree {
				encoding.bufferPool.Put(chunkOut)
			}
		}
		if readErr != nil {
			if readErr == io.EOF {
				if err := ioutil.WriteFully(conn, bigendian.Int32Bytes(0)); err != nil {
					return err
				}
			} else {
				return errors.New("read stream error, " + readErr.Error())
			}
			break
		}
	}
	encoding.bufferPool.Put(chunk)
	return nil
}
