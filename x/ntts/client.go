package ntts

import (
	. "byteio.org/commons"
	"byteio.org/commons/utils/ioutil"
	"byteio.org/commons/utils/randutil"
	"byteio.org/commons/utils/runtimeutil"
	"byteio.org/commons/utils/timeutil"
	"byteio.org/commons/x/buffer"
	"byteio.org/commons/x/errors"
	"byteio.org/commons/x/id/uuid"
	"byteio.org/commons/x/logging"
	"byteio.org/commons/x/schedule"
	"fmt"
	"io"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

const (
	clientReplyingLimit           = 128
	clientReplyingAcquireInterval = 200 * time.Millisecond
	clientCleanupInterval         = 1 * time.Minute
	clientConnWriteQueueSize      = 4
	clientConnWriterIdleTimeout   = 15 * time.Second
	clientConnWriterIdleIfWritten = 15 * 50

	clientChannelDefaultKey            ChannelKey = 0
	clientChannelMaxConns              int        = 4
	clientChannelNewConnInterval                  = 3 * time.Second
	clientChannelNewConnIfWriteBlocked            = 16

	handleIdleTimeout = 15 * time.Second
)

type Client struct {
	uid           string
	serverAddress string
	server        *Server
	channels      map[ChannelKey]*ClientChannel
	channelsLock  sync.Mutex
	connN         int32
	remoteIp      net.IP
	remoteCN      int
	bufferPool    *buffer.Pool
	encoding      *Encoding
	handlers      Handlers
	tlsEnabled    bool
	pushSavePath  string
	pusherM       *Pusher
	pusherF       *Pusher
	logger        logging.Logger
	closed        int32
	context       any
	cleanable     bool
	cleaner       schedule.Schedule
}

type ClientChannel struct {
	client        *Client
	key           ChannelKey
	conns         map[uint32]*ClientConn
	connsLock     sync.Mutex
	connNewLock   sync.Mutex
	connNewCtl    uint32
	nextConnId    uint32
	nextSequence  int32
	writeQueue    chan *writeTask
	writableN     int32
	writeBlocked  int32
	writeRetryC   chan int
	handleQueue   chan *handleTask
	replyTable    sync.Map
	replyChanPool sync.Pool
	replying      int32
	closed        int32
	closedC       chan struct{}
	connectingN   int32
	busy          uint32
	cleanable     bool
}

type ClientConn struct {
	id          uint32
	channel     *ClientChannel
	conn        net.Conn
	readAllowed bool
	writeQueue  chan *writeTask
	keepAlive   bool
	logger      logging.Logger
	available   int32
	closed      int32
	closedC     chan struct{}
}

type ClientConfig struct {
	Uid          string
	Address      string
	Secret       string
	EnableTls    bool
	PushSavePath string
}

func newClient(uid string, encoding *Encoding, handlers Handlers, pushSavePath string, logger logging.Logger) *Client {
	c := &Client{
		uid:          uid,
		channels:     make(map[ChannelKey]*ClientChannel),
		connN:        0,
		remoteCN:     0,
		bufferPool:   encoding.bufferPool,
		encoding:     encoding,
		handlers:     handlers,
		pushSavePath: pushSavePath,
		logger:       logger,
		closed:       0,
	}
	c.initPusher()
	return c
}

func NewClient(config *ClientConfig, handlers Handlers, logger logging.Logger) (*Client, error) {
	if config == nil {
		return nil, errors.NewIllegalArgumentError("config is nil")
	}
	uid := config.Uid
	if uid == "" {
		uid = uuid.Rand()
	}
	address, _ := net.ResolveTCPAddr("", config.Address)
	if address == nil {
		return nil, errors.NewIllegalArgumentError("config.address is invalid")
	}
	encoding, err := newEncoding(config.Secret)
	if err != nil {
		return nil, err
	}
	if handlers == nil {
		handlers = make(Handlers)
	}
	logger = logger.NewChild(fmt.Sprintf("NttsClient[->%s]", config.Address))
	c := newClient(uid, encoding, handlers, config.PushSavePath, logger)
	c.remoteIp = address.IP
	c.serverAddress = config.Address
	c.tlsEnabled = config.EnableTls
	c.cleaner = schedule.Repeating(c.cleanUp, clientCleanupInterval, clientCleanupInterval)
	return c, nil
}

func (c *Client) Client() *Client {
	return c
}

func (c *Client) Uid() string {
	return c.uid
}

func (c *Client) Handlers() Handlers {
	return c.handlers
}

func (c *Client) Logger() logging.Logger {
	return c.logger
}

func (c *Client) RemoteIp() net.IP {
	return c.remoteIp
}

func (c *Client) Context() any {
	return c.context
}

func (c *Client) SetContext(context any) {
	c.context = context
}

func (c *Client) Close() error {
	if !atomic.CompareAndSwapInt32(&c.closed, 0, 1) {
		return nil
	}
	if cleaner := c.cleaner; cleaner != nil {
		cleaner.Cancel()
		c.cleaner = nil
	}
	for _, pusher := range []*Pusher{c.pusherM, c.pusherF} {
		if pusher != nil {
			pusher.shutdown()
		}
	}
	handlers := c.handlers
	c.handlers = nil

	channels := make([]*ClientChannel, 0)
	c.channelsLock.Lock()
	for _, channel := range c.channels {
		channels = append(channels, channel)
	}
	clear(c.channels)
	c.channelsLock.Unlock()
	for _, channel := range channels {
		channel.close()
	}

	if c.server != nil {
		c.server.removeClient(c)
	}
	c.logger.Info("closed")
	_ = c.logger.Flush()
	if c.server == nil {
		_ = c.logger.Close()
	}
	if handler := handlers[EVENT_CLIENT_CLOSED]; handler != nil {
		_, _ = handler(c, nil)
	}
	return nil
}

func (c *Client) cleanUp() {
	var closeChannels []*ClientChannel

	c.channelsLock.Lock()
	for _, channel := range c.channels {
		cleanable := len(channel.conns) == 0 && channel.connectingN == 0
		if cleanable {
			if channel.cleanable {
				delete(c.channels, channel.key)
				closeChannels = append(closeChannels, channel)
			} else {
				channel.cleanable = true
			}
		} else {
			channel.cleanable = false
		}
	}
	c.channelsLock.Unlock()

	if c.server == nil {
		for _, channel := range closeChannels {
			channel.close()
		}
		_ = c.logger.Flush()
	} else {
		if len(closeChannels) > 0 {
			go func() {
				for _, channel := range closeChannels {
					channel.close()
				}
			}()
		}
	}
}

// --------------------------------------------------------

func (c *Client) remoteCNIncrease(addr net.IP) bool {
	allowed := true
	c.channelsLock.Lock()
	if c.remoteCN == 0 {
		c.remoteIp = addr
	} else if !c.remoteIp.Equal(addr) {
		allowed = false
	}
	if allowed {
		c.remoteCN++
	}
	c.channelsLock.Unlock()
	return allowed
}

func (c *Client) remoteCNDecrease() {
	c.channelsLock.Lock()
	if c.remoteCN > 0 {
		c.remoteCN--
		if c.remoteCN == 0 {
			c.remoteIp = nil
		}
	}
	c.channelsLock.Unlock()
}

// --------------------------------------------------------

func (c *Client) ensureChannel(channelKey ChannelKey) *ClientChannel {
	c.channelsLock.Lock()
	defer c.channelsLock.Unlock()

	channel := c.channels[channelKey]
	if channel != nil {
		channel.cleanable = false
		return channel
	}
	if c.closed != 0 {
		return nil
	}

	channel = &ClientChannel{
		client:       c,
		key:          channelKey,
		conns:        make(map[uint32]*ClientConn),
		connNewCtl:   0,
		writeQueue:   make(chan *writeTask),
		writableN:    0,
		writeBlocked: 0,
		writeRetryC:  make(chan int, 1),
		handleQueue:  make(chan *handleTask),
		replying:     0,
		closed:       0,
		closedC:      make(chan struct{}),
		connectingN:  0,
		busy:         0,
		cleanable:    false,
	}
	channel.replyChanPool.New = newReplyChan
	c.channels[channelKey] = channel
	if c.server == nil {
		channel.startNewConn(channelKey == clientChannelDefaultKey)
	}
	return channel
}

func (c *Client) removeChannel(channelKey ChannelKey) {
	c.channelsLock.Lock()
	delete(c.channels, channelKey)
	c.channelsLock.Unlock()
}

func (c *ClientChannel) addConn(conn *ClientConn) {
	c.connsLock.Lock()
	c.conns[conn.id] = conn
	c.connsLock.Unlock()
}

func (c *ClientChannel) removeConn(conn *ClientConn) {
	c.connsLock.Lock()
	delete(c.conns, conn.id)
	c.connsLock.Unlock()
}

func (c *ClientChannel) startNewConn(keepAlive bool) {
	if c.closed != 0 {
		return
	}
	var newConn *ClientConn
	if keepAlive {
		newConn = c.newConn(keepAlive)
	} else {
		if len(c.conns) >= clientChannelMaxConns {
			return
		}
		newConn = c.newConn(keepAlive)
	}
	if newConn != nil {
		newConn.setAvailable(true)
		go newConn.loopConnect()
	}
}

func (c *ClientChannel) newConn(keepAlive bool) *ClientConn {
	conn := &ClientConn{
		id:          atomic.AddUint32(&c.nextConnId, 1),
		channel:     c,
		readAllowed: true,
		writeQueue:  make(chan *writeTask, clientConnWriteQueueSize),
		keepAlive:   keepAlive,
		closed:      0,
		available:   0,
		closedC:     make(chan struct{}),
	}
	c.addConn(conn)
	return conn
}

func (c *ClientChannel) newConns() {
	c.connNewCtl = 1
	defer func() {
		c.connNewCtl = 0
	}()
	retryTick := time.NewTicker(time.Second)
	defer retryTick.Stop()
	const MaxCheckLoop = 7
	checkLoop := MaxCheckLoop
	newConnAllowedNT := timeutil.RuntimeNano() + clientChannelNewConnInterval.Nanoseconds()
	for checkLoop > 0 && c.closed == 0 {
		writableN := c.writableN
		if int(writableN) < clientChannelMaxConns && (writableN == 0 || (c.connectingN == 0 && c.writeBlocked >= writableN*clientChannelNewConnIfWriteBlocked && timeutil.RuntimeNano() >= newConnAllowedNT)) {
			if c.client.server == nil {
				c.startNewConn(false)
			} else {
				err := c.inviteNewConn()
				if err != nil {
					return
				}
			}
			newConnAllowedNT = timeutil.RuntimeNano() + clientChannelNewConnInterval.Nanoseconds()
		}
		checkLoop--
		c.connsLock.Lock()
		if c.connNewCtl == 2 {
			c.connNewCtl = 1
			checkLoop = MaxCheckLoop
		}
		c.connsLock.Unlock()
		select {
		case <-retryTick.C:
		case <-c.writeRetryC:
		case <-c.closedC:
			return
		}
	}
}

func (c *ClientChannel) wantNewConn() {
	startNewer := false
	c.connsLock.Lock()
	if c.connNewCtl == 0 {
		startNewer = true
	}
	c.connNewCtl = 2
	c.connsLock.Unlock()
	if startNewer {
		go c.newConns()
	}
}

func (c *ClientChannel) isNoAvailableConns() bool {
	return c.writableN-c.connectingN < 1
}

func (c *ClientConn) connected(conn net.Conn) {
	c.conn = conn
	c.setAvailable(true)
	client := c.channel.client
	connN := atomic.AddInt32(&client.connN, 1)
	if connN == 1 {
		client.startPusher()
	}

	_ = conn.SetDeadline(time.Time{})
	go c.loopRead()
	go c.loopWrite()
}

func (c *ClientConn) connect() error {
	channel := c.channel
	client := channel.client
	conn, err := net.DialTimeout("tcp", client.serverAddress, connectTimeout)
	if err != nil {
		return err
	}
	defer func() {
		if c.conn == nil {
			_ = conn.Close()
		}
	}()

	// Execute 'CONNECT'
	_ = conn.SetDeadline(time.Now().Add(connectTimeout))
	payload := &connectPayload{
		uid:       client.uid,
		channel:   channel.key,
		token:     randutil.AlphanumericString(16),
		tlsEnable: client.tlsEnabled,
		quick:     false,
	}
	err = requestConnect(conn, client.encoding, newPacketWithBody(channel, COMMAND_CONNECT, payload.encode(), true), payload.token)
	if err != nil {
		return err
	}

	// Connected
	c.logger = client.logger.NewChild(fmt.Sprintf("NttsConn[%s->%s]", conn.LocalAddr().String(), conn.RemoteAddr().String()))
	c.connected(conn)
	return nil
}

func (c *ClientConn) loopConnect() {
	channel := c.channel
	client := channel.client
	atomic.AddInt32(&channel.connectingN, 1)
	defer atomic.AddInt32(&channel.connectingN, -1)

	connected := false
	retry := 0
	for c.closed == 0 {
		err := c.connect()
		if err != nil {
			client.logger.Warn("connect failed: ", err.Error())
			retry++
			interval := connectRetryInterval
			if retry <= connectRetryFasts {
				interval = connectRetryFastInterval
			}
			intervalTimer := time.NewTimer(interval)
			select {
			case <-intervalTimer.C:
			case <-c.closedC:
			}
			intervalTimer.Stop()
		} else {
			connected = true
			c.logger.Info("connected, channel: ", c.channel.key)
			break
		}
	}
	if !connected || c.closed != 0 {
		c.close()
		if netConn := c.conn; c.conn != nil {
			_ = netConn.Close()
		}
	}
}

func (c *ClientConn) setAvailable(available bool) {
	channel := c.channel
	if available {
		if atomic.CompareAndSwapInt32(&c.available, 0, 1) {
			atomic.AddInt32(&channel.writableN, 1)
		}
	} else {
		if atomic.CompareAndSwapInt32(&c.available, 1, 0) {
			atomic.AddInt32(&channel.writableN, -1)
			SendToChan(channel.writeRetryC, -1)
		}
	}
}

func (c *ClientConn) close() {
	if !atomic.CompareAndSwapInt32(&c.closed, 0, 1) {
		return
	}
	close(c.closedC)
	c.readAllowed = false
	SendToChan(c.writeQueue, nil)
	c.setAvailable(false)

	channel := c.channel
	if channel != nil {
		channel.removeConn(c)
		if client := channel.client; client != nil {
			if c.conn != nil {
				atomic.AddInt32(&client.connN, -1)
			}
			if client.server != nil {
				client.remoteCNDecrease()
			}
		}
	}

	netConn := c.conn
	c.conn = nil
	if netConn != nil {
		_ = netConn.Close()
		c.logger.Info("closed")
	}

	closeWriteTasks(c.writeQueue)
	return
}

func (c *ClientChannel) close() {
	if !atomic.CompareAndSwapInt32(&c.closed, 0, 1) {
		return
	}
	close(c.closedC)
	c.client.removeChannel(c.key)

	conns := make([]*ClientConn, 0)
	c.connsLock.Lock()
	for _, conn := range c.conns {
		conns = append(conns, conn)
	}
	clear(c.conns)
	c.connsLock.Unlock()
	for _, conn := range conns {
		conn.close()
	}
}

func (c *ClientConn) loopRead() {
	err := c.loopRead0()
	if c.closed != 0 {
		return
	}
	if err != nil && !isUseClosedNetworkError(err) && (c.readAllowed || err != io.EOF) {
		c.logger.Warn("read error: ", err.Error())
	}
	channel := c.channel
	c.close()

	// Reconnect
	if c.keepAlive && channel.closed == 0 {
		channel.startNewConn(true)
	}
}

func (c *ClientConn) loopRead0() error {
	conn := c.conn
	channel := c.channel
	tlsEnabled := channel.client.tlsEnabled
	encoding := channel.client.encoding
	handleQueue := channel.handleQueue
	headBuf := make([]byte, packetHeadSize)
	for c.readAllowed {
		if err := ioutil.ReadFully(conn, headBuf); err != nil {
			return err
		}
		pkt := new(packet)
		pkt.decodeHead(headBuf)
		if tlsEnabled != (pkt.flags&packetFlagEncrypted != 0) {
			return packetInvalidEncryptedFlagErr
		}
		if err := readPacketContent(conn, encoding, pkt); err != nil {
			return err
		}
		ht := &handleTask{
			pkt, c,
		}
		select {
		case handleQueue <- ht:
		default:
			go channel.loopHandle(ht)
		}

		if pkt.stream != nil {
			err := waitStreamConsumed(pkt.stream.(*ContentStream), c.closedC)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

type writeTask struct {
	p         *packet
	cancelled bool
}

func closeWriteTasks(queue <-chan *writeTask) {
	for {
		select {
		case wt := <-queue:
			if wt != nil {
				wt.p.autoCloseNow()
			}
		default:
			return
		}
	}
}

func (c *ClientConn) loopWrite() {
	defer c.setAvailable(false)
	conn := c.conn
	headBuf := make([]byte, packetHeadSize)
	channel := c.channel
	client := channel.client
	encoding := client.encoding

	var idleTimer *time.Timer
	var isIdle = false
	if client.server == nil && !c.keepAlive {
		idleTimer = time.NewTimer(clientConnWriterIdleTimeout)
		defer idleTimer.Stop()
	}
	lastChannelBusy := channel.busy
	written := 0
	for c.closed == 0 {
		var wt *writeTask
		if isIdle {
			wt = <-c.writeQueue
		} else {
			if idleTimer == nil {
				select {
				case wt = <-c.writeQueue:
				case wt = <-channel.writeQueue:
				}
			} else {
				select {
				case wt = <-c.writeQueue:
				case wt = <-channel.writeQueue:
				case <-idleTimer.C:
					if written >= clientConnWriterIdleIfWritten || (channel.writableN == 1 && channel.busy != lastChannelBusy) {
						idleTimer.Reset(clientConnWriterIdleTimeout)
						written = 0
						lastChannelBusy = channel.busy
						continue
					}
					isIdle = true
					c.setAvailable(false)
					wt = &writeTask{p: prepareTerminatePacket(c)}
				}
				written++
			}
		}
		if wt == nil {
			if isIdle {
				break
			} else {
				c.setAvailable(false)
				isIdle = true
				continue
			}
		}
		if wt.cancelled {
			continue
		}
		pkt := wt.p
		var body []byte
		bodyCanFree := false
		if pkt.bodySize > 0 {
			body, bodyCanFree = encoding.encode(pkt.body, pkt.flags)
			pkt.bodySize = uint32(len(body))
		}
		pkt.encodeHead(headBuf)
		if err := ioutil.WriteFully(conn, headBuf); err != nil {
			c.writeFailed(conn, pkt, err)
			return
		}
		if body != nil {
			if err := ioutil.WriteFully(conn, body); err != nil {
				c.writeFailed(conn, pkt, err)
				return
			}
			if bodyCanFree {
				encoding.bufferPool.Put(body)
			}
		}
		if pkt.flags&packetFlagWithStream != 0 {
			err := writeStream(conn, encoding, pkt, &c.closed)
			if err != nil {
				c.writeFailed(conn, pkt, err)
				return
			} else {
				pkt.autoCloseNow()
			}
		}
	}
}

func (c *ClientConn) writeFailed(conn net.Conn, pkt *packet, err error) {
	replyC, _ := c.channel.replyTable.LoadAndDelete(pkt.sequence)
	if replyC != nil {
		SendToChan(replyC.(replyChan), &replyResult{nil, newErrorWithCause("write failed", err)})
	}
	if c.closed == 0 && !isUseClosedNetworkError(err) {
		c.logger.Warn("write error: ", err.Error())
		_ = conn.Close()
	}
	pkt.autoCloseNow()
}

// --------------------------------------------------------

type handleTask struct {
	p *packet
	c *ClientConn
}

type replyResult struct {
	payload *Content
	err     error
}

type replyChan = chan *replyResult

func newReplyChan() any {
	return make(replyChan, 1)
}

func (c *ClientChannel) loopHandle(ht *handleTask) {
	defer runtimeutil.RecoverWithErrorPrint()

	timeout := time.NewTimer(handleIdleTimeout)
	defer timeout.Stop()
	client := c.client
	handleQueue := c.handleQueue
	for c.closed == 0 {
		if ht == nil {
			select {
			case ht = <-handleQueue:
			default:
			}
			if ht == nil {
				timeout.Reset(handleIdleTimeout)
				select {
				case ht = <-handleQueue:
				case <-timeout.C:
					return
				}
			}
		}
		if ht == nil {
			continue
		}
		pkt := ht.p

		if pkt.flags&packetFlagReply != 0 {
			// Reply packet
			replyC, _ := c.replyTable.LoadAndDelete(pkt.sequence)
			if replyC != nil {
				var reply *replyResult
				if pkt.command == replyCodeOk {
					reply = &replyResult{pkt.getContent(), nil}
				} else {
					reply = &replyResult{nil, resolveReplyError(pkt)}
				}
				SendToChan(replyC.(replyChan), reply)
			}
		} else {
			// Request packet
			if pkt.command&commandInternalMask == 0 {
				c.busy++
				ht.handle(client, client.handlers[pkt.command])
			} else {
				ht.handleInternal(client)
			}
		}
		ht = nil
	}
}

func (c *ClientChannel) inviteNewConn() error {
	client := c.client
	if client.server == nil {
		return clientClosedErr
	}
	cip := &invitePayload{channel: c.key}
	timeout := time.NewTimer(inviteTimeout)
	defer timeout.Stop()
	channel := c.client.ensureChannel(clientChannelDefaultKey)
	var err error
	if channel == nil {
		err = clientClosedErr
	} else {
		_, err = channel.executeRequest(COMMAND_INVITE, NewContent(cip.encode()), timeout.C)
	}
	if err != nil {
		c.client.logger.Warn("invite new connection failed, channel: ", c.key, ", error: ", err.Error())
	}
	return err
}

func (c *ClientChannel) acquireReplying(timeoutC <-chan time.Time) error {
RETRY:
	if replying := c.replying; replying < clientReplyingLimit {
		if atomic.CompareAndSwapInt32(&c.replying, replying, replying+1) {
			return nil
		}
	} else {
		timer := time.NewTimer(clientReplyingAcquireInterval)
		defer timer.Stop()
		select {
		case <-timer.C:
		case <-timeoutC:
			if c.isNoAvailableConns() {
				return connectionNotAvailableErr
			}
			return requestWindowSizeLimitedErr
		case <-c.closedC:
			return whatClosedErr(c)
		}
	}
	goto RETRY
}

func evaluateChannelWriteBlocked(p *packet) int32 {
	if p.stream != nil {
		return clientChannelNewConnIfWriteBlocked
	}
	blocked := p.bodySize / 4096
	if blocked == 0 {
		return 1
	} else if blocked > clientChannelNewConnIfWriteBlocked {
		return clientChannelNewConnIfWriteBlocked
	}
	return int32(blocked)
}

func (c *ClientChannel) executeRequest(command Command, payload *Content, timeoutC <-chan time.Time) (*Content, error) {
	if c.closed != 0 {
		return nil, channelClosedErr
	}
	client := c.client

	c.busy++
	if err := c.acquireReplying(timeoutC); err != nil {
		return nil, err
	}
	defer atomic.AddInt32(&c.replying, -1)

	sequence := atomic.AddInt32(&c.nextSequence, 1)
	p := &packet{
		sequence: sequence,
		command:  command,
	}
	p.setContent(payload, client.tlsEnabled)

	replyC := c.replyChanPool.Get().(replyChan)
	c.replyTable.Store(sequence, replyC)
	defer c.replyTable.Delete(sequence)

	// Write request
	wt := &writeTask{p: p}
	select {
	case c.writeQueue <- wt:
	default:
		blocked := evaluateChannelWriteBlocked(p)
		atomic.AddInt32(&c.writeBlocked, blocked)
		if command != COMMAND_INVITE {
			c.wantNewConn()
		}
		var err error
		select {
		case c.writeQueue <- wt:
		case <-timeoutC:
			wt.cancelled = true
			if c.isNoAvailableConns() {
				err = connectionNotAvailableErr
			} else {
				err = writeTimeoutErr
			}
		case <-c.closedC:
			err = whatClosedErr(c)
		}
		atomic.AddInt32(&c.writeBlocked, -blocked)
		if err != nil {
			p.autoCloseNow()
			return nil, err
		}
	}

	// Wait reply
	select {
	case reply := <-replyC:
		c.replyChanPool.Put(replyC)
		return reply.payload, reply.err
	case <-timeoutC:
		wt.cancelled = true
		// Note: command maybe executed by remote client, but wait reply timeout
		return nil, waitReplyTimeoutErr
	case <-c.closedC:
		return nil, whatClosedErr(c)
	}
}

func (c *Client) Request(command Command, payload *Content, timeout time.Duration) (*Content, error) {
	var timeoutC <-chan time.Time
	if timeout > 0 {
		timer := time.NewTimer(timeout)
		timeoutC = timer.C
		defer timer.Stop()
	}
	channel := c.ensureChannel(ChannelOfCommand(command))
	if channel == nil {
		return nil, clientClosedErr
	}
	return channel.executeRequest(command, payload, timeoutC)
}

func (c *Client) RequestJSON(command Command, payload any, retPtr any, timeout time.Duration) error {
	var requestContent *Content
	if payload != nil {
		body, _ := MarshalJSONFunc(payload)
		requestContent = NewContent(body)
	}
	ret, err := c.Request(command, requestContent, timeout)
	if err != nil {
		return err
	}
	retBody := ret.GetBody()
	if retBody != nil && retPtr != nil {
		err = UnmarshalJSONFunc(retBody, retPtr)
		if err != nil {
			return invalidJsonReplyErr
		}
		c.bufferPool.Put(retBody)
	}
	return nil
}

func (c *Client) String() string {
	return "NttsClient/" + c.uid
}

func (c *Client) Stats() map[string]any {
	ret := map[string]any{
		"uid":        c.uid,
		"tlsEnabled": c.tlsEnabled,
	}
	if c.serverAddress != "" {
		ret["serverAddress"] = c.serverAddress
	}

	channelsStats := make([]any, 0)
	c.channelsLock.Lock()
	for _, channel := range c.channels {
		channelsStats = append(channelsStats, map[string]any{
			"key":               channel.key,
			"connections":       len(channel.conns),
			"writeableChannels": channel.writableN,
			"writeBlocked":      channel.writeBlocked,
		})
	}
	c.channelsLock.Unlock()
	ret["channels"] = channelsStats

	return ret
}
