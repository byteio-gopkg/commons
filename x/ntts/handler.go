package ntts

import (
	"byteio.org/commons/utils/timeutil"
	"encoding/json"
	"net"
	"time"
)

type HandlerKey = Command

type Handler func(requester Requester, payload *Content) (*Content, error)

type Handlers map[HandlerKey]Handler

type Requester interface {
	Client() *Client
	Uid() string
	RemoteIp() net.IP
	Context() any
	SetContext(context any)
}

// --------------------------------------------------------

func handleRequest(requester Requester, handler Handler, pkt *packet) *packet {
	var replyCode Command
	var replyContent *Content
	if handler != nil {
		ret, err := handler(requester, pkt.getContent())
		if err != nil {
			replyCode = replyCodeError
			replyContent = NewContent([]byte(err.Error()))
		} else {
			replyCode = replyCodeOk
			replyContent = ret
		}
	} else {
		replyCode = replyCodeNttsError
		replyContent = NewContent([]byte("[PROTOCOL]unacceptable command"))
	}
	reply := newReplyPacket(pkt, replyCode)
	reply.setContent(replyContent, pkt.flags&packetFlagEncrypted != 0)
	return reply
}

func (ht *handleTask) handle(client *Client, handler Handler) {
	reply := handleRequest(client, handler, ht.p)
	select {
	case ht.c.writeQueue <- &writeTask{p: reply}:
	case <-ht.c.closedC:
		reply.autoCloseNow()
	}
}

func (ht *handleTask) handleInternal(client *Client) {
	pkt := ht.p
	switch pkt.command {
	case COMMAND_ACTIVE:
		ht.handle(client, doNothingHandler)
	case COMMAND_TERMINATE:
		internalHandlerTerminate(client, ht)
	case COMMAND_INVITE:
		ht.handle(client, internalHandlerInvite)
	default:
		ht.handle(client, nil)
	}
}

func doNothingHandler(_ Requester, _ *Content) (*Content, error) {
	return nil, nil
}

func prepareTerminatePacket(c *ClientConn) *packet {
	channel := c.channel
	pkt := newPacketWithBody(channel, COMMAND_TERMINATE, nil, channel.client.tlsEnabled)
	replyC := channel.replyChanPool.Get().(replyChan)
	channel.replyTable.Store(pkt.sequence, replyC)
	go internalHandlerTerminateReply(c, replyC)
	return pkt
}

func internalHandlerTerminate(_ *Client, ht *handleTask) {
	conn := ht.c
	select {
	case conn.writeQueue <- nil:
	case <-conn.closedC:
		return
	}
	conn.readAllowed = false
	deadline := timeutil.RuntimeNano() + (terminateReplyTimeout).Nanoseconds()
	for conn.closed == 0 && timeutil.RuntimeNano() < deadline {
		if len(conn.writeQueue) == 0 && conn.channel.replying == 0 {
			break
		} else {
			time.Sleep(time.Second)
		}
	}
	if conn.closed == 0 {
		select {
		case conn.writeQueue <- &writeTask{p: newReplyPacketWithBody(ht.p, replyCodeOk, nil)}:
		case <-time.NewTimer(2 * time.Second).C:
		}
		time.Sleep(2 * time.Second)
	}
	conn.close()
}

func internalHandlerTerminateReply(c *ClientConn, replyC replyChan) {
	timeout := time.NewTimer(terminateReplyTimeout)
	defer timeout.Stop()
	select {
	case reply := <-replyC:
		if reply.err == nil {
			c.readAllowed = false
		}
	case <-timeout.C:
	case <-c.closedC:
	}
}

func internalHandlerInvite(requester Requester, payload *Content) (*Content, error) {
	client := requester.Client()
	if client.server != nil {
		return nil, unacceptableCommandErr
	}
	cip := &invitePayload{}
	if !cip.decode(payload.GetBody()) {
		return nil, packetInvalidBodyErr
	}
	channel := client.ensureChannel(cip.channel)
	if channel == nil {
		return nil, clientClosedErr
	}
	channel.wantNewConn()
	return nil, nil
}

// --------------------------------------------------------

var MarshalJSONFunc = json.Marshal
var UnmarshalJSONFunc = json.Unmarshal

func handlerParseJSON(requester Requester, payload *Content, in any) error {
	if body := payload.GetBody(); body != nil {
		err := UnmarshalJSONFunc(body, in)
		if err != nil {
			return invalidJsonPayloadErr
		}
		if client := requester.Client(); client != nil {
			client.bufferPool.Put(body)
		}
	}
	return nil
}

func WithJSONHandler1[InType any, OutType any](handler func(InType) (OutType, error)) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		var in InType
		if err := handlerParseJSON(requester, payload, &in); err != nil {
			return nil, err
		}
		ret, err := handler(in)
		if err != nil {
			return nil, err
		}
		retBytes, _ := MarshalJSONFunc(ret)
		return NewContent(retBytes), nil
	}
}

func WithJSONHandler1C[InType any, OutType any](handler func(Requester, InType) (OutType, error)) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		var in InType
		if err := handlerParseJSON(requester, payload, &in); err != nil {
			return nil, err
		}
		ret, err := handler(requester, in)
		if err != nil {
			return nil, err
		}
		retBytes, _ := MarshalJSONFunc(ret)
		return NewContent(retBytes), nil
	}
}

func WithJSONHandler2[InType any, OutType any](handler func(InType) OutType) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		var in InType
		if err := handlerParseJSON(requester, payload, &in); err != nil {
			return nil, err
		}
		ret := handler(in)
		retBytes, _ := MarshalJSONFunc(ret)
		return NewContent(retBytes), nil
	}
}

func WithJSONHandler2C[InType any, OutType any](handler func(Requester, InType) OutType) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		var in InType
		if err := handlerParseJSON(requester, payload, &in); err != nil {
			return nil, err
		}
		ret := handler(requester, in)
		retBytes, _ := MarshalJSONFunc(ret)
		return NewContent(retBytes), nil
	}
}

func WithJSONHandler3[InType any](handler func(InType) error) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		var in InType
		if err := handlerParseJSON(requester, payload, &in); err != nil {
			return nil, err
		}
		err := handler(in)
		return nil, err
	}
}

func WithJSONHandler3C[InType any](handler func(Requester, InType) error) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		var in InType
		if err := handlerParseJSON(requester, payload, &in); err != nil {
			return nil, err
		}
		err := handler(requester, in)
		return nil, err
	}
}

func WithJSONHandler4[InType any](handler func(InType)) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		var in InType
		if err := handlerParseJSON(requester, payload, &in); err != nil {
			return nil, err
		}
		handler(in)
		return nil, nil
	}
}

func WithJSONHandler4C[InType any](handler func(Requester, InType)) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		var in InType
		err := handlerParseJSON(requester, payload, &in)
		if err != nil {
			return nil, err
		}
		handler(requester, in)
		return nil, nil
	}
}

func WithJSONHandler5[OutType any](handler func() (OutType, error)) Handler {
	return func(_ Requester, payload *Content) (*Content, error) {
		ret, err := handler()
		if err != nil {
			return nil, err
		}
		retBytes, _ := MarshalJSONFunc(ret)
		return NewContent(retBytes), nil
	}
}

func WithJSONHandler5C[OutType any](handler func(Requester) (OutType, error)) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		ret, err := handler(requester)
		if err != nil {
			return nil, err
		}
		retBytes, _ := MarshalJSONFunc(ret)
		return NewContent(retBytes), nil
	}
}

func WithJSONHandler6[OutType any](handler func() OutType) Handler {
	return func(_ Requester, payload *Content) (*Content, error) {
		ret := handler()
		retBytes, _ := MarshalJSONFunc(ret)
		return NewContent(retBytes), nil
	}
}

func WithJSONHandler6C[OutType any](handler func(Requester) OutType) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		ret := handler(requester)
		retBytes, _ := MarshalJSONFunc(ret)
		return NewContent(retBytes), nil
	}
}

func WithJSONHandler7(handler func() error) Handler {
	return func(_ Requester, payload *Content) (*Content, error) {
		err := handler()
		return nil, err
	}
}

func WithJSONHandler7C(handler func(Requester) error) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		err := handler(requester)
		return nil, err
	}
}

func WithJSONHandler8(handler func()) Handler {
	return func(_ Requester, payload *Content) (*Content, error) {
		handler()
		return nil, nil
	}
}

func WithJSONHandler8C(handler func(Requester)) Handler {
	return func(requester Requester, payload *Content) (*Content, error) {
		handler(requester)
		return nil, nil
	}
}
