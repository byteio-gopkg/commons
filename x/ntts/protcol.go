package ntts

import (
	"byteio.org/commons/utils/cryptoutil"
	"byteio.org/commons/utils/ioutil"
	"byteio.org/commons/utils/mathutil"
	"byteio.org/commons/utils/randutil"
	"byteio.org/commons/utils/runtimeutil"
	"byteio.org/commons/x/buffer"
	"byteio.org/commons/x/encoding/bigendian"
	"github.com/golang/snappy"
	"io"
	"net"
	"runtime"
	"sync/atomic"
	"time"
)

//goland:noinspection GoSnakeCaseUsage
const (
	COMMAND_CONNECT   Command = 0x10000001
	COMMAND_ACTIVE    Command = 0x10000002
	COMMAND_TERMINATE Command = 0x10000006
	COMMAND_INVITE    Command = 0x10000011

	EVENT_CLIENT_CREATED Command = 0x30000001
	EVENT_CLIENT_CLOSED  Command = 0x30000002

	QUICK_UID = "NTTS-QUICK"

	commandInternalMask Command = 0x10000000
	commandChannelMask  Command = 0x0f000000

	connectTimeout           = 10 * time.Second
	connectRetryInterval     = 15 * time.Second
	connectRetryFastInterval = 2 * time.Second
	connectRetryFasts        = 50

	inviteTimeout         = 15 * time.Second
	terminateReplyTimeout = 2 * time.Minute

	packetHeadSize      = 13
	packetBodySizeLimit = 16 * 1024 * 1024

	packetFlagReply              uint8 = 0b00000001
	packetFlagCompressed         uint8 = 0b00000010
	packetFlagEncrypted          uint8 = 0b00000100
	packetFlagWithStream         uint8 = 0b00001000
	packetCompressThreshold            = 4096
	packetStreamChunkSizeLimit         = 1024 * 64
	packetStreamConsumeStagnated       = 1 * time.Minute

	cryptoNonceSize                        = 12
	cryptoNonceTimestampDifferenceMs int64 = 5 * 60 * 1000

	replyCodeOk        Command = 0
	replyCodeError     Command = 1
	replyCodeNttsError Command = 2
)

// --------------------------------------------------------

type Command = int32

type ChannelKey = int32

func CommandWithChannel(command Command, channelKey ChannelKey) Command {
	return command | ((channelKey << 24) & commandChannelMask)
}

func ChannelOfCommand(command Command) ChannelKey {
	return (command & commandChannelMask) >> 24
}

// --------------------------------------------------------

type Content struct {
	Body   []byte
	Stream io.Reader
}

func NewContent(body []byte) *Content {
	return &Content{Body: body}
}

func (c *Content) GetBody() []byte {
	if c == nil {
		return nil
	}
	return c.Body
}

func (c *Content) GetStream() io.Reader {
	if c == nil {
		return nil
	}
	return c.Stream
}

func (c *Content) Close() error {
	if c == nil {
		return nil
	}
	if stream := c.Stream; stream != nil {
		if closer, ok := stream.(io.Closer); ok {
			return closer.Close()
		}
	}
	return nil
}

type packet struct {
	sequence int32
	command  Command
	flags    uint8
	bodySize uint32
	body     []byte
	stream   io.Reader
}

func (p *packet) encodeHead(buf []byte) {
	bigendian.PutInt32(buf[0:4], p.sequence)
	bigendian.PutInt32(buf[4:8], p.command)
	buf[8] = p.flags
	bigendian.PutInt32(buf[9:13], int32(p.bodySize))
}

func (p *packet) decodeHead(buf []byte) {
	p.sequence = bigendian.Int32(buf[0:4])
	p.command = bigendian.Int32(buf[4:8])
	p.flags = buf[8]
	p.bodySize = uint32(bigendian.Int32(buf[9:13]))
}

func (p *packet) setBody(body []byte, withEncrypted bool) {
	if withEncrypted {
		p.flags |= packetFlagEncrypted
	}
	if body == nil {
		p.bodySize = 0
	} else {
		p.bodySize = uint32(len(body))
		p.body = body
		if p.bodySize > packetCompressThreshold {
			p.flags |= packetFlagCompressed
		}
	}
}

func (p *packet) getContent() *Content {
	return &Content{Body: p.body, Stream: p.stream}
}

func (p *packet) setContent(content *Content, withEncrypted bool) {
	if withEncrypted {
		p.flags |= packetFlagEncrypted
	}
	if content == nil {
		p.bodySize = 0
	} else {
		p.bodySize = uint32(len(content.Body))
		p.body = content.Body
		if p.bodySize > packetCompressThreshold {
			p.flags |= packetFlagCompressed
		}
		p.stream = content.Stream
		if p.stream != nil {
			p.flags |= packetFlagWithStream | packetFlagCompressed
		}
	}
}

func (p *packet) autoCloseNow() {
	if stream := p.stream; stream != nil {
		if closer, ok := stream.(*autoCloseReader); ok {
			_ = closer.Close()
		}
	}
}

func newPacketWithBody(channel *ClientChannel, command Command, body []byte, withEncrypted bool) *packet {
	p := &packet{
		sequence: atomic.AddInt32(&channel.nextSequence, 1),
		command:  command,
	}
	p.setBody(body, withEncrypted)
	return p
}

func newReplyPacket(p *packet, code Command) *packet {
	return &packet{
		sequence: p.sequence,
		command:  code,
		flags:    packetFlagReply,
	}
}

func newReplyPacketWithBody(p *packet, code Command, body []byte) *packet {
	reply := newReplyPacket(p, code)
	reply.setBody(body, p.flags&packetFlagEncrypted != 0)
	return reply
}

// --------------------------------------------------------

type Encoding struct {
	bufferPool *buffer.Pool
	cipher     *cryptoutil.GCMCipher
}

func newEncoding(secret string) (*Encoding, error) {
	cipher, err := cryptoutil.NewAesGCMCipher([]byte(secret))
	if err != nil {
		return nil, err
	}
	return &Encoding{
		cipher:     cipher,
		bufferPool: buffer.NewPool([]int{256, 1024, 4096, 8192}),
	}, nil
}

func (e *Encoding) encode(content []byte, flags uint8) ([]byte, bool) {
	if len(content) == 0 {
		return nil, false
	}

	bodyCanFree := false
	// Compress
	if flags&packetFlagCompressed != 0 {
		compressed := e.bufferPool.Alloc(snappy.MaxEncodedLen(len(content)))
		compressed = snappy.Encode(compressed, content)
		content = compressed
		bodyCanFree = true
	}
	// Encrypt
	if flags&packetFlagEncrypted != 0 {
		dst := e.bufferPool.Alloc(cryptoNonceSize + 16 + len(content))[0:cryptoNonceSize]
		bigendian.PutInt64(dst, time.Now().UnixMilli())
		randutil.PutAlphanumericString(dst[8:])
		encrypted := e.cipher.Encrypt(content, dst, dst)
		if bodyCanFree {
			e.bufferPool.Put(content)
		}
		content = encrypted
		bodyCanFree = true
	}
	return content, bodyCanFree
}

func (e *Encoding) decode(content []byte, flags uint8) []byte {
	if len(content) == 0 {
		return nil
	}

	// Decrypt
	if flags&packetFlagEncrypted != 0 {
		bodyLen := len(content)
		if bodyLen < cryptoNonceSize {
			return nil
		}
		nonce := content[0:cryptoNonceSize]
		unixMilli := time.Now().UnixMilli()
		if mathutil.Abs(unixMilli-bigendian.Int64(nonce[0:8])) > cryptoNonceTimestampDifferenceMs {
			return nil
		}
		decrypted := e.cipher.Decrypt(content[cryptoNonceSize:], nonce, e.bufferPool.Alloc(bodyLen)[:0])
		if decrypted == nil {
			return nil
		}
		e.bufferPool.Put(content)
		content = decrypted
	}
	// Decompress
	if flags&packetFlagCompressed != 0 {
		uncompressed, err := snappy.Decode(e.bufferPool.Alloc(len(content)), content)
		e.bufferPool.Put(content)
		if err != nil {
			return nil
		}
		content = uncompressed
	}
	return content
}

func encodePayloadString(v string) []byte {
	bs := []byte(v)
	vl := len(bs)
	bytes := make([]byte, 4+vl)
	bigendian.PutInt32(bytes, int32(vl))
	copy(bytes[4:], bs)
	return bytes
}

func decodePayloadString(bytesPtr *[]byte, out *string) bool {
	bytes := *bytesPtr
	if len(bytes) < 4 {
		return false
	}
	size := bigendian.Int32(bytes)
	end := int(size + 4)
	if size < 0 || end > len(bytes) {
		return false
	}
	*bytesPtr = bytes[end:]
	*out = string(bytes[4:end])
	return true
}

func encodePayloadBool(v bool) byte {
	if v {
		return 1
	} else {
		return 0
	}
}

type autoCloseReader struct {
	r      io.Reader
	closed int32
}

func WithAutoCloseReader(r io.Reader) io.Reader {
	if r == nil {
		return nil
	}
	if _, ok := r.(io.Closer); ok {
		cr := &autoCloseReader{
			r:      r,
			closed: 0,
		}
		runtime.SetFinalizer(cr, func(sc *autoCloseReader) {
			_ = sc.Close()
		})
		r = cr
	}
	return r
}

func (c *autoCloseReader) Read(p []byte) (n int, err error) {
	return c.r.Read(p)
}

func (c *autoCloseReader) Close() error {
	if !atomic.CompareAndSwapInt32(&c.closed, 0, 1) {
		return nil
	}
	defer runtimeutil.RecoverWithErrorPrint()
	if closer, ok := c.r.(io.Closer); ok {
		return closer.Close()
	}
	return nil
}

// --------------------------------------------------------

func readPacket(conn net.Conn, encoding *Encoding, withEncrypted bool, replyFor *packet) (*packet, error) {
	headBuf := make([]byte, packetHeadSize)
	if err := ioutil.ReadFully(conn, headBuf); err != nil {
		return nil, err
	}
	pkt := new(packet)
	pkt.decodeHead(headBuf)
	if withEncrypted != (pkt.flags&packetFlagEncrypted != 0) {
		return nil, packetInvalidEncryptedFlagErr
	}
	if replyFor != nil && (pkt.sequence != replyFor.sequence || pkt.flags&packetFlagReply == 0) {
		return nil, unexpectedReplyErr
	}
	if err := readPacketContent(conn, encoding, pkt); err != nil {
		return nil, err
	}
	return pkt, nil
}

func readPacketContent(conn net.Conn, encoding *Encoding, pkt *packet) error {
	if pkt.bodySize > 0 {
		if pkt.bodySize > packetBodySizeLimit {
			return packetBodyTooLargeErr
		}
		body := encoding.bufferPool.Alloc(int(pkt.bodySize))
		if err := ioutil.ReadFully(conn, body); err != nil {
			if err == io.EOF {
				err = io.ErrUnexpectedEOF
			}
			return err
		}
		body = encoding.decode(body, pkt.flags)
		if body == nil {
			return packetInvalidBodyErr
		}
		pkt.body = body
	}
	if pkt.flags&packetFlagWithStream != 0 {
		pkt.stream = newContentStream(conn, encoding, pkt.flags)
	}
	return nil
}

func writePacket(conn net.Conn, encoding *Encoding, pkt *packet) error {
	var bodyBytes []byte
	var bodyCanFree = false
	if pkt.bodySize > 0 {
		bodyBytes, bodyCanFree = encoding.encode(pkt.body, pkt.flags)
		pkt.bodySize = uint32(len(bodyBytes))
	}
	headBuf := make([]byte, packetHeadSize)
	pkt.encodeHead(headBuf)
	if err := ioutil.WriteFully(conn, headBuf); err != nil {
		return err
	}
	if bodyBytes != nil {
		if err := ioutil.WriteFully(conn, bodyBytes); err != nil {
			return err
		}
	}
	if bodyCanFree {
		encoding.bufferPool.Put(bodyBytes)
	}
	if pkt.stream != nil {
		var closed int32 = 0
		err := writeStream(conn, encoding, pkt, &closed)
		if err != nil {
			return err
		} else {
			pkt.autoCloseNow()
		}
	}
	return nil
}

func requestWriteAndRead(conn net.Conn, encoding *Encoding, pkt *packet) (*packet, error) {
	err := writePacket(conn, encoding, pkt)
	if err != nil {
		return nil, err
	}
	var reply *packet
	reply, err = readPacket(conn, encoding, pkt.flags&packetFlagEncrypted != 0, pkt)
	if err != nil {
		return nil, err
	}
	return reply, nil
}

// --------------------------------------------------------

type connectPayload struct {
	uid       string
	channel   ChannelKey
	token     string
	tlsEnable bool
	quick     bool
}

func (p *connectPayload) encode() []byte {
	var out []byte
	out = append(out, encodePayloadString(p.uid)...)
	out = append(out, bigendian.Int32Bytes(p.channel)...)
	out = append(out, encodePayloadString(p.token)...)
	out = append(out,
		encodePayloadBool(p.tlsEnable),
		encodePayloadBool(p.quick),
	)
	return out
}

func (p *connectPayload) decode(bytes []byte) bool {
	if !decodePayloadString(&bytes, &p.uid) {
		return false
	}
	if len(bytes) < 4 {
		return false
	}
	p.channel = bigendian.Int32(bytes)
	bytes = bytes[4:]
	if !decodePayloadString(&bytes, &p.token) {
		return false
	}
	if len(bytes) != 2 {
		return false
	}
	p.tlsEnable = bytes[0] == 1
	p.quick = bytes[1] == 1
	return true
}

func requestConnect(conn net.Conn, encoding *Encoding, connect *packet, connectToken string) error {
	var reply *packet
	var err error
	if reply, err = requestWriteAndRead(conn, encoding, connect); err != nil {
		return err
	}
	if reply.stream != nil {
		return packetCannotWithStreamErr
	}
	if reply.command != replyCodeOk {
		return unexpectedConnectReplyStatusErr
	}
	if string(reply.body) != connectToken {
		return invalidConnectReplyTokenErr
	}
	return err
}

type invitePayload struct {
	channel ChannelKey
}

func (p *invitePayload) encode() []byte {
	return bigendian.Int32Bytes(p.channel)
}

func (p *invitePayload) decode(bytes []byte) bool {
	if len(bytes) != 4 {
		return false
	}
	p.channel = bigendian.Int32(bytes)
	return true
}
