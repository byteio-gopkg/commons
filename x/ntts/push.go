package ntts

import (
	"byteio.org/commons"
	"byteio.org/commons/utils/runtimeutil"
	"byteio.org/commons/x/buffer"
	"byteio.org/commons/x/encoding/bigendian"
	"encoding/base64"
	"fmt"
	"math"
	"path/filepath"
	"sync"
	"sync/atomic"
	"time"
)

const (
	pushTimeout          = 3 * time.Minute
	pushRetryInterval    = 15 * time.Second
	pushIdleTimeout      = 5 * time.Second
	pushQueueMemoryLimit = 500000

	PushFlagWithSave PushFlags = 0b00000001
)

type PushFlags uint8

func (c *Client) initPusher() {
	c.pusherM = &Pusher{
		c:          c,
		queue:      buffer.NewMemoryQueue(pushQueueMemoryLimit),
		notEmpty:   make(chan int, 1),
		bufferPool: c.bufferPool,
		tn:         0,
		closed:     0,
	}

	if savePath := c.pushSavePath; savePath != "" {
		savePath = filepath.Join(savePath, fmt.Sprintf("%s.pq", c.uid))
		c.pusherF = &Pusher{
			c:          c,
			queue:      buffer.NewFileQueue(savePath, c.bufferPool),
			notEmpty:   make(chan int, 1),
			bufferPool: c.bufferPool,
			tn:         0,
			closed:     0,
		}
	}
}

type Pusher struct {
	c          *Client
	queue      buffer.Queue
	notEmpty   chan int
	bufferPool *buffer.Pool
	tn         int
	tl         sync.Mutex
	closed     int32
}

func (p *Pusher) submit(command Command, payload []byte, deadline int64) error {
	bufferPool := p.bufferPool
	data := bufferPool.Alloc(12 + len(payload))
	bigendian.PutInt32(data, command)
	bigendian.PutInt64(data[4:], deadline)
	if len(payload) > 0 {
		copy(data[12:], payload)
	}
	err := p.queue.Push(data)
	if err != nil {
		return err
	}
	if p.c.connN == 0 {
		return nil
	}
	select {
	case p.notEmpty <- 1:
	default:
	}
	p.startRun()
	return nil
}

func (p *Pusher) shutdown() {
	if !atomic.CompareAndSwapInt32(&p.closed, 0, 1) {
		return
	}
	_ = p.queue.Close()
	commons.SendToChan(p.notEmpty, 0)
}

func (p *Pusher) startRun() {
	p.tl.Lock()
	if p.tn < 1 {
		p.tn++
		go p.run()
	}
	p.tl.Unlock()
}

func (p *Pusher) run() {
	defer runtimeutil.RecoverWithErrorPrint()
	tnc := false
	defer func() {
		if !tnc {
			p.tl.Lock()
			p.tn--
			p.tl.Unlock()
		}
	}()

	c := p.c
	q := p.queue
	for p.closed == 0 && c.connN > 0 {
		t := q.Poll()
		if t == nil {
			idleTimeout := time.NewTimer(pushIdleTimeout)
			select {
			case <-p.notEmpty:
				idleTimeout.Stop()
				continue
			case <-idleTimeout.C:
				p.tl.Lock()
				t = q.Poll()
				if t == nil {
					p.tn--
					tnc = true
				}
				p.tl.Unlock()
			}
			if t == nil {
				break
			}
		}
		if len(t) < 12 {
			continue
		}
		p.c.executePush(bigendian.Int32(t), t[12:], bigendian.Int64(t[4:]))
		p.bufferPool.Put(t)
	}
}

// --------------------------------------------------------

func (c *Client) startPusher() {
	for _, pusher := range []*Pusher{c.pusherM, c.pusherF} {
		if pusher != nil && c.closed == 0 && !pusher.queue.IsEmpty() {
			pusher.startRun()
		}
	}
}

func (c *Client) executePush(command Command, payload []byte, deadline int64) {
	var requestContent *Content
	if payload != nil {
		requestContent = NewContent(payload)
	}
	var err error
	for {
		if c.closed != 0 {
			err = clientClosedErr
			break
		}
		timeout := pushTimeout
		if deadline > 0 {
			nowUnixMilli := time.Now().UnixMilli()
			if nowUnixMilli >= deadline {
				err = pushTimeoutErr
				break
			} else {
				timeout = time.Millisecond * time.Duration(deadline-nowUnixMilli)
			}
		}
		_, err = c.Request(command, requestContent, timeout)
		//goland:noinspection GoDirectComparisonOfErrors
		if err != nil && (err == connectionNotAvailableErr ||
			err == writeTimeoutErr ||
			err == requestWindowSizeLimitedErr ||
			err == waitReplyTimeoutErr) {
			if c.server != nil && err == connectionNotAvailableErr {
				break
			}
			time.Sleep(pushRetryInterval)
			continue
		}
		break
	}
	if err != nil {
		c.logger.Warn("push failed: ", command, "/", base64.StdEncoding.EncodeToString(payload), ", error: ", err.Error())
	}
}

func (c *Client) Push(command Command, payload []byte, timeout time.Duration, flags PushFlags) {
	var err error
	if c.closed == 0 {
		var deadline int64 = 0
		if timeout > 0 {
			deadline = time.Now().Add(timeout).UnixMilli()
		}
		if flags&PushFlagWithSave == 0 {
			err = c.pusherM.submit(command, payload, deadline)
		} else {
			if c.pusherF == nil {
				err = pushWithSaveUnavailableErr
			} else {
				err = c.pusherF.submit(command, payload, deadline)
			}
		}
	} else {
		err = clientClosedErr
	}
	if err != nil {
		c.logger.Warn("push failed: ", command, "/", base64.StdEncoding.EncodeToString(payload), ", error: ", err.Error())
	}
}

func (c *Client) PushJSON(command Command, payload any, timeout time.Duration, flags PushFlags) {
	var payloadBytes []byte
	if payload != nil {
		payloadBytes, _ = MarshalJSONFunc(payload)
	}
	c.Push(command, payloadBytes, timeout, flags)
}

// --------------------------------------------------------

func (s *Server) Push(uid string, command Command, payload []byte, timeout time.Duration, flags PushFlags) {
	c, _ := s.ensureClient(uid)
	c.Push(command, payload, timeout, flags)
}

func (s *Server) PushJSON(uid string, command Command, payload any, timeout time.Duration, flags PushFlags) {
	c, _ := s.ensureClient(uid)
	c.PushJSON(command, payload, timeout, flags)
}

// --------------------------------------------------------

type BatchPusher struct {
	c          *Client
	list       []any
	mu         sync.Mutex
	command    Command
	timeout    time.Duration
	flags      PushFlags
	batchLimit int
	interval   time.Duration
	wait       chan int
	tn         int
}

func (c *Client) NewBatchPusher(command Command, timeout time.Duration, flags PushFlags, batchLimit int, interval time.Duration) *BatchPusher {
	if batchLimit <= 0 {
		batchLimit = math.MaxInt
	}
	return &BatchPusher{
		c:          c,
		command:    command,
		timeout:    timeout,
		flags:      flags,
		batchLimit: batchLimit,
		interval:   interval,
		wait:       make(chan int, 1),
		tn:         0,
	}
}

func (p *BatchPusher) PushJSON(payload any) {
	p.mu.Lock()
	p.list = append(p.list, payload)
	if p.tn == 0 {
		p.tn = 1
		go p.run()
	}
	if len(p.list) >= p.batchLimit {
		commons.SendToChan(p.wait, 1)
	}
	p.mu.Unlock()
}

func (p *BatchPusher) run() {
	c := p.c
	idle := 0
	for {
		if len(p.list) < 1 {
			if idle > 5 {
				p.mu.Lock()
				if len(p.list) < 1 {
					p.tn = 0
					p.mu.Unlock()
					return
				}
				p.mu.Unlock()
			}

			idle++
			select {
			case <-p.wait:
			case <-time.After(p.interval):
			}
			continue
		}
		p.mu.Lock()
		list := p.list
		p.list = nil
		p.mu.Unlock()

		if len(list) > 0 {
			idle = 0
			c.PushJSON(p.command, list, p.timeout, p.flags)
		}
	}
}

func (p *BatchPusher) Flush() {
	p.mu.Lock()
	if len(p.list) > 0 {
		if p.tn == 0 {
			p.tn = 1
			go p.run()
		} else {
			commons.SendToChan(p.wait, 1)
		}
	}
	p.mu.Unlock()
}
