// Copyright 2023 TimAndy. All rights reserved.
// Licensed under the Apache-2.0 license that can be found in the LICENSE file.
// https://github.com/timandy/routine

package g

import (
	"byteio.org/commons/utils/reflectutil"
	"unsafe"
)

var goidOffset uintptr

func init() {
	gt := reflectutil.LookupType("runtime.g")
	if gt == nil {
		panic("runtime.g type not found, incompatible for current golang version")
	}
	if field, found := gt.FieldByName("goid"); found {
		goidOffset = field.Offset
	} else {
		panic("goid field of runtime.g not found, incompatible for current golang version")
	}
}

// getg returns the pointer to the current runtime.g.
//
//go:nosplit
func getg() unsafe.Pointer

// Goid get current go routine id
func Goid() int64 {
	return *(*int64)(unsafe.Pointer(uintptr(getg()) + goidOffset))
}
