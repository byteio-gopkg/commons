package routine

import (
	"byteio.org/commons/x/routine/g"
	"sync"
	"time"
)

var (
	runCtls       = new(sync.Map)
	runCtlDefault = runCtl0{}
)

type RunCtl interface {
	Runnable() bool
	Sleep(time.Duration)
	WakeUpC() <-chan struct{}
}

type runCtl0 struct{}

func (c runCtl0) Runnable() bool {
	return true
}

func (c runCtl0) Sleep(d time.Duration) {
	time.Sleep(d)
}

func (c runCtl0) WakeUpC() <-chan struct{} {
	return nil
}

func GetRunCtl() RunCtl {
	if ctl, ok := runCtls.Load(g.Goid()); ok {
		return ctl.(RunCtl)
	}
	return runCtlDefault
}

func SetRunCtl(ctl RunCtl) {
	if ctl == nil {
		return
	}
	runCtls.Store(g.Goid(), ctl)
}

func DelRunCtl() {
	runCtls.Delete(g.Goid())
}
