package app

import (
	"byteio.org/commons/utils/fileutil"
	"byteio.org/commons/utils/ioutil"
	"byteio.org/commons/utils/runtimeutil"
	"byteio.org/commons/x/cache"
	"byteio.org/commons/x/counter"
	"byteio.org/commons/x/executor"
	"byteio.org/commons/x/goo"
	"byteio.org/commons/x/logging"
	"byteio.org/commons/x/schedule"
	syncx "byteio.org/commons/x/sync"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"
)

type Context interface {
	Name() string
	Home() string
	ResourcesPath() string
	DataPath() string
	LogsPath() string
	Logger() logging.Logger
	Services() []goo.Service
	GetService(name string) goo.Service
	AddServices(services ...goo.Service)
	Executor() executor.Executor
	Cache() cache.Cache
	Counters() *counter.Table
	Locks() *syncx.Locks
}

type OnApplicationStarted interface {
	OnApplicationStarted() error
}

// --------------------------------------------------------

type ContextBase struct {
	name          string
	home          string
	resourcesPath string
	dataPath      string
	logsPath      string
	logger        logging.Logger
	services      []goo.Service
	serviceMap    map[string]goo.Service
	executor      executor.Executor
	cache         cache.Cache
	counters      *counter.Table
	locks         *syncx.Locks
	schedules     []schedule.Schedule
	dailyExecuted int
	pidFile       string
	signalC       chan os.Signal
}

func (c *ContextBase) Name() string {
	return c.name
}

func (c *ContextBase) Home() string {
	return c.home
}

func (c *ContextBase) ResourcesPath() string {
	return c.resourcesPath
}

func (c *ContextBase) DataPath() string {
	return c.dataPath
}

func (c *ContextBase) LogsPath() string {
	return c.logsPath
}

func (c *ContextBase) Logger() logging.Logger {
	return c.logger
}

func (c *ContextBase) Services() []goo.Service {
	return c.services
}

func (c *ContextBase) GetService(name string) goo.Service {
	return c.serviceMap[name]
}

func (c *ContextBase) AddServices(services ...goo.Service) {
	if services == nil {
		return
	}
	c.services = append(c.services, services...)
	for _, s := range services {
		c.serviceMap[s.Name()] = s
	}
}

func (c *ContextBase) Executor() executor.Executor {
	return c.executor
}

func (c *ContextBase) Cache() cache.Cache {
	return c.cache
}

func (c *ContextBase) Counters() *counter.Table {
	return c.counters
}

func (c *ContextBase) Locks() *syncx.Locks {
	return c.locks
}

// --------------------------------------------------------

func (c *ContextBase) Run(appName string, appVersion string,
	startup1 func(), startup2 func(),
	shutdown func(), secondClock func()) {
	if secondClock != nil {
		secondClock()
	}
	c.startup(appName, appVersion, startup1, startup2)
	c.waitExit(secondClock)
	c.shutdown(shutdown)
}

func (c *ContextBase) startup(appName string, appVersion string, startup1 func(), startup2 func()) {
	c.name = appName
	var err error
	daemonMode := os.Getenv("RUN_MODE") == "DAEMON"

	// Prepare paths and root logger
	home := fileutil.ExecPath()
	logsPath := filepath.Join(home, "logs")
	ExitIfError(os.MkdirAll(logsPath, 0766))
	rootLogFile := filepath.Join(logsPath, appName+".log")
	logger, err := logging.NewFileLogger("", rootLogFile, 0, !daemonMode)
	ExitIfError(err)
	logging.SetRoot(logger)
	c.home = home
	c.resourcesPath = filepath.Join(home, "resources")
	c.dataPath = filepath.Join(home, "data")
	ExitIfError(os.MkdirAll(c.dataPath, 0766))
	c.logsPath = logsPath
	c.logger = logger
	if appVersion == "" {
		appVersion = ExecFileVersion()
	}
	logger.Info(appName + "(v" + appVersion + ") starting...")

	// Set environment variables
	_ = os.Setenv("APP_HOME", home)
	_ = os.Setenv("APP_RESOURCES_PATH", c.resourcesPath)
	_ = os.Setenv("APP_DATA_PATH", c.dataPath)
	_ = os.Setenv("APP_LOGS_PATH", c.logsPath)

	// Startup1
	c.serviceMap = make(map[string]goo.Service)
	startup1()

	// Cache, Counters, Timers
	now := time.Now()
	c.dailyExecuted = now.Day()
	c.executor = executor.NewRoutinePoolExecutor(4)
	c.cache = cache.NewMapCache()
	c.counters = counter.NewTable()
	c.locks = syncx.NewLocks()
	c.schedules = []schedule.Schedule{
		// 1.daily schedules
		schedule.Repeating(func() {
			day := time.Now().Day()
			if c.dailyExecuted != day {
				c.dailyExecuted = day
				c.runDailyExecutions()
			}
		}, 1*time.Minute, 1*time.Minute),
		// 2.cleanUp: counters & daily schedules
		schedule.Repeating(func() {
			c.cache.CleanUp()
			c.counters.CleanUp()
		}, 5*time.Minute, 5*time.Minute),
	}

	// Initialize all service
	c.initializeServices()

	// Startup2
	if startup2 != nil {
		startup2()
	}

	// Started
	c.onStarted()

	// Write PID file
	c.pidFile, _ = c.writePidFile()
	logger.Info(appName, " started")
	return
}

func ExitIfError(err error) {
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}
}

func ExecFileVersion() string {
	execFileModTime, err := fileutil.ModTime(os.Args[0])
	if err == nil {
		return execFileModTime.Format("0601021504")
	}
	return ""
}

func (c *ContextBase) LoadConfig(name string, out any) error {
	b, err := fileutil.Read(filepath.Join(c.home, name))
	if err != nil {
		return errors.New("Load configuration file error: " + err.Error())
	}
	err = json.Unmarshal(b, out)
	if err != nil {
		return errors.New("Load configuration error: " + err.Error())
	}
	return nil
}

func (c *ContextBase) writePidFile() (string, error) {
	pf := filepath.Join(c.home, ".pid")
	file, err := os.Create(pf)
	if err != nil {
		return "", err
	}
	_, _ = file.WriteString(fmt.Sprint(os.Getpid()))
	_ = file.Close()
	return pf, nil
}

// --------------------------------------------------------

func (c *ContextBase) waitExit(secondClock func()) {
	osc := make(chan os.Signal, 1)
	c.signalC = osc
	signal.Notify(osc, os.Interrupt, os.Kill, syscall.SIGQUIT, syscall.SIGTERM)
	if secondClock == nil {
		<-osc
	} else {
		ticker := time.NewTicker(time.Second)
		runnable := true
		for runnable {
			select {
			case <-ticker.C:
				secondClock()
			case <-osc:
				runnable = false
			}
		}
		ticker.Stop()
	}
	c.signalC = nil
}

func (c *ContextBase) SignalExit() {
	ch := c.signalC
	if ch != nil {
		select {
		case ch <- syscall.SIGQUIT:
		default:
		}
	}
}

func (c *ContextBase) shutdown(shutdown func()) {
	c.logger.Info(c.name + " stopping...")
	schedule.CancelAll(c.schedules)
	c.schedules = nil
	c.executor.Shutdown()
	if shutdown != nil {
		shutdown()
	}
	if c.services != nil {
		c.DestroyServices()
	}
	c.cache.Clear()
	c.locks.Purge()
	c.counters.Clear()
	c.logger.Info(c.name + " stopped")
	logging.SetRoot(nil)
	ioutil.Close(c.logger)
	if c.pidFile != "" {
		_ = os.Remove(c.pidFile)
	}
}

func (c *ContextBase) initializeServices() {
	for _, service := range c.services {
		err := service.Initialize()
		if err != nil {
			panic(err)
		}
	}
}

func (c *ContextBase) DestroyServices() {
	services := c.services
	for i := len(services) - 1; i >= 0; i-- {
		service := services[i]
		err := service.Destroy()
		if err != nil {
			c.Logger().Error("Destroy service '", service.Name(), "' error: ", err)
		}
	}
	c.services = nil
	clear(c.serviceMap)
}

func (c *ContextBase) onStarted() {
	// 1.'OnApplicationStarted' callback
	for _, service := range c.services {
		if callback, ok := service.(OnApplicationStarted); ok {
			err := callback.OnApplicationStarted()
			if err != nil {
				panic(err)
			}
		}
	}
}

type dailyExecution interface {
	DailyExecution() error
}

func (c *ContextBase) runDailyExecutions() {
	defer runtimeutil.RecoverWithErrorPrint()
	for _, service := range c.services {
		if e, ok := service.(dailyExecution); ok {
			err := e.DailyExecution()
			if err != nil {
				c.Logger().Error("Daily execution[", service.Name(), "] error: ", err)
			} else {
				c.Logger().Info("Daily execution[", service.Name(), "] succeeded")
			}
		}
	}
}
