package counter

func Decrease[K comparable](m map[K]int, key K) {
	if count := m[key]; count > 0 {
		if count == 1 {
			delete(m, key)
		} else {
			m[key] = count - 1
		}
	}
}
