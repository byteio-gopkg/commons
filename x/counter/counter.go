package counter

import (
	"byteio.org/commons/utils/timeutil"
	"sync"
	"time"
)

type counter struct {
	n   int
	end int64
}

// --------------------------------------------------------

type Counter struct {
	counter
	duration time.Duration
	lock     sync.Mutex
}

func NewCounter(duration time.Duration) *Counter {
	return &Counter{
		duration: duration,
	}
}

func (c *Counter) Count() int {
	var count int
	now := timeutil.RuntimeNano()
	c.lock.Lock()
	if now < c.end {
		count = c.n
	}
	c.lock.Unlock()
	return count
}

func (c *Counter) Increase() (count int) {
	now := timeutil.RuntimeNano()
	c.lock.Lock()
	if now >= c.end {
		c.n = 1
		c.end = now + c.duration.Nanoseconds()
		count = 1
	} else {
		c.n++
		count = c.n
	}
	c.lock.Unlock()
	return
}

func (c *Counter) Reset() {
	c.lock.Lock()
	c.n = 0
	c.end = 0
	c.lock.Unlock()
}

// --------------------------------------------------------

type Table struct {
	counters map[string]*counter
	lock     sync.RWMutex
}

func NewTable() *Table {
	return &Table{
		counters: make(map[string]*counter),
	}
}

func (t *Table) Get(key string) (count int) {
	now := timeutil.RuntimeNano()
	t.lock.RLock()
	c := t.counters[key]
	if c != nil && now < c.end {
		count = c.n
	}
	t.lock.RUnlock()
	return
}

func (t *Table) Increase(key string, duration time.Duration) (count int) {
	now := timeutil.RuntimeNano()

	t.lock.Lock()
	c := t.counters[key]
	if c == nil {
		t.counters[key] = &counter{
			n:   1,
			end: now + duration.Nanoseconds(),
		}
		count = 1
	} else if now >= c.end {
		c.n = 1
		c.end = now + duration.Nanoseconds()
		count = 1
	} else {
		c.n++
		count = c.n
	}
	t.lock.Unlock()

	return
}

func (t *Table) Size() int {
	return len(t.counters)
}

func (t *Table) CleanUp() {
	if len(t.counters) < 1 {
		return
	}

	now := timeutil.RuntimeNano()
	t.lock.Lock()
	for key, c := range t.counters {
		if now >= c.end {
			delete(t.counters, key)
		}
	}
	t.lock.Unlock()
}

func (t *Table) Clear() {
	t.lock.Lock()
	if len(t.counters) <= 64 {
		clear(t.counters)
	} else {
		t.counters = make(map[string]*counter)
	}
	t.lock.Unlock()
}
