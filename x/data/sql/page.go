package sql

import (
	. "byteio.org/commons/goi/sql"
	"byteio.org/commons/utils/sqlutil"
	"byteio.org/commons/utils/stringutil"
	"byteio.org/commons/utils/timeutil"
	"byteio.org/commons/x/data"
	"database/sql"
	"database/sql/driver"
	"strconv"
	"strings"
	"time"
)

type PageQuery struct {
	form     *data.PaginationForm
	listSql  string
	countSql string
	orderSql string
	where    []string
	params   []any
}

func NewPageQuery(form *data.PaginationForm, listSql string, countSql string) *PageQuery {
	return &PageQuery{form: form, listSql: listSql, countSql: countSql}
}

func (q *PageQuery) AddWhere(sql string) *PageQuery {
	q.where = append(q.where, sql)
	return q
}

func (q *PageQuery) AddValue(value any) {
	q.params = append(q.params, value)
}

func (q *PageQuery) WhereAnd(sql string, value any) {
	q.where = append(q.where, sql)
	q.params = append(q.params, value)
}

func (q *PageQuery) WhereAndValues(sql string, values ...any) {
	q.where = append(q.where, sql)
	for _, v := range values {
		q.params = append(q.params, v)
	}
}

func (q *PageQuery) WhereBy(sql string, filterKey string) {
	if v, ok := q.form.Filter[filterKey]; ok {
		q.WhereAnd(sql, v)
	}
}

func (q *PageQuery) WhereAndOr(columns []string, orSql string, value any) {
	var where []string
	for _, column := range columns {
		where = append(where, column+" "+orSql)
		q.params = append(q.params, value)
	}
	if len(where) > 0 {
		q.where = append(q.where, "("+strings.Join(where, " OR ")+")")
	}
}

func (q *PageQuery) WhereEqual(column string, filterKey string) {
	if v, ok := q.form.Filter[filterKey]; ok {
		q.WhereAnd(column+" = ?", v)
	}
}

func (q *PageQuery) WhereEquals(columns ...string) {
	for _, column := range columns {
		q.WhereEqual(column, column)
	}
}

func (q *PageQuery) WhereEqualAny(columns []string, filterKey string) {
	if v, ok := q.form.Filter[filterKey]; ok {
		q.WhereAndOr(columns, "= ?", v)
	}
}

func (q *PageQuery) WhereNotEqual(column string, filterKey string) {
	if v, ok := q.form.Filter[filterKey]; ok {
		q.WhereAnd(column+" != ?", v)
	}
}

func (q *PageQuery) WhereNotEqualAny(columns []string, filterKey string) {
	if v, ok := q.form.Filter[filterKey]; ok {
		for _, column := range columns {
			q.WhereAnd(column+" != ?", v)
		}
	}
}

func (q *PageQuery) WhereLike(column string, filterKey string) {
	if v, ok := q.form.Filter[filterKey]; ok {
		tv, _ := v.(string)
		q.WhereAnd(column+" LIKE ?", "%"+tv+"%")
	}
}

func (q *PageQuery) WhereLikes(columns ...string) {
	for _, column := range columns {
		q.WhereLike(column, column)
	}
}

func (q *PageQuery) WhereLikeAny(columns []string, filterKey string) {
	if v, ok := q.form.Filter[filterKey]; ok {
		tv, _ := v.(string)
		q.WhereAndOr(columns, "LIKE ?", "%"+tv+"%")
	}
}

func (q *PageQuery) WhereIn(column string, values []any) *PageQuery {
	if values != nil {
		q.where = append(q.where, column+" IN "+sqlutil.ValuesArgsSQL(len(values)))
		q.params = append(q.params, values...)
	}
	return q
}

func (q *PageQuery) WhereNotIn(column string, values []any) *PageQuery {
	if values != nil {
		q.where = append(q.where, column+" NOT IN "+sqlutil.ValuesArgsSQL(len(values)))
		q.params = append(q.params, values...)
	}
	return q
}

func (q *PageQuery) OrderBy(sql string) {
	q.orderSql = sql
}

type QueryList func(db QueryAble, sql string, params []any) (any, int)

func (q *PageQuery) Query(db QueryAble, listFunc QueryList) *data.Pagination {
	listSql := q.listSql
	countSql := q.countSql
	if len(q.where) > 0 {
		whereSql := ` WHERE ` + strings.Join(q.where, ` AND `)
		listSql += whereSql
		countSql += whereSql
	}
	if q.orderSql != "" {
		listSql += ` ORDER BY ` + q.orderSql
	}
	form := q.form
	listSql += ` LIMIT ` + strconv.Itoa(form.Size) + ` OFFSET ` + strconv.Itoa(form.Offset)
	list, listSize := listFunc(db, listSql, q.params)
	if list == nil {
		return nil
	}
	var total = 0
	if !form.ListOnly {
		if (listSize == 0 && form.Offset == 0) || (listSize > 0 && listSize < form.Size) {
			total = form.Offset + listSize
		} else {
			var err error
			total, err = sqlutil.QueryE[int](db, countSql, q.params...)
			if err != nil {
				total = listSize
			}
		}
	}
	return data.NewPagination(form, total, list)
}

type formattedTime struct {
	time.Time
}

func (t *formattedTime) MarshalJSON() ([]byte, error) {
	return []byte("\"" + timeutil.FormatDateTime(t.Time) + "\""), nil
}

func PageMapRowHandler(name string, value driver.Value) (string, any) {
	if t, ok := value.(time.Time); ok {
		value = &formattedTime{t}
	}
	return stringutil.UnderscoreToLowerCamel(name), value
}

func queryListMap(db QueryAble, listSql string, params []any) (any, int) {
	list := sqlutil.QueryMapRows(db, PageMapRowHandler, listSql, params...)
	if list == nil {
		return nil, 0
	}
	return list, len(list)
}

func (q *PageQuery) QueryM(db QueryAble) *data.Pagination {
	return q.Query(db, queryListMap)
}

func WithQueryRows[T any](scan func(rows *sql.Rows, t *T) error) QueryList {
	return func(db QueryAble, sql string, params []any) (any, int) {
		rows := sqlutil.QueryRows(db, scan, sql, params...)
		if rows == nil {
			return nil, 0
		}
		return rows, len(rows)
	}
}
