package data

import (
	"strings"
)

type Pagination struct {
	Page    int `json:"page"`
	Size    int `json:"size"`
	Total   int `json:"total"`
	Content any `json:"content"`
}

func NewPagination(form *PaginationForm, total int, content any) *Pagination {
	return &Pagination{
		Page:    form.Page,
		Size:    form.Size,
		Total:   total,
		Content: content,
	}
}

type PaginationForm struct {
	Page       int            `json:"page"`
	Size       int            `json:"size"`
	Offset     int            `json:"offset"`
	ListOnly   bool           `json:"listOnly"`
	Filter     map[string]any `json:"filter"`
	Sort       map[string]int `json:"sort"`
	Projection string         `json:"projection"`
}

func (f *PaginationForm) PutFilter(name string, val any) {
	if f.Filter == nil {
		f.Filter = map[string]any{}
	}
	f.Filter[name] = val
}

func (f *PaginationForm) FilterEqual(name string, val any) bool {
	fv := f.Filter[name]
	return fv == nil || fv == val
}

func (f *PaginationForm) FilterLike(name string, val string) bool {
	fv0 := f.Filter[name]
	if fv0 == nil {
		return true
	}
	fv, ok := fv0.(string)
	if !ok {
		return false
	}
	return strings.Contains(val, fv)
}

var emptyMapRows = make([]map[string]any, 0)

func (f *PaginationForm) EmptyMapRows() *Pagination {
	return NewPagination(f, 0, emptyMapRows)
}
