package cache

import "reflect"

func TryAssign(to reflect.Value, cached any) bool {
	cv := reflect.ValueOf(cached)
	if cv.Kind() == to.Kind() {
		to.Set(cv)
		return true
	} else if cv.Kind() == reflect.Pointer && to.Kind() == reflect.Struct {
		cv = cv.Elem()
		if !cv.IsValid() {
			return true
		}
		if cv.Kind() == reflect.Struct {
			to.Set(cv)
			return true
		}
	}
	return false
}
