package cache

import (
	"byteio.org/commons/x/goo"
	"io"
	"runtime"
	"sync"
	"weak"
)

type Value[T any] interface {
	Get() T
}

type singleton[V any] struct {
	ref weak.Pointer[V]
	mu  sync.Mutex
	new func() V
}

func WithSingleton[V any](new func() V) Value[V] {
	return &singleton[V]{
		new: new,
	}
}

func (s *singleton[V]) Get() V {
	s.mu.Lock()
	defer s.mu.Unlock()
	vp := s.ref.Value()
	if vp == nil {
		v := s.new()
		vp = &v
		s.ref = weak.Make(vp)
		vo := (any)(v)
		if closer, ok1 := vo.(io.Closer); ok1 {
			runtime.AddCleanup(vp, callClose, closer)
		} else if releaser, ok2 := vo.(goo.Releaser); ok2 {
			runtime.AddCleanup(vp, callRelease, releaser)
		}
		return v
	} else {
		return *vp
	}
}

func callClose(closer io.Closer) {
	_ = closer.Close()
}

func callRelease(releaser goo.Releaser) {
	releaser.Release()
}
