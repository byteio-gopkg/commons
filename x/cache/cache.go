package cache

import (
	"sync"
)

type Cache interface {
	Get(key any) any
	GetWP(key any) (any, bool)
	Put(key any, value any)
	Remove(key any)
	Contains(key any) bool
	Size() int
	CleanUp()
	Clear()
}

type mapCache struct {
	c    map[any]any
	lock sync.RWMutex
}

func NewMapCache() Cache {
	return &mapCache{
		c: make(map[any]any),
	}
}

func (c *mapCache) Get(key any) (v any) {
	c.lock.RLock()
	v = c.c[key]
	c.lock.RUnlock()
	return
}

func (c *mapCache) GetWP(key any) (v any, present bool) {
	c.lock.RLock()
	v, present = c.c[key]
	c.lock.RUnlock()
	return
}

func (c *mapCache) Put(key any, value any) {
	c.lock.Lock()
	c.c[key] = value
	c.lock.Unlock()
}

func (c *mapCache) Remove(key any) {
	c.lock.Lock()
	delete(c.c, key)
	c.lock.Unlock()
}

func (c *mapCache) Contains(key any) bool {
	c.lock.RLock()
	_, present := c.c[key]
	c.lock.RUnlock()
	return present
}

func (c *mapCache) Size() int {
	return len(c.c)
}

func (c *mapCache) CleanUp() {
	if len(c.c) < 1 {
		return
	}
	c.lock.Lock()
	if len(c.c) > 64 {
		c.c = make(map[any]any)
	} else {
		clear(c.c)
	}
	c.lock.Unlock()
}

func (c *mapCache) Clear() {
	c.CleanUp()
}
