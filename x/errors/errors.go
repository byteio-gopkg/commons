package errors

import (
	"errors"
	"time"
)

// New just link to errors.New
func New(text string) error {
	return errors.New(text)
}

// Is just link to errors.Is
func Is(err, target error) bool {
	return errors.Is(err, target)
}

// As just link to errors.As
//
//goland:noinspection GoErrorsAs
func As(err error, target any) bool {
	return errors.As(err, target)
}

// ----------------------------------------------

type IllegalArgumentError struct {
	message string
}

func NewIllegalArgumentError(message string) error {
	return &IllegalArgumentError{message}
}

func (e *IllegalArgumentError) Error() string {
	return e.message
}

func IsIllegalArgumentError(err error) bool {
	_, is := err.(*IllegalArgumentError)
	return is
}

// ----------------------------------------------

type FatalError struct {
	Err     error
	Message string
}

func ToFatalError(err error) error {
	return &FatalError{Err: err}
}

func ToFatalErrorWithMessage(err error, message string) error {
	return &FatalError{Err: err, Message: message}
}

func NewFatalError(message string) error {
	return &FatalError{Message: message}
}

func (e *FatalError) Error() string {
	if e.Err == nil {
		return "[Fatal]" + e.Message
	} else {
		if e.Message != "" {
			return "[Fatal]" + e.Message + "|" + e.Err.Error()
		} else {
			return "[Fatal]" + e.Err.Error()
		}
	}
}

func IsFatalError(err error) bool {
	_, is := err.(*FatalError)
	return is
}

// ----------------------------------------------

type RetryableError struct {
	Err  error
	Wait time.Duration
}

func ToRetryableError(err error, wait time.Duration) error {
	return &RetryableError{err, wait}
}

func (e *RetryableError) Error() string {
	return e.Err.Error()
}

func IsRetryableError(err error) bool {
	_, is := err.(*RetryableError)
	return is
}

// ----------------------------------------------

type SuspendingError struct {
	Duration time.Duration
	Cause    string
}

func NewSuspendingError(duration time.Duration, cause string) error {
	return &SuspendingError{
		Duration: duration,
		Cause:    cause,
	}
}

func (e *SuspendingError) Error() string {
	return e.Cause
}

func IsSuspendingError(err error) bool {
	_, is := err.(*SuspendingError)
	return is
}

// ----------------------------------------------

type NoMoreElementsError struct {
	message string
}

func NewNoMoreElementsError(message string) error {
	return &NoMoreElementsError{message}
}

func (e *NoMoreElementsError) Error() string {
	return e.message
}

func IsNoMoreElementsError(err error) bool {
	_, is := err.(*NoMoreElementsError)
	return is
}
