package null

type Value interface {
	Get() any
	Set(any)
	IsNil() bool
}

// --------------------------------------------------------

type String struct {
	Value   string
	NotNull bool
}

func StringOf(value string) String {
	return String{
		Value:   value,
		NotNull: true,
	}
}

func (v *String) Get() any {
	if v.NotNull {
		return v.Value
	}
	return nil
}

func (v *String) Set(v0 any) {
	v.NotNull = v0 != nil
	if v.NotNull {
		v.Value = v0.(string)
	}
}

func (v *String) IsNil() bool {
	return !v.NotNull
}

// --------------------------------------------------------

type Int struct {
	Value   int
	NotNull bool
}

func IntOf(value int) Int {
	return Int{
		Value:   value,
		NotNull: true,
	}
}

func (v *Int) Get() any {
	if v.NotNull {
		return v.Value
	}
	return nil
}

func (v *Int) Set(v0 any) {
	v.NotNull = v0 != nil
	if v.NotNull {
		v.Value = v0.(int)
	}
}

func (v *Int) IsNil() bool {
	return !v.NotNull
}

// --------------------------------------------------------

type Int8 struct {
	Value   int8
	NotNull bool
}

func Int8Of(value int8) Int8 {
	return Int8{
		Value:   value,
		NotNull: true,
	}
}

func (v *Int8) Get() any {
	if v.NotNull {
		return v.Value
	}
	return nil
}

func (v *Int8) Set(v0 any) {
	v.NotNull = v0 != nil
	if v.NotNull {
		v.Value = v0.(int8)
	}
}

func (v *Int8) IsNil() bool {
	return !v.NotNull
}

// --------------------------------------------------------

type Int32 struct {
	Value   int32
	NotNull bool
}

func Int32Of(value int32) Int32 {
	return Int32{
		Value:   value,
		NotNull: true,
	}
}

func (v *Int32) Get() any {
	if v.NotNull {
		return v.Value
	}
	return nil
}

func (v *Int32) Set(v0 any) {
	v.NotNull = v0 != nil
	if v.NotNull {
		v.Value = v0.(int32)
	}
}

func (v *Int32) IsNil() bool {
	return !v.NotNull
}

// --------------------------------------------------------

type Int64 struct {
	Value   int64
	NotNull bool
}

func Int64Of(value int64) Int64 {
	return Int64{
		Value:   value,
		NotNull: true,
	}
}

func (v *Int64) Get() any {
	if v.NotNull {
		return v.Value
	}
	return nil
}

func (v *Int64) Set(v0 any) {
	v.NotNull = v0 != nil
	if v.NotNull {
		v.Value = v0.(int64)
	}
}

func (v *Int64) IsNil() bool {
	return !v.NotNull
}

// --------------------------------------------------------

type Float32 struct {
	Value   float32
	NotNull bool
}

func Float32Of(v float32) Float32 {
	return Float32{
		Value:   v,
		NotNull: true,
	}
}

func (v *Float32) Get() any {
	if v.NotNull {
		return v.Value
	}
	return nil
}

func (v *Float32) Set(v0 any) {
	v.NotNull = v0 != nil
	if v.NotNull {
		v.Value = v0.(float32)
	}
}

func (v *Float32) IsNil() bool {
	return !v.NotNull
}

// --------------------------------------------------------

type Float64 struct {
	Value   float64
	NotNull bool
}

func Float64Of(v float64) Float64 {
	return Float64{
		Value:   v,
		NotNull: true,
	}
}

func (v *Float64) Get() any {
	if v.NotNull {
		return v.Value
	}
	return nil
}

func (v *Float64) Set(v0 any) {
	v.NotNull = v0 != nil
	if v.NotNull {
		v.Value = v0.(float64)
	}
}

func (v *Float64) IsNil() bool {
	return !v.NotNull
}

// --------------------------------------------------------

type Bool struct {
	Value   bool
	NotNull bool
}

func BoolOf(v bool) Bool {
	return Bool{
		Value:   v,
		NotNull: true,
	}
}

func (v *Bool) Get() any {
	if v.NotNull {
		return v.Value
	}
	return nil
}

func (v *Bool) Set(v0 any) {
	v.NotNull = v0 != nil
	if v.NotNull {
		v.Value = v0.(bool)
	}
}

func (v *Bool) IsNil() bool {
	return !v.NotNull
}
