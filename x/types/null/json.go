package null

import (
	"errors"
	"fmt"
	"strconv"
)

func isNotNull(b []byte) bool {
	return !(len(b) == 4 &&
		b[0] == 'n' &&
		b[1] == 'u' &&
		b[2] == 'l' &&
		b[3] == 'l')
}

var jsonNull = []byte("null")

// --------------------------------------------------------

func (v *String) MarshalJSON() ([]byte, error) {
	if v.NotNull {
		return []byte(`"` + v.Value + `"`), nil
	} else {
		return jsonNull, nil
	}
}

func (v *String) UnmarshalJSON(b []byte) error {
	v.NotNull = isNotNull(b)
	if v.NotNull {
		v.Value = string(b[1 : len(b)-1])
	}
	return nil
}

func (v *Int) MarshalJSON() ([]byte, error) {
	if v.NotNull {
		return []byte(strconv.Itoa(v.Value)), nil
	} else {
		return jsonNull, nil
	}
}

func (v *Int) UnmarshalJSON(b []byte) error {
	if isNotNull(b) {
		v1, err := strconv.Atoi(string(b))
		if err != nil {
			return err
		}
		v.Value = v1
		v.NotNull = true
	} else {
		v.NotNull = false
	}
	return nil
}

func (v *Int8) MarshalJSON() ([]byte, error) {
	if v.NotNull {
		return []byte(strconv.Itoa(int(v.Value))), nil
	} else {
		return jsonNull, nil
	}
}

func (v *Int8) UnmarshalJSON(b []byte) error {
	if isNotNull(b) {
		v1, err := strconv.Atoi(string(b))
		if err != nil {
			return err
		}
		v.Value = int8(v1)
		v.NotNull = true
	} else {
		v.NotNull = false
	}
	return nil
}

func (v *Int32) MarshalJSON() ([]byte, error) {
	if v.NotNull {
		return []byte(strconv.Itoa(int(v.Value))), nil
	} else {
		return jsonNull, nil
	}
}

func (v *Int32) UnmarshalJSON(b []byte) error {
	if isNotNull(b) {
		v1, err := strconv.Atoi(string(b))
		if err != nil {
			return err
		}
		v.Value = int32(v1)
		v.NotNull = true
	} else {
		v.NotNull = false
	}
	return nil
}

func (v *Int64) MarshalJSON() ([]byte, error) {
	if v.NotNull {
		return []byte(fmt.Sprint(v.Value)), nil
	} else {
		return jsonNull, nil
	}
}

func (v *Int64) UnmarshalJSON(b []byte) error {
	if isNotNull(b) {
		v1, err := strconv.ParseInt(string(b), 10, 64)
		if err != nil {
			return err
		}
		v.Value = v1
		v.NotNull = true
	} else {
		v.NotNull = false
	}
	return nil
}

func (v *Float32) MarshalJSON() ([]byte, error) {
	if v.NotNull {
		return []byte(strconv.FormatFloat(float64(v.Value), 'f', -1, 32)), nil
	} else {
		return jsonNull, nil
	}
}

func (v *Float32) UnmarshalJSON(b []byte) error {
	if isNotNull(b) {
		v1, err := strconv.ParseFloat(string(b), 32)
		if err != nil {
			return err
		}
		v.Value = float32(v1)
		v.NotNull = true
	} else {
		v.NotNull = false
	}
	return nil
}

func (v *Float64) MarshalJSON() ([]byte, error) {
	if v.NotNull {
		return []byte(strconv.FormatFloat(v.Value, 'f', -1, 64)), nil
	} else {
		return jsonNull, nil
	}
}

func (v *Float64) UnmarshalJSON(b []byte) error {
	if isNotNull(b) {
		v1, err := strconv.ParseFloat(string(b), 64)
		if err != nil {
			return err
		}
		v.Value = v1
		v.NotNull = true
	} else {
		v.NotNull = false
	}
	return nil
}

func (v *Bool) MarshalJSON() ([]byte, error) {
	if v.NotNull {
		var t string
		if v.Value {
			t = "true"
		} else {
			t = "false"
		}
		return []byte(t), nil
	} else {
		return jsonNull, nil
	}
}

func (v *Bool) UnmarshalJSON(b []byte) error {
	if isNotNull(b) {
		t := string(b)
		if t == "true" {
			v.Value = true
		} else if t == "false" {
			v.Value = false
		} else {
			return errors.New("invalid bool string '" + t + "'")
		}
		v.NotNull = true
	} else {
		v.NotNull = false
	}
	return nil
}
