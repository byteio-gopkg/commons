package null

import (
	"fmt"
	"strconv"
)

func newScanTypeError[T any](found any, want T) error {
	return fmt.Errorf("unsupported Scan, storing driver.Value type %T into type %T", found, want)
}

func scanString(v0 any) string {
	switch v := v0.(type) {
	case string:
		return v
	case []byte:
		return string(v)
	}
	return ""
}

func (v *String) Scan(value any) error {
	if value == nil {
		v.Value, v.NotNull = "", false
		return nil
	}
	switch s := value.(type) {
	case string:
		v.Value = s
	case []byte:
		v.Value = string(s)
	default:
		return newScanTypeError(value, v.Value)
	}
	v.NotNull = true
	return nil
}

func (v *Int) Scan(value any) error {
	if value == nil {
		v.Value, v.NotNull = 0, false
		return nil
	}
	switch s := value.(type) {
	case int:
		v.Value = s
	case int32:
		v.Value = int(s)
	case int64:
		v.Value = int(s)
	case string, []byte:
		vn, pe := strconv.Atoi(scanString(value))
		if pe != nil {
			return newScanTypeError(value, v.Value)
		}
		v.Value = vn
	default:
		return newScanTypeError(value, v.Value)
	}
	v.NotNull = true
	return nil
}

func (v *Int8) Scan(value any) error {
	if value == nil {
		v.Value, v.NotNull = 0, false
		return nil
	}
	switch s := value.(type) {
	case int8:
		v.Value = s
	case int:
		v.Value = int8(s)
	case int32:
		v.Value = int8(s)
	case int64:
		v.Value = int8(s)
	case string, []byte:
		vn, pe := strconv.ParseInt(scanString(value), 10, 8)
		if pe != nil {
			return newScanTypeError(value, v.Value)
		}
		v.Value = int8(vn)
	default:
		return newScanTypeError(value, v.Value)
	}
	v.NotNull = true
	return nil
}

func (v *Int32) Scan(value any) error {
	if value == nil {
		v.Value, v.NotNull = 0, false
		return nil
	}
	switch s := value.(type) {
	case int32:
		v.Value = s
	case int:
		v.Value = int32(s)
	case int64:
		v.Value = int32(s)
	case string, []byte:
		vn, pe := strconv.ParseInt(scanString(value), 10, 32)
		if pe != nil {
			return newScanTypeError(value, v.Value)
		}
		v.Value = int32(vn)
	default:
		return newScanTypeError(value, v.Value)
	}
	v.NotNull = true
	return nil
}

func (v *Int64) Scan(value any) error {
	if value == nil {
		v.Value, v.NotNull = 0, false
		return nil
	}
	switch s := value.(type) {
	case int64:
		v.Value = s
	case int:
		v.Value = int64(s)
	case int32:
		v.Value = int64(s)
	case string, []byte:
		vn, pe := strconv.ParseInt(scanString(value), 10, 64)
		if pe != nil {
			return newScanTypeError(value, v.Value)
		}
		v.Value = vn
	default:
		return newScanTypeError(value, v.Value)
	}
	v.NotNull = true
	return nil
}

func (v *Float32) Scan(value any) error {
	if value == nil {
		v.Value, v.NotNull = 0, false
		return nil
	}
	switch s := value.(type) {
	case float32:
		v.Value = s
	case float64:
		v.Value = float32(s)
	case string, []byte:
		vn, pe := strconv.ParseFloat(scanString(value), 32)
		if pe != nil {
			return newScanTypeError(value, v.Value)
		}
		v.Value = float32(vn)
	default:
		return newScanTypeError(value, v.Value)
	}
	v.NotNull = true
	return nil
}

func (v *Float64) Scan(value any) error {
	if value == nil {
		v.Value, v.NotNull = 0, false
		return nil
	}
	switch s := value.(type) {
	case float64:
		v.Value = s
	case float32:
		v.Value = float64(s)
	case string, []byte:
		vn, pe := strconv.ParseFloat(scanString(value), 64)
		if pe != nil {
			return newScanTypeError(value, v.Value)
		}
		v.Value = vn
	default:
		return newScanTypeError(value, v.Value)
	}
	v.NotNull = true
	return nil
}

func (v *Bool) Scan(value any) error {
	if value == nil {
		v.Value, v.NotNull = false, false
		return nil
	}
	switch s := value.(type) {
	case bool:
		v.Value = s
	case int8:
		v.Value = s > 0
	default:
		return newScanTypeError(value, v.Value)
	}
	v.NotNull = true
	return nil
}
