package logging

var root Logger = nil

func Root() Logger {
	return root
}

func SetRoot(l Logger) {
	root = l
}

func RootDebug(content ...any) {
	if l := root; l != nil {
		root.Debug(content...)
	}
}

func RootInfo(content ...any) {
	if l := root; l != nil {
		root.Info(content...)
	}
}

func RootWarn(content ...any) {
	if l := root; l != nil {
		root.Warn(content...)
	}
}
