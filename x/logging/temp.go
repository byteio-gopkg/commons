package logging

import (
	"byteio.org/commons/x/goo"
	"fmt"
)

type tempLogger struct {
	l       goo.Logger
	content string
}

func NewTempLogger(logger goo.Logger, content string) goo.Logger {
	return &tempLogger{logger, content}
}

func (l *tempLogger) Debug(content ...any) {
	l.l.Debug(l.content + fmt.Sprint(content...))
}

func (l *tempLogger) Info(content ...any) {
	l.l.Info(l.content + fmt.Sprint(content...))
}

func (l *tempLogger) Warn(content ...any) {
	l.l.Warn(l.content + fmt.Sprint(content...))
}

func (l *tempLogger) Error(content ...any) {
	l.l.Error(l.content + fmt.Sprint(content...))
}
