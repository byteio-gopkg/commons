package logging

import (
	"bufio"
	"byteio.org/commons/utils/timeutil"
	"byteio.org/commons/x/goo"
	"fmt"
	"io"
	"os"
	"sync"
)

type Logger interface {
	goo.Logger
	Subject() string
	NewChild(subject string) Logger
	Writer() io.StringWriter
	Flush() error
	io.Closer
}

type logger struct {
	subject     string
	hasSubject  bool
	childLogger bool
	stdout      bool
	writer      io.StringWriter
	lock        *sync.Mutex
}

func NewLogger(subject string, writer io.StringWriter, stdout bool) Logger {
	return &logger{
		subject:     subject,
		hasSubject:  subject != "",
		childLogger: false,
		stdout:      stdout,
		writer:      writer,
		lock:        new(sync.Mutex),
	}
}

func NewStdLogger(subject string) Logger {
	return NewLogger(subject, nil, true)
}

func NewFileLogger(subject string, filepath string, buffer uint, stdout bool) (Logger, error) {
	f, err := os.OpenFile(filepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModeAppend|os.ModePerm)
	if err != nil {
		return nil, err
	}
	var writer io.StringWriter = nil
	if buffer > 0 {
		writer = bufio.NewWriterSize(f, int(buffer))
	} else {
		writer = f
	}
	return NewLogger(subject, writer, stdout), nil
}

func (l *logger) NewChild(subject string) Logger {
	l1 := *l
	l1.subject = subject
	l1.hasSubject = subject != ""
	l1.childLogger = true
	return &l1
}

func (l *logger) Subject() string {
	return l.subject
}

func (l *logger) doLog(level string, content []any) {
	contentTotal := len(content) + 4
	if l.hasSubject {
		contentTotal += 2
	}
	log := make([]any, contentTotal)
	log[0] = timeutil.NowDateTimeMS()
	log[1] = level
	log[2] = ": "
	from := 3
	if l.hasSubject {
		log[3] = l.subject
		log[4] = " "
		from = 5
	}
	for _, v := range content {
		log[from] = v
		from++
	}
	log[from] = "\n"
	logText := fmt.Sprint(log...)

	writer := l.writer
	if writer != nil {
		l.lock.Lock()
		_, _ = writer.WriteString(logText)
		l.lock.Unlock()
	}
	if l.stdout {
		_, _ = os.Stdout.WriteString(logText)
	}
}

func (l *logger) Debug(content ...any) {
	l.doLog(" [DEBUG]", content)
}

func (l *logger) Info(content ...any) {
	l.doLog(" [INFO]", content)
}

func (l *logger) Warn(content ...any) {
	l.doLog(" [WARN]", content)
}

func (l *logger) Error(content ...any) {
	l.doLog(" [ERROR]", content)
}

func (l *logger) Writer() io.StringWriter {
	return l.writer
}

func (l *logger) flush() error {
	if w, ok := l.writer.(interface {
		Flush() error
	}); ok {
		return w.Flush()
	}
	return nil
}

func (l *logger) Flush() error {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.flush()
}

func (l *logger) Close() error {
	if l.childLogger {
		return nil
	}

	l.lock.Lock()
	defer l.lock.Unlock()

	// Close writer
	if l.writer != nil {
		_ = l.flush()
		if w, ok := l.writer.(io.Closer); ok {
			return w.Close()
		}
		l.writer = nil
	}
	return nil
}
