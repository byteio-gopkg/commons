package sync

import (
	"byteio.org/commons"
	"byteio.org/commons/utils/timeutil"
	"sync"
	"time"
)

type Locks struct {
	locks  map[string]*locksLock
	mu     sync.Mutex
	lkp    sync.Pool
	purgeC chan struct{}
}

type locksLock struct {
	locked  bool
	locking uint32
	ch      chan struct{}
}

func NewLocks() *Locks {
	return &Locks{
		locks: make(map[string]*locksLock),
		lkp: sync.Pool{
			New: func() any {
				return &locksLock{
					locked:  false,
					locking: 0,
					ch:      make(chan struct{}, 1),
				}
			},
		},
		purgeC: make(chan struct{}),
	}
}

func (ls *Locks) Size() int {
	return len(ls.locks)
}

func (ls *Locks) releaseLock(l *locksLock, key string) {
	l.locking = 0
	delete(ls.locks, key)
	if len(l.ch) == 0 {
		ls.lkp.Put(l)
	}
}

func (ls *Locks) Lock(key string, timeout time.Duration) bool {
	var deadline int64 = 0
	if timeout > 0 {
		deadline = timeutil.RuntimeNano() + timeout.Nanoseconds()
	}
	for {
		locked := false
		ls.mu.Lock()
		lock := ls.locks[key]
		if lock == nil {
			lock = ls.lkp.Get().(*locksLock)
			ls.locks[key] = lock
		}
		lock.locking++
		if !lock.locked {
			lock.locked = true
			locked = true
		}
		ls.mu.Unlock()
		if locked {
			return true
		}
		stopNext := false
		if deadline > 0 {
			wait := deadline - timeutil.RuntimeNano()
			if wait > 0 {
				timeoutC := time.NewTimer(timeout)
				select {
				case <-lock.ch:
				case <-timeoutC.C:
				case <-ls.purgeC:
					stopNext = true
				}
				timeoutC.Stop()
			} else {
				stopNext = true
			}
		} else {
			select {
			case <-lock.ch:
			case <-ls.purgeC:
				stopNext = true
			}
		}
		ls.mu.Lock()
		lock.locking--
		if stopNext && lock.locking < 1 && lock.locked == false {
			ls.releaseLock(lock, key)
		}
		ls.mu.Unlock()
		if stopNext {
			return false
		}
	}
}

func (ls *Locks) Unlock(key string) {
	ls.mu.Lock()
	lock := ls.locks[key]
	if lock != nil {
		lock.locked = false
		if lock.locking > 1 {
			lock.locking--
		} else {
			ls.releaseLock(lock, key)
			lock = nil
		}
	}
	ls.mu.Unlock()
	if lock != nil {
		commons.SendToChan(lock.ch, empty)
	}
}

func (ls *Locks) Purge() {
	var purgeC chan struct{}
	ls.mu.Lock()
	if len(ls.locks) > 0 {
		clear(ls.locks)
		purgeC = ls.purgeC
		ls.purgeC = make(chan struct{})
	}
	ls.mu.Unlock()
	if purgeC != nil {
		close(purgeC)
	}
}
