package sync

import (
	"byteio.org/commons"
	"sync"
	"time"
)

var empty = struct{}{}

type Signal struct {
	w chan struct{}
	l sync.Mutex
}

func NewSignal() *Signal {
	return &Signal{
		w: make(chan struct{}),
	}
}

func (s *Signal) WaitC() <-chan struct{} {
	return s.w
}

func (s *Signal) Wait(cond func() bool, timeout time.Duration) bool {
	if cond() {
		return true
	}
	if timeout > 0 {
		timer := time.NewTimer(timeout)
		for !cond() {
			select {
			case <-s.w:
			case <-timer.C:
				return cond()
			}
		}
		timer.Stop()
	} else {
		for !cond() {
			<-s.w
		}
	}
	return true
}

func (s *Signal) Notify() {
	s.l.Lock()
	commons.SendToChan(s.w, empty)
	s.l.Unlock()
}

func (s *Signal) NotifyAll() {
	s.l.Lock()
	w := s.w
	s.w = make(chan struct{})
	s.l.Unlock()
	close(w)
}
