package schedule

import (
	"byteio.org/commons/utils/runtimeutil"
	"sync"
	"time"
)

type Delay struct {
	delay   time.Duration
	handler func()
	trigger *time.Timer
	lock    sync.Mutex
}

func NewDelay(delay time.Duration, handler func()) *Delay {
	return &Delay{
		delay:   delay,
		handler: handler,
	}
}

func (o *Delay) Trigger() {
	o.lock.Lock()
	if o.trigger == nil {
		o.trigger = time.AfterFunc(o.delay, o.run)
	}
	o.lock.Unlock()
}

func (o *Delay) run() {
	o.lock.Lock()
	o.trigger = nil
	o.lock.Unlock()

	defer runtimeutil.RecoverWithErrorPrint()
	o.handler()
}

func (o *Delay) ExecuteNow() {
	o.lock.Lock()
	if trigger := o.trigger; trigger != nil {
		trigger.Reset(0)
	} else {
		o.trigger = time.AfterFunc(0, o.run)
	}
	o.lock.Unlock()
}

func (o *Delay) Cancel() {
	o.lock.Lock()
	if trigger := o.trigger; trigger != nil {
		trigger.Stop()
		o.trigger = nil
	}
	o.lock.Unlock()
}

// --------------------------------------------------------

type Once[T comparable] struct {
	list    map[T]struct{}
	period  time.Duration
	handler func(T)
	trigger *time.Timer
	lock    sync.Mutex
}

func NewOnce[T comparable](period time.Duration, handler func(T)) *Once[T] {
	return &Once[T]{
		list:    make(map[T]struct{}),
		period:  period,
		handler: handler,
	}
}

func (o *Once[T]) Len() int {
	return len(o.list)
}

func (o *Once[T]) Trigger(t T) {
	o.lock.Lock()
	o.list[t] = struct{}{}
	if o.trigger == nil {
		o.trigger = time.AfterFunc(o.period, o.run)
	}
	o.lock.Unlock()
}

func (o *Once[T]) run() {
	var list map[T]struct{}
	o.lock.Lock()
	o.trigger = nil
	if len(o.list) > 0 {
		list = o.list
		o.list = make(map[T]struct{})
	}
	o.lock.Unlock()

	defer runtimeutil.RecoverWithErrorPrint()
	for t := range list {
		o.handler(t)
	}
}

func (o *Once[T]) ExecuteNow() {
	o.lock.Lock()
	if len(o.list) > 0 {
		if trigger := o.trigger; trigger != nil {
			trigger.Reset(0)
		} else {
			o.trigger = time.AfterFunc(0, o.run)
		}
	}
	o.lock.Unlock()
}

func (o *Once[T]) Cancel() {
	o.lock.Lock()
	clear(o.list)
	if trigger := o.trigger; trigger != nil {
		trigger.Stop()
		o.trigger = nil
	}
	o.lock.Unlock()
}

// --------------------------------------------------------

type Next struct {
	handler func()
	limit   time.Duration
	next    int64
	trigger *time.Timer
	lock    sync.Mutex
}

func NewNext(handler func(), limit time.Duration) *Next {
	return &Next{
		handler: handler,
		limit:   limit,
		next:    0,
	}
}

func (n *Next) Next(when time.Time) {
	if n == nil || when.IsZero() {
		return
	}
	whenTm := when.UnixMilli()
	n.lock.Lock()
	if whenTm < n.next || n.trigger == nil {
		now := time.Now()
		delay := when.Sub(now)
		if delay < 0 {
			delay = 0
			whenTm = now.UnixMilli()
		}
		if n.limit == 0 || delay <= n.limit {
			n.next = whenTm
			if t := n.trigger; t != nil {
				t.Reset(delay)
			} else {
				n.trigger = time.AfterFunc(delay, n.run)
			}
		}
	}
	n.lock.Unlock()
}

func (n *Next) WhenNext() time.Time {
	if n == nil {
		return time.Time{}
	}
	var nextTm int64 = 0
	n.lock.Lock()
	if n.trigger != nil {
		nextTm = n.next
	}
	n.lock.Unlock()

	if nextTm == 0 {
		return time.Time{}
	} else {
		return time.UnixMilli(nextTm)
	}
}

func (n *Next) run() {
	n.lock.Lock()
	n.next = 0
	n.trigger = nil
	n.lock.Unlock()

	defer runtimeutil.RecoverWithErrorPrint()
	n.handler()
}

func (n *Next) ExecuteNow() {
	if n == nil {
		return
	}
	now := time.Now().UnixMilli()
	n.lock.Lock()
	n.next = now
	if trigger := n.trigger; trigger != nil {
		trigger.Reset(0)
	} else {
		n.trigger = time.AfterFunc(0, n.run)
	}
	n.lock.Unlock()
}

func (n *Next) Cancel() {
	if n == nil {
		return
	}
	n.lock.Lock()
	n.next = 0
	if t := n.trigger; t != nil {
		t.Stop()
		n.trigger = nil
	}
	n.lock.Unlock()
}
