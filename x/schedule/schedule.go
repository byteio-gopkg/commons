package schedule

import (
	"byteio.org/commons/utils/runtimeutil"
	"sync/atomic"
	"time"
)

type Schedule interface {
	ExecuteNow()
	Cancel()
}

func CancelAll(schedules []Schedule) {
	for _, schedule := range schedules {
		if schedule != nil {
			schedule.Cancel()
		}
	}
}

type scheduleBase struct {
	timer  *time.Timer
	target func()
}

func (s *scheduleBase) Cancel() {
	timer := s.timer
	if timer != nil {
		timer.Stop()
		s.timer = nil
	}
}

// --------------------------------------------------------

func Repeating(target func(), interval time.Duration, delay time.Duration) Schedule {
	if interval <= time.Second {
		schedule := &intervalSchedule2{
			scheduleBase: scheduleBase{
				timer:  time.NewTimer(delay),
				target: target,
			},
			interval: interval,
			runnable: true,
		}
		go schedule.run()
		return schedule
	} else {
		schedule := &intervalSchedule1{
			scheduleBase: scheduleBase{
				target: target,
			},
			interval: interval,
		}
		schedule.timer = time.AfterFunc(delay, schedule.run)
		return schedule
	}
}

type intervalSchedule1 struct {
	scheduleBase
	interval   time.Duration
	executeNow int32
}

func (s *intervalSchedule1) run() {
	defer runtimeutil.RecoverWithErrorPrint()
	for {
		enb := s.executeNow
		s.target()
		if timer := s.timer; timer != nil {
			if atomic.CompareAndSwapInt32(&s.executeNow, enb, 0) {
				timer.Reset(s.interval)
			} else {
				continue
			}
		}
		break
	}
}

func (s *intervalSchedule1) ExecuteNow() {
	timer := s.timer
	if timer != nil && atomic.CompareAndSwapInt32(&s.executeNow, 0, 1) {
		timer.Reset(0)
	}
}

type intervalSchedule2 struct {
	scheduleBase
	interval   time.Duration
	executeNow int32
	runnable   bool
}

func (s *intervalSchedule2) run() {
	defer runtimeutil.RecoverWithErrorPrint()
	var enb int32
	for s.runnable {
		enb = s.executeNow
		s.target()
		if timer := s.timer; timer != nil {
			if atomic.CompareAndSwapInt32(&s.executeNow, enb, 0) {
				s.timer.Reset(s.interval)
				<-s.timer.C
			}
		}
	}
}

func (s *intervalSchedule2) ExecuteNow() {
	timer := s.timer
	if s.runnable && timer != nil && atomic.CompareAndSwapInt32(&s.executeNow, 0, 1) {
		timer.Reset(0)
	}
}

func (s *intervalSchedule2) Cancel() {
	s.runnable = false
	s.scheduleBase.Cancel()
}

// --------------------------------------------------------
