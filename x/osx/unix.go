//go:build !windows

package osx

import (
	"os/exec"
	"syscall"
)

const (
	LineSeparator = "\n"

	FileExtCmd = ".sh"
	FileExtExe = ""
)

func ScriptCmd(path string) *exec.Cmd {
	return exec.Command("/bin/sh", path)
}

func NewSessionProcAttr() *syscall.SysProcAttr {
	return &syscall.SysProcAttr{Setpgid: true, Pgid: 0}
}
