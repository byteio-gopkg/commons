//go:build windows

package osx

import (
	"os/exec"
	"syscall"
)

const (
	LineSeparator = "\r\n"

	FileExtCmd = ".bat"
	FileExtExe = ".exe"
)

func ScriptCmd(path string) *exec.Cmd {
	return exec.Command(path)
}

func NewSessionProcAttr() *syscall.SysProcAttr {
	return &syscall.SysProcAttr{CreationFlags: syscall.CREATE_NEW_PROCESS_GROUP}
}
