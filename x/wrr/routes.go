package wrr

import (
	"net/http"
	"path"
	"sort"
)

type Routes[H any] struct {
	routes   *routes[H]
	handlers []H
	basePath string
}

func NewRoutes[H any](basePath string, handlers ...H) *Routes[H] {
	return &Routes[H]{
		routes:   &routes[H]{},
		handlers: handlers,
		basePath: basePath,
	}
}

type routes[H any] struct {
	routes      []*route[H]
	middlewares []*middleware[H]
}

type route[H any] struct {
	method   string
	path     string
	handlers []H
}

type middleware[H any] struct {
	handler   H
	qualifier func(string, string) bool
	order     int8
}

func (r *Routes[H]) combineHandlers(handlers []H) []H {
	if len(r.handlers) == 0 {
		return handlers
	}
	finalSize := len(r.handlers) + len(handlers)
	mergedHandlers := make([]H, finalSize)
	copy(mergedHandlers, r.handlers)
	copy(mergedHandlers[len(r.handlers):], handlers)
	return mergedHandlers
}

func (r *Routes[H]) calculateAbsolutePath(relativePath string) string {
	if relativePath == "" {
		return r.basePath
	}
	return path.Join(r.basePath, relativePath)
}

func (r *Routes[H]) Group(relativePath string, handlers ...H) *Routes[H] {
	return &Routes[H]{
		routes:   r.routes,
		handlers: r.combineHandlers(handlers),
		basePath: r.calculateAbsolutePath(relativePath),
	}
}

func (r *Routes[H]) Middleware(handler H, qualifier func(method string, path string) bool, order int8) {
	rs := r.routes
	rs.middlewares = append(rs.middlewares, &middleware[H]{handler, qualifier, order})
}

func (r *Routes[H]) Handle(method string, relativePath string, handlers []H) {
	rs := r.routes
	rs.routes = append(rs.routes, &route[H]{
		method,
		r.calculateAbsolutePath(relativePath),
		r.combineHandlers(handlers),
	})
}

// POST is a shortcut for routes.Handle("POST", path, handlers).
func (r *Routes[H]) POST(relativePath string, handlers ...H) {
	r.Handle(http.MethodPost, relativePath, handlers)
}

// GET is a shortcut for routes.Handle("GET", path, handlers).
func (r *Routes[H]) GET(relativePath string, handlers ...H) {
	r.Handle(http.MethodGet, relativePath, handlers)
}

// DELETE is a shortcut for routes.Handle("DELETE", path, handlers).
func (r *Routes[H]) DELETE(relativePath string, handlers ...H) {
	r.Handle(http.MethodDelete, relativePath, handlers)
}

// PATCH is a shortcut for routes.Handle("PATCH", path, handlers).
func (r *Routes[H]) PATCH(relativePath string, handlers ...H) {
	r.Handle(http.MethodPatch, relativePath, handlers)
}

// PUT is a shortcut for routes.Handle("PUT", path, handlers).
func (r *Routes[H]) PUT(relativePath string, handlers ...H) {
	r.Handle(http.MethodPut, relativePath, handlers)
}

// OPTIONS is a shortcut for routes.Handle("OPTIONS", path, handlers).
func (r *Routes[H]) OPTIONS(relativePath string, handlers ...H) {
	r.Handle(http.MethodOptions, relativePath, handlers)
}

// HEAD is a shortcut for routes.Handle("HEAD", path, handlers).
func (r *Routes[H]) HEAD(relativePath string, handlers ...H) {
	r.Handle(http.MethodHead, relativePath, handlers)
}

func (r *Routes[H]) Complete(dest func(method string, path string, handlers []H)) int {
	rs := r.routes
	middlewares := rs.middlewares
	sort.Slice(middlewares, func(i, j int) bool {
		return middlewares[i].order < middlewares[j].order
	})
	middlewarePostIndex := 0
	for ; middlewarePostIndex < len(middlewares); middlewarePostIndex++ {
		if middlewares[middlewarePostIndex].order > 0 {
			break
		}
	}
	middlewares1 := middlewares[:middlewarePostIndex]
	middlewares2 := middlewares[middlewarePostIndex:]
	appendMiddlewares := func(rt *route[H], handlers []H, middlewares []*middleware[H]) []H {
		for _, p := range middlewares {
			if qf := p.qualifier; qf == nil || qf(rt.method, rt.path) {
				handlers = append(handlers, p.handler)
			}
		}
		return handlers
	}
	for _, rt := range rs.routes {
		handlers := make([]H, 0, len(rt.handlers)+len(middlewares))
		handlers = appendMiddlewares(rt, handlers, middlewares1)
		handlers = append(handlers, rt.handlers...)
		handlers = appendMiddlewares(rt, handlers, middlewares2)
		dest(rt.method, rt.path, handlers)
	}
	return len(rs.routes)
}
