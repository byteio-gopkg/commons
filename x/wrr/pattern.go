package wrr

import (
	"regexp"
	"strings"
)

func Patterns(patterns []string) func(string, string) bool {
	var includes []*regexp.Regexp
	var excludes []*regexp.Regexp
	for _, pattern := range patterns {
		pattern0 := pattern
		exclude := false
		if strings.HasPrefix(pattern, "!") {
			exclude = true
			pattern = pattern[1:]
		}
		parts := strings.SplitN(pattern, ":", 2)
		if len(parts) == 1 {
			parts = []string{"*", parts[0]}
		}
		parts[0] = strings.ReplaceAll(parts[0], "*", "\\w+")
		part := parts[1]
		part = strings.ReplaceAll(part, "**", "<ANY>")
		part = strings.ReplaceAll(part, "*", "[^/]*")
		parts[1] = strings.ReplaceAll(part, "<ANY>", ".*")
		regx, err := regexp.Compile("^" + parts[0] + ":" + parts[1] + "$")
		if err != nil {
			panic(`invalid route pattern[` + pattern0 + `]`)
		}
		if exclude {
			excludes = append(excludes, regx)
		} else {
			includes = append(includes, regx)
		}
	}
	return func(method string, path string) bool {
		routeName := method + ":" + path
		for _, p := range excludes {
			if p.MatchString(routeName) {
				return false
			}
		}
		for _, p := range includes {
			if p.MatchString(routeName) {
				return true
			}
		}
		return false
	}
}
