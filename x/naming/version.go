package naming

import (
	"strconv"
	"strings"
)

func VersionCompare(a, b string) int {
	if a == b {
		return 0
	}
	as := strings.Split(a, ".")
	bs := strings.Split(b, ".")
	for i := 0; i < len(as); i++ {
		bv := 0
		if i < len(bs) {
			bv, _ = strconv.Atoi(bs[i])
		}
		av, _ := strconv.Atoi(as[i])
		if av > bv {
			return 1
		} else if av < bv {
			return -1
		}
	}
	if len(bs) > len(as) {
		for i := len(as); i < len(bs); i++ {
			bv, _ := strconv.Atoi(bs[i])
			if bv > 0 {
				return -1
			}
		}
	}
	return 0
}
