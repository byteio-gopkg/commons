package forms

import (
	"byteio.org/commons"
	"byteio.org/commons/x/errors"
	"byteio.org/commons/x/types/null"
	"fmt"
	"regexp"
	"slices"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

type Lang struct {
	Required                string
	Invalid                 string
	LengthLimited           string
	NumericRequired         string
	MinimumLimited          string
	MaximumLimited          string
	ElementNotEmptyRequired string
	ElementsTotalLimit      string
	ContainsInvalidValue    string
}

var lang = LangEN

func SetLang(_lang Lang) {
	lang = _lang
}

var LangEN = Lang{
	Required:                " required",
	Invalid:                 " invalid",
	LengthLimited:           " length limited %d",
	NumericRequired:         " numeric required",
	MinimumLimited:          " minimum value %s limited",
	MaximumLimited:          " maximum value %s limited",
	ElementNotEmptyRequired: " element required not empty",
	ElementsTotalLimit:      " elements total limited %s",
	ContainsInvalidValue:    " contains invalid value '%s'",
}

var LangZhCN = Lang{
	Required:                "不能为空",
	Invalid:                 "无效",
	LengthLimited:           "长度不能超过%d个字符",
	NumericRequired:         "必须为数字",
	MinimumLimited:          "不能小于%s",
	MaximumLimited:          "不能大于%s",
	ElementNotEmptyRequired: "不能含有空值",
	ElementsTotalLimit:      "个数不能超过%d",
	ContainsInvalidValue:    "含有无效值'%s'",
}

// --------------------------------------------------------
// ---------------- text ------------------------

func TextValue(value string, name string, optional bool, maxLength int) (string, error) {
	value = strings.TrimSpace(value)
	if value == "" {
		if !optional {
			return "", errors.NewIllegalArgumentError(name + lang.Required)
		}
		return "", nil
	}
	if utf8.RuneCountInString(value) > maxLength {
		return "", errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.LengthLimited, maxLength))
	}
	return value, nil
}

func TextValueRequired(value string, name string) (string, error) {
	return TextValue(value, name, false, 200)
}

func TextValueOptional(value string, name string) (string, error) {
	return TextValue(value, name, true, 200)
}

func Text(value *string, name string, optional bool, maxLength int) error {
	v := *value
	v = strings.TrimSpace(v)
	if v == "" {
		if !optional {
			return errors.NewIllegalArgumentError(name + lang.Required)
		}
		*value = ""
		return nil
	}
	if utf8.RuneCountInString(v) > maxLength {
		return errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.LengthLimited, maxLength))
	}
	*value = v
	return nil
}

func TextRequired(value *string, name string) error {
	return Text(value, name, false, 200)
}

func TextOptional(value *string, name string) error {
	return Text(value, name, true, 200)
}

func NumericText(value *string, name string, optional bool, maxLength int) error {
	err := Text(value, name, optional, maxLength)
	if err != nil || value == nil {
		return err
	}
	if v := *value; v != "" {
		if matched, _ := regexp.MatchString("^[0-9]+$", v); !matched {
			return errors.NewIllegalArgumentError(name + lang.NumericRequired)
		}
	}
	return nil
}

func Texts(texts []string, name string, optional bool, maxSize int) error {
	if texts == nil {
		if optional {
			return nil
		}
		return errors.NewIllegalArgumentError(name + lang.Required)
	}
	if len(texts) == 0 {
		return errors.NewIllegalArgumentError(name + lang.Required)
	}
	if len(texts) > maxSize {
		return errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.ElementsTotalLimit, maxSize))
	}
	for i, v := range texts {
		v = strings.TrimSpace(v)
		if v == "" {
			return errors.NewIllegalArgumentError(name + lang.ElementNotEmptyRequired)
		}
		if utf8.RuneCountInString(v) > 500 {
			return errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.LengthLimited, 500))
		}
		texts[i] = v
	}
	return nil
}

func Tokens(value *string, name string, optional bool, allowed func(v string) bool) error {
	vv := *value
	if vv == "" {
		if !optional {
			return errors.NewIllegalArgumentError(name + lang.Required)
		}
		return nil
	}
	array := strings.Split(vv, ",")
	out := make([]string, 0, len(array))
	for _, v := range array {
		v = strings.TrimSpace(v)
		if v == "" || slices.Contains(out, v) {
			continue
		}
		if allowed != nil && !allowed(v) {
			return errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.ContainsInvalidValue, v))
		}
		out = append(out, v)
	}
	if len(out) == 0 {
		if !optional {
			return errors.NewIllegalArgumentError(name + lang.Required)
		}
	}
	*value = strings.Join(out, ",")
	return nil
}

func EnumTokens(value *string, name string, optional bool, allowed []string) error {
	return Tokens(value, name, optional, func(v string) bool {
		return slices.Contains(allowed, v)
	})
}

func TextOfValues[V any](array []V) string {
	out := make([]string, 0, len(array))
	for _, v := range array {
		out = append(out, fmt.Sprint(v))
	}
	return strings.Join(out, ",")
}

// ---------------- number ----------------------

func Number[V commons.Number](value V, name string, min V, max V) error {
	if value < min {
		return errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.MinimumLimited, fmt.Sprint(min)))
	}
	if value > max {
		return errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.MaximumLimited, fmt.Sprint(max)))
	}
	return nil
}

func IntText(value string, name string, min int, max int) (int, error) {
	value = strings.TrimSpace(value)
	if value == "" {
		return 0, errors.NewIllegalArgumentError(name + lang.Required)
	}
	intValue, err := strconv.Atoi(value)
	if err != nil {
		return 0, errors.NewIllegalArgumentError(name + lang.Invalid)
	}
	if intValue < min {
		return 0, errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.MinimumLimited, fmt.Sprint(min)))
	}
	if intValue > max {
		return 0, errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.MaximumLimited, fmt.Sprint(max)))
	}
	return intValue, nil
}

func ParseInt32sText(value string) []int32 {
	array := strings.Split(value, ",")
	out := make([]int32, 0, len(array))
	for _, v0 := range array {
		if v, err := strconv.Atoi(v0); err == nil {
			out = append(out, int32(v))
		} else {
			return nil
		}
	}
	return out
}

func Int32sText(value string, name string, required bool, limit int, deduplicate bool) ([]int32, error) {
	if value == "" {
		if required {
			return nil, errors.New(name + lang.Required)
		}
		return nil, nil
	}
	array := strings.Split(value, ",")
	length := len(array)
	if limit > 0 && length > limit {
		return nil, errors.New(name + fmt.Sprintf(lang.ElementsTotalLimit, limit))
	}
	int32s := make([]int32, 0, length)
	for _, v0 := range array {
		v0 = strings.TrimSpace(v0)
		if id, err := strconv.Atoi(v0); err == nil {
			int32s = append(int32s, int32(id))
		} else {
			return nil, errors.New(name + fmt.Sprintf(lang.ContainsInvalidValue, v0))
		}
	}
	if length == 1 || !deduplicate {
		return int32s, nil
	}
	set := map[int32]bool{}
	out := make([]int32, 0)
	for _, v := range int32s {
		if !set[v] {
			set[v] = true
			out = append(out, v)
		}
	}
	return out, nil
}

func NullableInt(value *null.Int, name string, optional bool, min int, max int) error {
	if !value.NotNull {
		if optional {
			return nil
		}
		return errors.NewIllegalArgumentError(name + lang.Required)
	}
	if value.Value < min {
		return errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.MinimumLimited, fmt.Sprint(min)))
	}
	if value.Value > max {
		return errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.MaximumLimited, fmt.Sprint(max)))
	}
	return nil
}

// ---------------- time -------------------------

func TimeText(value *string, name string, optional bool, layout string) (*time.Time, error) {
	v := *value
	v = strings.TrimSpace(v)
	if v == "" {
		if !optional {
			return nil, errors.NewIllegalArgumentError(name + lang.Required)
		}
		*value = ""
		return nil, nil
	}
	parsed, err := time.ParseInLocation(layout, v, time.Local)
	if err != nil {
		return nil, errors.NewIllegalArgumentError(name + lang.Invalid)
	}
	*value = v
	return &parsed, nil
}

func DateText(value *string, name string, optional bool) error {
	_, err := TimeText(value, name, optional, time.DateOnly)
	return err
}

func DateTimeText(value *string, name string, optional bool) error {
	_, err := TimeText(value, name, optional, time.DateTime)
	return err
}

// ---------------- others ----------------------

func MailAddress(value *string, name string, optional bool) error {
	v := *value
	v = strings.TrimSpace(v)
	if v == "" {
		if !optional {
			return errors.NewIllegalArgumentError(name + lang.Required)
		}
		*value = v
		return nil
	}
	if len(v) > 200 {
		return errors.NewIllegalArgumentError(name + fmt.Sprintf(lang.LengthLimited, 200))
	}
	if !regexp.MustCompile(`\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*`).MatchString(v) {
		return errors.NewIllegalArgumentError(name + lang.Invalid)
	}
	*value = v
	return nil
}
