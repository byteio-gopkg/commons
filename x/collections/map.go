package collections

import "sync"

type SyncMap[K comparable, V any] struct {
	m    map[K]V
	lock sync.RWMutex
}

func NewSyncMap[K comparable, V any]() *SyncMap[K, V] {
	return &SyncMap[K, V]{
		m: make(map[K]V),
	}
}

func (m *SyncMap[K, V]) Size() int {
	return len(m.m)
}

func (m *SyncMap[K, V]) Get(key K) V {
	m.lock.RLock()
	value := m.m[key]
	m.lock.RUnlock()
	return value
}

func (m *SyncMap[K, V]) Put(key K, value V) {
	m.lock.Lock()
	m.m[key] = value
	m.lock.Unlock()
}

func (m *SyncMap[K, V]) Contains(key K) bool {
	m.lock.RLock()
	_, present := m.m[key]
	m.lock.RUnlock()
	return present
}

func (m *SyncMap[K, V]) Remove(key K) V {
	m.lock.Lock()
	value, present := m.m[key]
	if present {
		delete(m.m, key)
	}
	m.lock.Unlock()
	return value
}

func (m *SyncMap[K, V]) Clear() {
	m.lock.Lock()
	if len(m.m) <= 64 {
		clear(m.m)
	} else {
		m.m = make(map[K]V)
	}
	m.lock.Unlock()
}

func (m *SyncMap[K, V]) Range(out func(k K, v V)) {
	m.lock.RLock()
	defer m.lock.RUnlock()
	for k, v := range m.m {
		out(k, v)
	}
}

func (m *SyncMap[K, V]) Keys() []K {
	out := make([]K, 0, len(m.m))
	m.lock.RLock()
	for k := range m.m {
		out = append(out, k)
	}
	m.lock.RUnlock()
	return out
}

func (m *SyncMap[K, V]) Values() []V {
	out := make([]V, 0, len(m.m))
	m.lock.RLock()
	for _, v := range m.m {
		out = append(out, v)
	}
	m.lock.RUnlock()
	return out
}
