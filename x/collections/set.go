package collections

import "sync"

func ToSet[T comparable](array []T) map[T]bool {
	if array == nil {
		return nil
	}
	set := make(map[T]bool)
	for _, v := range array {
		set[v] = true
	}
	return set
}

func ToSetAll[T comparable](all ...[]T) map[T]bool {
	if all == nil {
		return nil
	}
	set := make(map[T]bool)
	for _, array := range all {
		for _, v := range array {
			set[v] = true
		}
	}
	return set
}

// --------------------------------------------------------

var setMapValue = struct{}{}

type SyncSet[E comparable] struct {
	m    map[E]struct{}
	lock sync.RWMutex
}

func NewSyncSet[E comparable]() *SyncSet[E] {
	return &SyncSet[E]{
		m: make(map[E]struct{}),
	}
}

func (m *SyncSet[E]) Contains(value E) bool {
	m.lock.RLock()
	_, ok := m.m[value]
	m.lock.RUnlock()
	return ok
}

func (m *SyncSet[E]) AddIfAbsent(value E) bool {
	m.lock.Lock()
	_, present := m.m[value]
	if !present {
		m.m[value] = setMapValue
	}
	m.lock.Unlock()
	return !present
}

func (m *SyncSet[E]) Remove(value E) {
	m.lock.Lock()
	delete(m.m, value)
	m.lock.Unlock()
}

func (m *SyncSet[E]) Size() int {
	return len(m.m)
}

func (m *SyncSet[E]) Values() []E {
	out := make([]E, 0, len(m.m))
	m.lock.RLock()
	for k := range m.m {
		out = append(out, k)
	}
	m.lock.RUnlock()
	return out
}

func (m *SyncSet[E]) Clear() {
	m.lock.Lock()
	clear(m.m)
	m.lock.Unlock()
}
