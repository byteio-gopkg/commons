package collections

import (
	"sync"
)

type queueNode struct {
	value any
	next  *queueNode
}

type Queue struct {
	first *queueNode
	last  *queueNode
	len   int
	lock  sync.Mutex
}

func NewQueue() *Queue {
	return &Queue{}
}

func (q *Queue) IsEmpty() bool {
	return q.first == nil
}

func (q *Queue) Len() int {
	return q.len
}

func (q *Queue) Push(v any) {
	n := &queueNode{value: v}
	q.lock.Lock()
	if q.last == nil {
		q.first = n
	} else {
		q.last.next = n
	}
	q.last = n
	q.len++
	q.lock.Unlock()
}

func (q *Queue) Poll() (v any) {
	q.lock.Lock()
	f := q.first
	if f != nil {
		v = f.value
		q.first = f.next
		if q.first == nil {
			q.last = nil
		}
		q.len--
	}
	q.lock.Unlock()
	return
}

func (q *Queue) Peek() any {
	q.lock.Lock()
	f := q.first
	q.lock.Unlock()
	if f != nil {
		return f.value
	}
	return nil
}

func (q *Queue) Clear() {
	q.lock.Lock()
	q.first = nil
	q.last = nil
	q.len = 0
	q.lock.Unlock()
}

// --------------------------------------------------------

type orderedQueueNode struct {
	value any
	order int64
}

type OrderedQueue struct {
	nodes []*orderedQueueNode
	lock  sync.Mutex
}

func NewOrderedQueue() *OrderedQueue {
	return &OrderedQueue{}
}

func (q *OrderedQueue) IsEmpty() bool {
	return len(q.nodes) == 0
}

func (q *OrderedQueue) Len() int {
	return len(q.nodes)
}

func (q *OrderedQueue) Push(value any, order int64) {
	node := &orderedQueueNode{
		value: value,
		order: order,
	}
	q.lock.Lock()
	next := len(q.nodes)
	q.nodes = append(q.nodes, node)
	if next > 0 {
		prev := next - 1
		array := q.nodes
		for prev >= 0 && next >= 0 {
			if array[prev].order <= array[next].order {
				break
			}
			array[prev], array[next] = array[next], array[prev]
			prev--
			next--
		}
	}
	q.lock.Unlock()
}

func (q *OrderedQueue) Pop() any {
	q.lock.Lock()
	size := len(q.nodes)
	if size < 1 {
		q.lock.Unlock()
		return nil
	}
	node := q.nodes[0]
	if size == 1 {
		q.nodes = nil
	} else {
		q.nodes = q.nodes[1:size]
	}
	q.lock.Unlock()
	return node.value
}

func (q *OrderedQueue) PopAll(order int64) []any {
	q.lock.Lock()
	size := len(q.nodes)
	if size < 1 || q.nodes[0].order > order {
		q.lock.Unlock()
		return nil
	}
	next := 0
	nodes := q.nodes
	var node *orderedQueueNode
	var ret []any
	for ; next < size; next++ {
		node = nodes[next]
		if node.order > order {
			break
		}
		ret = append(ret, node.value)
	}
	if next > 0 {
		if next >= size {
			q.nodes = nil
		} else {
			q.nodes = nodes[next:]
		}
	}
	q.lock.Unlock()
	return ret
}

func (q *OrderedQueue) Peek() (any, int64) {
	q.lock.Lock()
	if len(q.nodes) < 1 {
		q.lock.Unlock()
		return nil, 0
	}
	first := q.nodes[0]
	q.lock.Unlock()
	return first.value, first.order
}

func (q *OrderedQueue) PeekWithValidate(validate func(any) bool) (any, int64) {
	q.lock.Lock()
	defer q.lock.Unlock()
	size := len(q.nodes)
	if size < 1 {
		return nil, 0
	}
	i := 0
	var node *orderedQueueNode = nil
	for ; i < size; i++ {
		node = q.nodes[i]
		if validate(node.value) {
			break
		} else {
			node = nil
		}
	}
	if i > 0 {
		if i >= size {
			q.nodes = nil
		} else {
			q.nodes = q.nodes[i:]
		}
	}
	if node != nil {
		return node.value, node.order
	} else {
		return nil, 0
	}
}

func (q *OrderedQueue) Remove(value any) {
	q.lock.Lock()
	for i, node := range q.nodes {
		if node.value == value {
			q.nodes = append(q.nodes[:i], q.nodes[i+1:]...)
			break
		}
	}
	q.lock.Unlock()
}

func (q *OrderedQueue) Clear() {
	q.lock.Lock()
	q.nodes = nil
	q.lock.Unlock()
}
