package executor

import (
	"byteio.org/commons/utils/runtimeutil"
	"byteio.org/commons/x/collections"
	"errors"
	"sync"
	"time"
)

type Executor interface {
	Submit(run func(args []any), args ...any) error
	Shutdown()
	TasksNow() int
	WorkersNow() int
}

type task struct {
	run  func(args []any)
	args []any
}

type routinePoolExecutor struct {
	queue        *collections.Queue
	runnable     bool
	workersNow   int
	workersLimit int
	notify       chan int
	lock         sync.Mutex
}

func NewRoutinePoolExecutor(workersLimit int) Executor {
	return &routinePoolExecutor{
		queue:        collections.NewQueue(),
		runnable:     true,
		workersNow:   0,
		workersLimit: workersLimit,
		notify:       make(chan int),
	}
}

var (
	taskIsNilErr           = errors.New("task is nil")
	executorYetShutdownErr = errors.New("executor yet shutdown")
)

func (e *routinePoolExecutor) Submit(run func(args []any), args ...any) error {
	if run == nil {
		return taskIsNilErr
	}
	t := &task{run, args}

	e.lock.Lock()
	if !e.runnable {
		e.lock.Unlock()
		return executorYetShutdownErr
	}
	e.queue.Push(t)
	idle := false
	select {
	case e.notify <- 1:
		idle = true
	default:
	}
	if !idle {
		workersNow := e.workersNow
		if workersNow < e.workersLimit {
			e.workersNow++
			go e.workerRun()
		}
	}
	e.lock.Unlock()

	return nil
}

func (e *routinePoolExecutor) Shutdown() {
	e.lock.Lock()
	defer e.lock.Unlock()
	if !e.runnable {
		return
	}
	e.runnable = false
	e.queue.Clear()
	close(e.notify)
}

func (e *routinePoolExecutor) TasksNow() int {
	return e.queue.Len()
}

func (e *routinePoolExecutor) WorkersNow() int {
	return e.workersNow
}

func (e *routinePoolExecutor) workerRun() {
	defer func() {
		if rv := recover(); rv != nil {
			e.lock.Lock()
			e.workersNow--
			e.lock.Unlock()
			runtimeutil.PrintError("routine pool executor panic:", rv)
		}
	}()

	queue := e.queue
	for e.runnable {
		task0 := e.queue.Poll()
		if task0 == nil {
			timeout := time.NewTimer(time.Second * 5)
			select {
			case a := <-e.notify:
				timeout.Stop()
				if a == 1 {
					continue
				} else {
					e.lock.Lock()
					e.workersNow--
					e.lock.Unlock()
					return
				}
			case <-timeout.C:
				ensured := false
				e.lock.Lock()
				if queue.IsEmpty() {
					e.workersNow--
					ensured = true
				}
				e.lock.Unlock()
				if ensured {
					return
				} else {
					continue
				}
			}
		}

		t := task0.(*task)
		t.run(t.args)
	}
}
