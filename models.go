package commons

type NameValue struct {
	Name  string
	Value any
}

type Entity []*NameValue

func (e Entity) Append(name string, value any) Entity {
	return append(e, &NameValue{name, value})
}

func EntityExtend[V any](e Entity, name string, value *V) Entity {
	if value != nil {
		return e.Append(name, *value)
	}
	return e
}

type Return[V any] struct {
	Result V
	Error  error
}
