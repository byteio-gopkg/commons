// Copyright 2017 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build 386 || amd64

package cpu

// cpuid is implemented in cpu_x86.s.
func cpuid(eaxArg, ecxArg uint32) (eax, ebx, ecx, edx uint32)

func doInit() {
	maxID, _, _, _ := cpuid(0, 0)
	if maxID < 7 {
		return
	}
	_, ebx7, _, _ := cpuid(7, 0)
	X86.HasBMI2 = isSet(ebx7, 1<<8)
	X86.HasADX = isSet(ebx7, 1<<19)
}

func isSet(hwc uint32, value uint32) bool {
	return hwc&value != 0
}
